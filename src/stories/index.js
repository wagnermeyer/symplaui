import React from 'react'
import { Input } from '../source/components/ThreeDataFields'
import { storiesOf } from '@storybook/react'
import { State, Store } from '@sambego/storybook-state'
import inputMask from '../source/components/ThreeDataFields/_utils/inputMask'


const fn1 = obj => console.log( 'onChange', obj )
const fn2 = obj => console.log( 'onBlur', obj )
const fn3 = obj => console.log( 'onMount', obj )

storiesOf('Input', module)
  .add('Max length 10', () => (
    <Input
      name="my-input"
      maxLength={ 10 }
    />
  ))
  .add('Min length 5', () => (
    <Input
      name="my-input"
      minLength={ 5 }
    />
  ))
  .add('Min length 5 and max length 10', () => (
    <Input
      name="my-input"
      minLength={ 5 }
      maxLength={ 10 }
    />
  ))
  .add('Required', () => (
    <Input
      name="my-input"
      isRequired
    />
  ))
  .add('Disabled', () => (
    <Input
      name="my-input"
      isDisabled
    />
  ))
  .add('Prevent paste', () => (
    <Input
      name="my-input"
      isPasteAllowed={ false }
    />
  ))
  .add('With locales', () => (
    <Input
      name="my-input"
      lang="PT"
      isRequired
      locales={
        {
          "PT": {
            "REQUIRED_ERROR": "O campo é obrigatório"
          }
        }
      }
    />
  ))
  .add('With label', () => (
    <Input
      name="my-input"
      label="My label"
    />
  ))
  .add('With label and description', () => (
    <Input
      name="my-input"
      label="My label"
      description="My description"
    />
  ))
  .add('With phone mask', () => {
    return(
      <Input
        name="my-input"
        placeholder="(00) 00000-0000"
        maskFunc={ value => inputMask( 'DDD_PHONE', value ) }
      />
    )
  })
  .add('onUnmount', () => {
    return(
      <Input
        name="my-input"
        isRequired
        onUnmount={ fieldObj => console.log( fieldObj ) }
      />
    )
  })
  .add('Read only', () => {
    return(
      <Input
        name="my-input"
        isReadOnly
        value="Teste"
        lavel="Label"
      />
    )
  })
  .add('With external validation', () => {
    
    const teste = () => {
      return new Promise( ( resolve, reject ) => {
        setTimeout( () => {
          resolve( {
            errorMessage: 'This error message is set by the external validation, outside this component'
          } )
        }, 2000 )
      } )
    }

    return(
      <Input
        value="123"
        name="TESTE"
        onBlur={ fieldObj => console.log( fieldObj ) }
        externalValidationFunc={ () => teste() }
      />
    )
  })
  .add('With custom Validation', () => {
    
    const customValidation = value => {
      return {
        isValid: false,
        errorMessage: "This error message is set by the custom validation function, outside this component"
      }
    }

    return(
      <Input
        name="TESTE"
        customValidationFunc={ value => customValidation( value ) }
        onMount={ fieldObj => console.log( fieldObj ) }
      />
    )
  })

  .add( 'With error message', () => (
    <Input
        name="TESTE"
        errorMessage="My error message"
        isDebug
      />
  ) )

  .add('Passing error messages', () => {

    const fn1 = obj => console.log( 'onChange', obj )
    const fn2 = obj => console.log( 'onBlur', obj )
    const fn3 = obj => console.log( 'onMount', obj )

    const store = new Store({
      errorMessage: undefined
    })

    return(
      <>
        <div>
          <button onClick={ () => store.set({ errorMessage: 'The last season of Game of Thrones was a mistake' })}>Error Message 1</button>
          <button onClick={ () => store.set({ errorMessage: 'ERROR_2' })}>Error Message 2</button>
          <button onClick={ () => store.set({ errorMessage: undefined })}>Remove Error Message</button>
        </div>
        <State store={ store }>
          { state => [
            <Input
            name="TESTE"
            errorMessage={ state.errorMessage }
            onChange={ fieldObj => fn1( fieldObj ) }
            onBlur={ fieldObj => fn2( fieldObj ) }
            onMount={ fieldObj => fn3( fieldObj ) }
            isDebug
          />
          ] }
        </State>
      </>
    )
  })

  .add('Passing error messages with external validation', () => {

    const store = new Store({
      errorMessage: "MY initial error message"
    })

    const promise = () => {
      console.log( 'ssd' )
      return new Promise( ( resolve, reject ) => {
        setTimeout( () => {
          console.log( '123' )
          resolve( {
            errorMessage: 'This error message is set by the external validation, outside this component'
          } )
        }, 2000 )
      } )
    }

    const fn2 = () => {
      console.log( '213' )
      store.set({ errorMessage: undefined })
    }

    return(
      <>
        <State store={ store }>
          { state => [
            <Input
            name="TESTE"
            errorMessage={ state.errorMessage }
            externalValidationFunc={ v => promise() }
            onBlur={ fn2 }
            isDebug
          />
          ] }
        </State>
      </>
    )
  })

  .add('Passing values', () => {

    const store = new Store({
      value: undefined
    })

    return(
      <>
        <div>
          <button onClick={ () => store.set({ value: 'The Lord of The Rings' })}>Value 1</button>
          <button onClick={ () => store.set({ value: 'The Return of Jedi' })}>Value 2</button>
          <button onClick={ () => store.set({ value: undefined })}>Remove value</button>
        </div>
        <State store={ store }>
          { state => (
            <Input
              name="TESTE"
              isRequired
              value={ state.value }
              onChange={ fieldObj => fn1( fieldObj ) }
              onBlur={ fieldObj => fn2( fieldObj ) }
              onMount={ fieldObj => fn3( fieldObj ) }
              isDebug
          />
          ) }
        </State>
      </>
    )
  })

  .add('Passing valuess', () => {

    const store = new Store({
      value: undefined,
      isErrorVisible: false
    })

    return(
      <>
        <div>
          <button onClick={ () => store.set({ value: 'The Lord of The Rings' })}>Value 1</button>
          <button onClick={ () => store.set({ value: 'The Return of Jedi' })}>Value 2</button>
          <button onClick={ () => store.set({ value: undefined })}>Remove value</button>
          <button onClick={ () => store.set({ isErrorVisible: true })}>Visible</button>
        </div>
        <State store={ store }>
          { state => (
            <Input
              name="TESTE"
              isRequired
              value={ state.value }
              onChange={ fieldObj => fn1( fieldObj ) }
              onBlur={ fieldObj => fn2( fieldObj ) }
              onMount={ fieldObj => fn3( fieldObj ) }
              isErrorVisible={ state.isErrorVisible }
              isDebug
          />
          ) }
        </State>
      </>
    )
  })