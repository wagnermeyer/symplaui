// import ReactDOM from 'react-dom'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Tooltip from './_Tooltip'

const allowedEvents = ['onMouseEnter', 'onMouseLeave', 'onClick', 'onFocus', 'onBlur', 'onScroll']
class TooltipHOC extends Component {

  constructor(props) {
    super(props)
    this.state = {
        show: false
    }
    this.toolTipRef = React.createRef()
    this.top = 0
    this.left = 0
    this.width = 0
    this.height = 0
    this.setShowToFalse = this.setShowToFalse.bind( this )
  }

  setShowToFalse() {
    this.setState({ show: false })
  }

  showToolTip(e, bool) {
    window.addEventListener( 'scroll', this.setShowToFalse )
    if ( bool ) {
      this.top = e.currentTarget.getBoundingClientRect().top + window.scrollY
      this.left = e.currentTarget.getBoundingClientRect().left
      this.width = e.currentTarget.getBoundingClientRect().width
      this.height = e.currentTarget.getBoundingClientRect().height
    }
    this.setState({ show: bool })
  }

  _onMouseEnter(e, parentFunction, HOCFunction) {
    this.props.showEvent === allowedEvents[0] && this.showToolTip(e, true)
    parentFunction && parentFunction()
    HOCFunction && HOCFunction()
  }
  
  _onMouseLeave(e, parentFunction, HOCFunction) {
    this.props.hideEvent === allowedEvents[1] && this.showToolTip(e, false)
    parentFunction && parentFunction()
    HOCFunction && HOCFunction()
  }
  
  _onFocus(e, parentFunction) {
    this.props.showEvent == allowedEvents[3] && this.showToolTip(e, true)
    parentFunction && parentFunction()
  }

  _onBlur(e, parentFunction) {
    this.props.hideEvent == allowedEvents[4] && this.showToolTip(e, false)
    parentFunction && parentFunction() 
  }

  _onClick(e, parentFunction) {
    this.props.showEvent === allowedEvents[2] && this.props.showOn && this.showToolTip(e, true)
    this.props.showEvent === allowedEvents[0] && this.showToolTip(e, true)
    parentFunction && parentFunction()
  }

  componentDidMount() {
    let elPortal = document.getElementById('bottom-portal')
    if ( !elPortal ) {
      elPortal = document.createElement('div')
      elPortal.setAttribute('id', 'bottom-portal')
      document.body.appendChild( elPortal )
    }
  }

  componentWillUnmount() {
    window.removeEventListener( 'scroll', this.setShowToFalse )
  }
    
  render() {

    const fn = child => (
      React.cloneElement(child, {
        onMouseEnter: (e) => this._onMouseEnter(e, child.props.onMouseEnter, this.props.onMouseEnter),
        onMouseLeave: (e) => this._onMouseLeave(e, child.props.onMouseLeave, this.props.onMouseLeave),
        onFocus: (e) => this._onFocus(e, child.props.onFocus),
        onBlur: (e) => this._onBlur(e, child.props.onBlur),
        onClick: (e) => this._onClick(e, child.props.onClick)
      })
    )

    return (
      <React.Fragment>
        { React.Children.map(this.props.children, fn) }
        { this.state.show ? <Tooltip  description={this.props.description}
                                      parentTopPosition={this.top}
                                      parentLeftPosition={this.left}
                                      parentWidth={this.width}
                                      parentHeight={this.height} /> : null }
      </React.Fragment>
    )

  }
}

TooltipHOC.defaultProps = {
  showEvent: allowedEvents[0], //onMouseEnter
  hideEvent: allowedEvents[1], //onMouseLeave
  showOn: true
}

TooltipHOC.propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array
    ]),
    description: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string
    ]).isRequired,
    showEvent: PropTypes.oneOf(allowedEvents),
    hideEvent: PropTypes.oneOf(allowedEvents),
    showOn: PropTypes.bool,
    onMouseLeave: PropTypes.func,
    onMouseEnter: PropTypes.func
}

export default TooltipHOC


