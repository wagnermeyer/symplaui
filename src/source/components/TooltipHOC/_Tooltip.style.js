import styled from 'styled-components'

export const StyledTooltip = styled.div`

  max-width: 300px;
  padding: 12px 16px;
  background-color: #ffffff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  border-radius: 0;
  color: #777;
  font-size: 11px;
  font-weight: bold;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
          box-shadow: 0 5px 10px rgba(0, 0, 0, .2);

  &:after {
    content: '';
    display: block;
    width: 15px;
    height: 15px;
    position: absolute;
    left: 50%;
    background-color: #ffffff;
    border-bottom-width: 1px;
  }

  &.above {
    &:after {
      top: 100%;
      transform: translate(-50%,-48%) rotate(45deg);
      border: 1px solid #ccc;
      border-top: 0;
      border-left: 0;
    }
  }

  &.below {
    &:after {
      top: 0%;
      transform: translate(-50%,-52%) rotate(45deg);
      border: 1px solid #ccc;
      border-bottom: 0;
      border-right: 0;
    }
  }

  &.left {
    &:after {
      left: 20px
    }
  }

  &.right {
    &:after {
      left: auto;
      right: 3px;
    }
  }

`