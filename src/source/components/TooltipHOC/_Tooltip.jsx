import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import * as El from './_Tooltip.style'

class Tooltip extends Component {

  constructor(props) {
    super(props)
    this.refCard = React.createRef()
    this.state = {
      position: {
        top: 0,
        left: 0
      }
    }
  }

  alignBallonX = (parentLeftPosition, ballonWidth, parentWidth, windowWidth) => {

    let leftPosition;
    let className;

    if (parentLeftPosition + (ballonWidth/2) <= windowWidth) {
      leftPosition = (parentLeftPosition - (ballonWidth/2)) + (parentWidth/2)
      className = ''
    } else if (parentLeftPosition + (ballonWidth/2) > windowWidth) {
      leftPosition = (parentLeftPosition - ballonWidth) + (parentWidth/2) + 20
      className = 'right'
    }

    if (leftPosition < 0) {
      leftPosition = (parentLeftPosition + (parentWidth/2)) - 20
      className = 'left'
    }

    return {
      left: leftPosition,
      class: className
    }
  }

  componentDidMount() {

    this.width = this.refCard.current.getBoundingClientRect().width
    this.height = this.refCard.current.getBoundingClientRect().height
    this.abovePosition = this.props.parentTopPosition - this.height - 15
    this.belowPosition = this.props.parentTopPosition + this.props.parentHeight + 15
    this.yPosition = this.abovePosition >= 0 ? 'above' : 'below'

    this.setState({
      position: {
        top: this.abovePosition >= 0 ? this.abovePosition : this.belowPosition,
        left: this.alignBallonX(this.props.parentLeftPosition, this.width, this.props.parentWidth, window.innerWidth).left,
        className: this.alignBallonX(this.props.parentLeftPosition, this.width, this.props.parentWidth, window.innerWidth).class
      }
    })

  }

  render() {

    return ReactDOM.createPortal(
      <El.StyledTooltip ref={this.refCard}
                    className={`${this.yPosition} ${this.state.position.className}`}
                    style={{
                      position: 'absolute',
                      top: this.state.position.top,
                      left: this.state.position.left,
                      zIndex: 999
                    }}>
        {this.props.description}
      </El.StyledTooltip>,
      document.getElementById("bottom-portal")
    )

  }
}

Tooltip.propTypes = {
  description: PropTypes.node.isRequired,
  parentTopPosition: PropTypes.number.isRequired,
  parentLeftPosition: PropTypes.number.isRequired,
  parentWidth: PropTypes.number.isRequired,
  parentHeight: PropTypes.number.isRequired
}

export default Tooltip