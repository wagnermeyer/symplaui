import React, { useEffect } from 'react'

const useConcatSubmit = ( fn, submitObj ) => {
  return useEffect( () => {
    console.log( 'useConcatSubmit' )
    submitObj.shouldSubmit
    && submitObj.submitFn( fn )
  }, [submitObj.submitTrigger] )
}

export default useConcatSubmit