import React, { useState, useEffect } from 'react'

const ConcatForms = props => {

  const [ submitCounter, setSubmitCounter ] = useState( 0 )
  const [ isSubmitting, setIsSubmitting ] = useState( false )
  const [ isErrorVisible, setIsErrorVisible ] = useState( false )

  const submitForm = () => { console.log('started'); setIsSubmitting( true ); setSubmitCounter( val => val + 1 ) }
  const maxIndex = props.children.length - 1
  let concatedData = {}
  let allInvalidFields = []

  const run = (fn, formIndex) => {
    const { fields, invalidFields } = fn()
    concatedData = {
      ...concatedData,
      ...fields
    }
    allInvalidFields = [
      ...allInvalidFields,
      ...invalidFields
    ]
    // Save validation in array
    // Concat values in object
    if ( formIndex === maxIndex ) {
      // Check validation
      // If is valid
      console.log( 'done', {
        concatedData,
        allInvalidFields
      } )
      setIsSubmitting( false )
    }
  }

  const save = ( name, value ) => {
    concatedData[name] = value
  }
  
  const submit = () => {
    setSubmitCounter( val => val + 1 )
    setIsErrorVisible( true )
    // console.log( concatedData )
  }

  useEffect( () => {
    console.log( submitCounter )
  }, [submitCounter] )

  const giveSuperPowers = (child, i) => {
    return React.cloneElement( child, {
      teste: {
        isErrorVisible: submitCounter
      },
      saveField: ( name, value ) => save( name, value ),
    } )
  }

  return (
    <React.Fragment>
      { React.Children.map( props.children, (child, i) => giveSuperPowers( child, i ) ) }
      <div>
      <br/>
      <br/>
        <button
          onClick={ () => submit() }
          disabled={ isSubmitting }
        >SUBMIT ALL FORMS (ConcatForms)</button>
      </div>
      {
        isSubmitting && 'Processing'
      }
      <br/>
      <br/>
      <hr></hr>
      <div>ConcatForms output:</div>
      {/* <pre>
        { JSON.stringify( formsOutput, null, 4 ) }
      </pre> */}
    </React.Fragment>
    )

}

export default ConcatForms