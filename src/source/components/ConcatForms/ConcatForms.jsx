import React, { useState } from 'react'

const ConcatForms = props => {

  const [ submitCounter, setSubmitCounter ] = useState( 0 )
  const [ isSubmitting, setIsSubmitting ] = useState( false )

  const submitForm = () => { console.log('started'); setSubmitCounter( val => val + 1 ) }
  const maxIndex = props.children.length - 1
  let concatedData = {}
  let allInvalidFields = []

  const run = (fn, formIndex) => {
    const { fields, invalidFields } = fn()
    concatedData = {
      ...concatedData,
      ...fields
    }
    allInvalidFields = [
      ...allInvalidFields,
      ...invalidFields
    ]
    // Save validation in array
    // Concat values in object
    if ( formIndex === maxIndex ) {
      // Check validation
      // If is valid
      console.log( 'done', {
        concatedData,
        allInvalidFields
      } )
    }
  }
  
  const giveSuperPowers = (child, i) => {
    return React.cloneElement( child, {
      submit: {
        submitFn: fn => run( fn, i ),
        submitTrigger: submitCounter,
        shouldSubmit: !!submitCounter
      }
    } )
  }

  return (
    <React.Fragment>
      { React.Children.map( props.children, (child, i) => giveSuperPowers( child, i ) ) }
      <div>
      <br/>
      <br/>
        <button
          onClick={ () => submitForm() }
          disabled={ isSubmitting }
        >SUBMIT ALL FORMS (ConcatForms)</button>
      </div>
      {
        isSubmitting && 'Processing'
      }
      <br/>
      <br/>
      <hr></hr>
      <div>ConcatForms output:</div>
      {/* <pre>
        { JSON.stringify( formsOutput, null, 4 ) }
      </pre> */}
    </React.Fragment>
    )

}

export default React.memo(ConcatForms)