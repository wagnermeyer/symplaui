export default {
  "reservation": {
    "tickets": [
      {
        "id": 1,
        "name": "Ticket #1",
        "qty": 2
      },
      {
        "id": 2,
        "name": "Ticket #1",
        "qty": 1
      }
    ],
    "customForm": {
      "groups": [
        {
          "id": "custom-text-group",
          "label": "My custom text",
          "isRequired": true,
          "description": undefined,
          "appliedTo": [1,2],
          "fields": [
            {
              "name": "custom-text",
              "maxLength": 12,
              "minLength": 5,
              "label": "",
              "isUnique": false,
              "isRequired": true
            }
          ]
        },
        {
          "id": "address-group",
          "label": "Endereço",
          "isRequired": true,
          "description": undefined,
          "appliedTo": [1,2],
          "fields": [
            {
              "name": "cep",
              "maxLength": 12,
              "minLength": 5,
              "label": "CEP",
              "isUnique": false,
              "isRequired": true
            },
            {
              "name": "street",
              "maxLength": 12,
              "minLength": 5,
              "label": "Av/Rua",
              "isUnique": false,
              "isRequired": true
            }
          ]
        }
      ]
    }
  }
}