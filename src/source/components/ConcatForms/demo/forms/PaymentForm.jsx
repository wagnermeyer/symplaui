import React, { useState, useRef, useEffect } from 'react'
import Input from '../.././../ThreeDataFields/Input/Input'
import useConcatSubmit from '../../hooks/useConcatSubmit'

const PaymentForm = props => {

  const [ isErrorVisible, setIsErrorVisible ] = useState( false )
  const outputObj = useRef( {} ) //Set the object that will be submitted
  const invalidFields = useRef( [] )

  /**
  * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  * @function submit
  * @description The submit function.
  * @returns {undefined}
  */
  const submit = () => {
    console.log('paymentForm submitted')
    return {
      fields: outputObj.current,
      invalidFields: invalidFields.current
    }
  }

  useEffect( () => {
    console.log( props.name, 'REFRESHED', props )
  } )


  /**
  * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  * @function onFieldsBlur
  * @description When the field has an onBlur event, this function is called
  * @param {object} fieldObj - The field object
  * @param {string} fieldObj.name - The field name
  * @param {string} fieldObj.value - The field value
  * @param {boolean} fieldObj.isValid - The field validation
  * @returns {undefined}
  */
  const onFieldsBlur = fieldObj => {
    // Updating the output object
    outputObj.current = {
      ...outputObj.current,
      [ fieldObj.name ]: fieldObj.value
    }
    handleInvalidField( fieldObj.name, fieldObj.isValid )
  }


  /**
  * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  * @function onFieldsMount
  * @description When the field mounts, this function is called
  * @param {object} fieldObj - The field object
  * @param {string} fieldObj.name - The field name
  * @param {string} fieldObj.value - The field value
  * @param {boolean} fieldObj.isValid - The field validation
  * @returns {undefined}
  */
  const onFieldsMount = fieldObj => {
    // Creating the output object
    outputObj.current = {
      ...outputObj.current,
      [ fieldObj.name ]: fieldObj.value
    }
    handleInvalidField( fieldObj.name, fieldObj.isValid )
  }


  /**
  * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  * @function handleInvalidField
  * @description Adds or removes the field name from the invalid fields array
  * @param {string} fieldName - The field name that will be added
  * @param {boolean} isValid - The field validation
  * @returns {undefined}
  */
  const handleInvalidField = ( fieldName, isValid ) => {

    const fieldIndex = invalidFields.current.indexOf( fieldName )
    const fieldNameIsSet = Boolean( fieldIndex > -1 )
    if ( isValid ) {
      // Remove from invalid fields array
      fieldNameIsSet && invalidFields.current.splice( fieldIndex, 1 )
    } else {
      // Add to invalid fields array
      !fieldNameIsSet && invalidFields.current.push( fieldName )
    }
  
  }

  useConcatSubmit( () => submit(), props.submit )

  const renderFields = () => {
    let inputs = []
    let i = 0
    let max = 20
    while (i < max) {
      inputs.push(
        <Input
          key={ i }
          name={`credit-card-number-${ i }`}
          label={`Credit card number ${ i }`}
          lang={ 'pt' }
          locales={
            {
              PT: {
                "MIN_LENGTH_ERROR": "123 {{minLength}}"
              }
            }
          }
          minLength={ 3 }
          onMount={ fieldObj => onFieldsMount( fieldObj ) }
          onBlur={ fieldObj => onFieldsBlur( fieldObj ) }
          isRequired
        />
      )
      i++
    }
    return inputs
  }

  return (
    <React.Fragment>

      <h2>PAYMENT FORM</h2>


      {
        renderFields()
      }


      <button onClick={ () => submit() }>Submit</button>

    </React.Fragment>
  )

}

export default React.memo(PaymentForm)