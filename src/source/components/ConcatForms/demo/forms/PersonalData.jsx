import React, { useState, useRef } from 'react'
import { Input } from '../../../ThreeDataFields'
import useConcatSubmit from '../../hooks/useConcatSubmit'

const PersonalData = props => {

  const fieldsArr = [ 1,2,3 ]
  const [ isErrorVisible, setIsErrorVisible ] = useState( false )
  const outputObj = useRef( {} )
  
  const submit = a => {
    setIsErrorVisible( true )
    return {
      outputObj: outputObj.current,
      isValid: false
    }
  }

  const onFieldsBlur = fieldObj => {
    outputObj.current = {
      ...outputObj.current,
      [ fieldObj.name ]: fieldObj.value
    }
  }

  const onFieldsMount = fieldObj => {
    outputObj.current = {
      ...outputObj.current,
      [ fieldObj.name ]: fieldObj.value
    }
  }

  useConcatSubmit( () => submit(), props.submit )
  
  return (
    <div style={  { padding: '10px' }  } >
      Personal data (Form 1)
      <div>{ props.someProp }</div>
    {
      fieldsArr.map( (field, i) => (
        <Input
          label={ String(field) }
          key={ i }
          name={ `f-${ field }` }
          isRequired
          isErrorVisible={ isErrorVisible }
          onBlur={ fieldObj => onFieldsBlur( fieldObj ) }
          onMount={ fieldObj => onFieldsMount( fieldObj ) }
        />
      )
       )
    }

    </div>
  )

}

export default React.memo(PersonalData)