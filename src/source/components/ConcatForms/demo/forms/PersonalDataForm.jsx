import React, { useState, useEffect } from 'react'
import useConcatSubmit from '../../hooks/useConcatSubmit'

  const PersonalDataForm = props => {


    // Defining state - - - - - - - - - - - - - - - - - - - - - - - - - - 
    const [ error, setError ] = useState( '' )
    const [ invalidFields, setInvalidFields ] = useState( [] )
    const [ isErrorVisible, setIsErrorVisible ] = useState( false )
    const [
      values,
      setValues
    ] = useState({
      'first-name': '',
      'last-name': ''
    })


    // Validation - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    useEffect( () => {

      const invalidFieldsCopy = [ ...invalidFields ] // copying arrays
      const fieldIndex = invalidFieldsCopy.indexOf( 'first-name' ) // get the index

      // Validating first-name as required
      if ( values[ 'first-name' ].length > 0 ) {

        // If there's a value, we remove the error and remove the value from invalid fields array
        setError('')
        fieldIndex > -1 && invalidFieldsCopy.splice( fieldIndex, 1 ) // if exists, remove the value from the invalid fields array
        setInvalidFields( invalidFieldsCopy ) // Set the new array
      } else {
        // Otherwise, we set the error and add the field to the invalidFields array
        setError('This field is required')
        if ( !fieldIndex > -1 ) { // If this field is not set on invalid fields array
          // We add it to the array
          invalidFieldsCopy.push( 'first-name' )
          setInvalidFields( invalidFieldsCopy )
        }
      }
    }, [values] )


    // Submit function - - - - - - - - - - - - - - - - - - - - - - - - - -
    const submit = () => {
      
      console.log( 'personalData submitted' )
      
      setIsErrorVisible( true )
      
      return {
        fields: values,
        invalidFields
      }
      
    }

    useConcatSubmit( () => submit(), props.submit )

    return (
      <React.Fragment>

      <h2>PERSONAL DATA</h2>

      <div>
        { /* First name field */ }
        <label>First name*</label>
        <input
          type="text"
          name="first-name"
          value={ values['first-name'] }
          className={ error ? 'error' : '' }
          onChange={
            e => setValues({
              ...values,
              [ e.target.name ]: e.target.value
            })
          }
        />
        { error && isErrorVisible && <div className="error-message">{ error }</div> }
      </div>

      <div>
        { /* Last name field */ }
        <label>Last name</label>
        <input
          type="text"
          name="last-name"
          value={ values['last-name'] }
          onChange={
            e => setValues({
              ...values,
              [ e.target.name ]: e.target.value
            })
          }
        />
      </div>

      {/* <div>
        <button onClick={ () => submit() }>submit</button>
      </div> */}

      </React.Fragment>
    )

  }

  export default React.memo(PersonalDataForm)