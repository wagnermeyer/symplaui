import React from 'react'
import ConcatForms from '../ConcatForms'
import PersonalData from './forms/PersonalData'
import Address from './forms/Address'

const Demo1 = () => {

  return (
    <ConcatForms>
      <PersonalData
        someProp={ 'Passing some prop to this form' }
      />
      <Address/>
    </ConcatForms>
  )

}

export default Demo1