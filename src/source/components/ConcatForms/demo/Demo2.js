import React from 'react'
import ConcatForms from '../ConcatForms'
import PersonalDataForm from './forms/PersonalDataForm'
import PaymentForm from './forms/PaymentForm'

const Demo2 = () => {

  return (
    <ConcatForms>
      <PaymentForm
        name="alow"
      />
      <PaymentForm
        name="brazil"
      />
      <PaymentForm
        name="blah"
      />
      <PaymentForm
        name="alow"
      />
      <PaymentForm
        name="brazil"
      />
      <PaymentForm
        name="blah"
      />
    </ConcatForms>
  )

}

export default Demo2