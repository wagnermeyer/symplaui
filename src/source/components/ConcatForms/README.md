#ConcatForms
POC v0.1.0

##Problem
Sometimes, we have a huge form with many subforms that have many fields. As an example, we can have a form with "Personal data form", "Payment form" and "Billing form". This can be messy when we start to handle all this forms together without a pattern. Besides that, we can have some performance issues, as we need to handle all this data together.

##Solution
This component has been developed with all of this concerns in mind. So, this component has the solutions below:
- **Forms data concating:** Each form inside `<ConcatForms/>` have his own data, his own validation and his own rules. But, when the `<ConcatForms/>` runs the *submit* action on each form, all of the data will be concated in one object inside `<ConcatForms/>`, after this, the concated data will be exposed.
- **General validation handling:** Each form inside `<ConcatForms/>` have his own validations. Sometimes, the "Personal data form" can be valid, but the "Payment form" isn't. In this case, the `<ConcatForms/>` will be invalid.
- **Auto-scroll to invalid fields (In development):** Based on the rule that each form has his own validations, we need to inform the `<ConcatForms/>` which fields are invalid. This will allow the `<ConcatForms/>` to scroll to the invalid fields.
- **Lazy loading forms (In development):** Huge forms can have some performance issues because of the amount of components beeing loaded at the same time. To prevent a binding hell, we can lazy load each form as we scroll.

##Usage example
First of all, let's create two forms that will be concated by the `<ConcatForms/>`:

###1. Building the example forms
In this section we will build two independent forms. This step doesn't make part of the implementation, you can use your own forms.

####PersonalDataForm
As we want to ensure that every type of form will work, no matter how it has been coded, we will develop the `PersonalDataForm` in the classic way:
```js
// PersonalDataForm.jsx

import React, { useState, useEffect } from 'react'

const PersonalDataForm = props => {

  // Defining state
  const [ error, setError ] = useState( '' )
  const [
    values,
    setValues
  ] = useState({
    'first-name': '',
    'last-name': ''
  })

  //Validation
  useEffect( () => {
    // Validating first-name as required
    if ( values[ 'first-name' ].length > 0 ) {
      // If there's a value, we remove the error
      setError('')
    } else {
      // Otherwise, we set the error
      setError('This field is required')
    }
  }, [values] )

  // Submit function
  const submit = () => {
    if ( !error ) { // If there's no error, we can submit the form
      props.onSubmit( values )
    }
  }

  return (
    <React.Fragment>

      { /* First name field */ }
      <label>First name*</label>
      <input
        type="text"
        name="first-name"
        value={ values['first-name'] }
        className={ error ? 'error' : '' }
        onChange={
          e => setValues({
            ...values,
            [ e.target.name ]: e.target.value
          })
        }
      />
      { error && <div className="error-message">{ error }</div> }

      { /* Last name field */ }
      <label>Last name</label>
      <input
        type="text"
        name="last-name"
        value={ values['last-name'] }
        onChange={
          e => setValues({
            ...values,
            [ e.target.name ]: e.target.value
          })
        }
      />

      <button onClick={ () => submit() } >Submit</button>

    </React.Fragment>
  )

}

export default PersonalDataForm

```

####PaymentForm
This form will be developed using our Sympla-UI's input component:

```js
// PaymentForm.jsx

import React, { useState } from 'react'
import { Input } from '@sympla/sympla-ui'

const PaymentForm = props => {

  const [ fields, setFields ] = useState( {} )
  const outputObj = useRef( {} ) //Set the object that will be submitted
  

  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @function submit
   * @description The submit function.
   * @returns {undefined}
   */
  const submit = () => {
    setIsErrorVisible( true ) // Setting the error visibility to true
    props.onSubmit( outputObj ) // Run the onSubmit function with the output object
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @function onFieldsBlur
   * @description When the field has an onBlur event, this function is called
   * @param {object} fieldObj - The field object
   * @param {string} fieldObj.name - The field name
   * @param {string} fieldObj.value - The field value
   * @param {boolean} fieldObj.isValid - The field validation
   * @returns {undefined}
   */
  const onFieldsBlur = fieldObj => {
    // Updating the output object
    outputObj.current = {
      ...outputObj.current,
      [ fieldObj.name ]: fieldObj.value
    }
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @function onFieldsMount
   * @description When the field mounts, this function is called
   * @param {object} fieldObj - The field object
   * @param {string} fieldObj.name - The field name
   * @param {string} fieldObj.value - The field value
   * @param {boolean} fieldObj.isValid - The field validation
   * @returns {undefined}
   */
  const onFieldsMount = fieldObj => {
    // Creating the output object
    outputObj.current = {
      ...outputObj.current,
      [ fieldObj.name ]: fieldObj.value
    }
  }


  return (
    <React.Fragment>

      { /* Credit card name field */ }
      <Input
        name='credit-card-number'
        label='Credit card number'
        onMount={ fieldObj => onFieldsMount( fieldObj ) }
        isRequired
      />

      { /* Credit card code field */ }
      <Input
        name='credit-card-code'
        label='Credit card code'
        onMount={ fieldObj => onFieldsMount( fieldObj ) }
        isRequired
      />

      <button onClick={  }>Submit</button>

    </React.Fragment>
  )

}

export default PaymentForm

```

###2. Concating our forms
As we have two working independent forms that have his own validations and submit methods, now we will concat everything:

First of all, we need to pass the forms as children of the `<ConcatForms/>`
```js
// App.js

import React from 'react'
import PersonalDataForm from './PersonalDataForm'
import PaymentForm from './PaymentForm'
import { ConcatForms } from '@sympla/sympla-ui'

const App = () => {
  return (
    <React.Fragment>
      <ConcatForms>
        <PersonalDataForm/>
        <PaymentForm/>
      </ConcatForms>
    </React.Fragment>
  )
}

export default App
```

This actually will do **nothing**. We need to **update** our forms to allow the `<ConcatForms/>` to run the *submit* function inside each form. This can be achieved by three steps:

##### Step 1 - Import the `useConcatSubmit` hook.
```js
import React from 'react'
import { useConcatSubmit } from '@sympla/sympla-ui'

//...
```
##### Step 2 -Use the hook to fire the submit event
The `<ConcatForms/>` provides a custom hook to simplify the implementation. You can use it like:
```js
import React from 'react'
import { useConcatSubmit } from '@sympla/sympla-ui'

const YourForm = props => {

  useConcatSubmit( () => {
    // Here you can write your submit code
  }, props.submit )

  return (
    // ...your form tags
  )

}
```
Or, you can do

###### Wait, where the props.submit comes from?
As you can see, the second argument of the `useConcatSubmit` is `props.submit`. Should you write a new prop?

No. Here's why:
`<ConcatForms>` is a H.O.C., when you provide your forms as his children they will receive a new prop called `submit`. So, you can use it with the `useConcatSubmit` hook.

##### Step 3 - Your submit function needs to return an object with `fields (object)` and `invalidFields (array)`.
The `<ConcatForms/>` expects a object like:
```js
import React from 'react'
import { useConcatSubmit } from '@sympla/sympla-ui'

const YourForm = props => {

  useConcatSubmit( () => {
    return {
      fields: {
        "field-name-1:": "some inputted value",
        "field-name-2:": "another value"
      },
      invalidFields: [
        "field-name-3",
        "field-name-4",
        "field-name-5"
      ]
    }
  }, props.submit )

  return (
    // ...your form
  )

}
//...
```

## Done!
In our example, the new code will look like:

####PersonalDataForm
```diff
  // PersonalDataForm.jsx

  import React, { useState, useEffect } from 'react'
+ import { useConcatSubmit } from '@sympla/sympla-ui'

  const PersonalDataForm = props => {


    // Defining state - - - - - - - - - - - - - - - - - - - - - - - - - - 
    const [ error, setError ] = useState( '' )
+   const [ invalidFields, setInvalidFields ] = useState( [] )
    const [
      values,
      setValues
    ] = useState({
      'first-name': '',
      'last-name': ''
    })


    // Validation - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    useEffect( () => {

+       const invalidFieldsCopy = [ ...invalidFields ] // copying arrays
+       const fieldIndex = invalidFieldsCopy.indexOf( 'first-name' ) // get the index

      // Validating first-name as required
      if ( values[ 'first-name' ].length > 0 ) {

-       // If there's a value, we remove the error
+       // If there's a value, we remove the error and remove the value from invalid fields array
        setError('')
+       fieldIndex > -1 && invalidFieldsCopy.splice( fieldIndex, 1 ) // if exists, remove the value from the invalid fields array
+       setInvalidFields( invalidFieldsCopy ) // Set the new array
      } else {
-       // Otherwise, we set the error
+       // Otherwise, we set the error and add the field to the invalidFields array
        setError('This field is required')
+       if ( !fieldIndex > -1 ) { // If this field is not set on invalid fields array
+         // We add it to the array
+         invalidFieldsCopy.push( 'first-name' )
+         setInvalidFields( invalidFieldsCopy )
+       }
      }
    }, [values] )


    // Submit function - - - - - - - - - - - - - - - - - - - - - - - - - -
    const submit = () => {
-     if ( !error ) { // If there's no error, we can submit the form
-       props.onSubmit( values )
-     }
+     return {
+       fields: values,
+       invalidFields
+     }
    }

+ useConcatSubmit( () => submit(), props.submit )

    return (
      <React.Fragment>

        { /* First name field */ }
        <label>First name*</label>
        <input
          type="text"
          name="first-name"
          value={ values['first-name'] }
          className={ error ? 'error' : '' }
          onChange={
            e => setValues({
              ...values,
              [ e.target.name ]: e.target.value
            })
          }
        />
        { error && <div className="error-message">{ error }</div> }

        { /* Last name field */ }
        <label>Last name</label>
        <input
          type="text"
          name="last-name"
          value={ values['last-name'] }
          onChange={
            e => setValues({
              ...values,
              [ e.target.name ]: e.target.value
            })
          }
        />

+     // As we want to allow the ConcatForm to submit our form, we don't need this anymore.
-     <button onClick={ () => submit() } >Submit</button>

      </React.Fragment>
    )

  }

  export default PersonalDataForm

```

####PaymentForm
```diff
  // PaymentForm.jsx

  import React, { useState, useRef } from 'react'
  import { Input } from '@sympla/sympla-ui'
+ import { useConcatSubmit } from '@sympla/sympla-ui'

  const PaymentForm = props => {

    const [ fields, setFields ] = useState( {} )
+   const [ invalidFields, setInvalidFields ] = useState( [] )
    const outputObj = useRef( {} ) //Set the object that will be submitted
    

    /**
    * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    * @function submit
    * @description The submit function.
    * @returns {undefined}
    */
    const submit = () => {
      setIsErrorVisible( true ) // Setting the error visibility to true
-     props.onSubmit( outputObj ) // Run the onSubmit function with the output object
+     return {
+       fields,
+       invalidFields
+     }
    }


    /**
    * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    * @function onFieldsBlur
    * @description When the field has an onBlur event, this function is called
    * @param {object} fieldObj - The field object
    * @param {string} fieldObj.name - The field name
    * @param {string} fieldObj.value - The field value
    * @param {boolean} fieldObj.isValid - The field validation
    * @returns {undefined}
    */
    const onFieldsBlur = fieldObj => {
      // Updating the output object
      outputObj.current = {
        ...outputObj.current,
        [ fieldObj.name ]: fieldObj.value
      }
    }


    /**
    * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    * @function onFieldsMount
    * @description When the field mounts, this function is called
    * @param {object} fieldObj - The field object
    * @param {string} fieldObj.name - The field name
    * @param {string} fieldObj.value - The field value
    * @param {boolean} fieldObj.isValid - The field validation
    * @returns {undefined}
    */
    const onFieldsMount = fieldObj => {
      // Creating the output object
      outputObj.current = {
        ...outputObj.current,
        [ fieldObj.name ]: fieldObj.value
      }
+     handleInvalidField( fieldObj.name, fieldObj.isValid )
    }


+   /**
+   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
+   * @function handleInvalidField
+   * @description Adds or removes the field name from the invalid fields array
+   * @param {string} fieldName - The field name that will be added
+   * @param {boolean} isValid - The field validation
+   * @returns {undefined}
+   */
+   const handleInvalidField = ( fieldName, isValid ) => {
+     
+     const invalidFieldsCopy = [ ...invalidFields ]
+     const fieldIndex = invalidFieldsCopy.indexOf( fieldName )
+     const fieldNameIsSet = Boolean( fieldIndex > -1 )
+     if ( isValid ) {
+       // Remove from invalid fields array
+      fieldNameIsSet && invalidFieldsCopy.splice( fieldIndex, 1 )
+     } else {
+       // Add to invalid fields array
+       !fieldNameIsSet && invalidFieldsCopy.push( fieldName )
+     }
+     setInvalidFields( invalidFieldsCopy )
+
+   }


+   useConcatSubmit( () => submit(), props.submit )


    return (
      <React.Fragment>

        { /* Credit card name field */ }
        <Input
          name='credit-card-number'
          label='Credit card number'
          onMount={ fieldObj => onFieldsMount( fieldObj ) }
          onBlur={ fieldObj => onFieldsBlur( fieldObj ) }
          isRequired
        />

        { /* Credit card code field */ }
        <Input
          name='credit-card-code'
          label='Credit card code'
          onMount={ fieldObj => onFieldsMount( fieldObj ) }
          onBlur={ fieldObj => onFieldsBlur( fieldObj ) }
          isRequired
        />

-       <button onClick={  }>Submit</button>

      </React.Fragment>
    )

  }

  export default PaymentForm

```

[Demo here](https://sympla-ui-6b7cb.web.app/)