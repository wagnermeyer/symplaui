import styled from 'styled-components'

export const Label = styled.div`
  font-size: 14px;
  line-height: 24px;
  font-weight: 600;
  margin-bottom: 5px;
  & svg {
    vertical-align: top;
    cursor: pointer;
  }
`