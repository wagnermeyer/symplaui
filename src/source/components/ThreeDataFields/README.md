#The "Three Data Fields" concept

##What is a "Three Data Field" component?
As the concept name refers, a "Three Data Field" is a field that outputs 3 values:
- **name**: The first data type is the name of the field. This is used to identify the field.
- **value**: The second data type is the value of the field. This is the most important data because it represents the value presented in the Input.
- **isValid**: The third data type is the validation of the field. Based on the field configuration, it will validate itself and set this value.


##Why three values?
Three values are exactly the amount of data needed to handle anything that uses the field. Here's why:
- First of all, the most obvious: when you need to manage values from fields, you need to relate the field **name** to the field **value**. So, there's a **name** and a **value**.
- As the field has his own validations, we need to expose if that field is valid or not. There's no need to expose which validation have failed. So, we expose the validation as a `boolean` on a property called **isValid**.

At the end, we have this three data types together in a single object. We use to call this object as `fieldObj`. Here's an example:
```js
const fieldObj = {
  name: 'my-input-name',
  value: 'Do or do not, there\'s no try',
  isValid: true
}
```


##How to access the fieldObj?
Each field component has his own props methods that will run a function inside the component and expose the current `fieldObj` of the field to the parent:

Let's se an example of the `Input` component:
```js

import React from 'react'
import { Input } from '@sympla/three-data-fields'

const MyParentComponent = () => {

  const logTheFieldObj = fieldObj => {
    console.log( fieldObj )
    // -> {
    //  name: 'my-input',
    //  value: '',
    //  isValid: true
    //   }
  }

  return (
    <Input
      name="my-input"
      onBlur={ fieldObj => logTheFieldObj( fieldObj ) }
    >
  )

}

```
In the example above, the `logTheFieldObj()` function will be called when the `Input` component has an `onBlur` event. This will allow you to access the `fieldObj` from outside the component.

##Which components are available?
Currently, we have this components available:
- Input
- Select (In development)
- RadioButtons (In development)
- TextArea (In development)
- CheckBox (In development)