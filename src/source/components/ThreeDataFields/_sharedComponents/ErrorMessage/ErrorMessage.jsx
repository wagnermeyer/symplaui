import { FieldError } from '../../_utils/errors'
import originalLocales from '../../_locales/ThreeDataFields.locales.json'


const ErrorMessage = ( { fieldName, errorCode, lang, minLength, maxLength, locales } ) => {

  const throwError = message => {
    const errorInfos = {
      fieldName,
      message,
      componentName: 'ErrorMessage'
    }
    throw new FieldError( errorInfos )
  }

  let _errorMessage = errorCode
  lang = lang.toUpperCase()
  errorCode = errorCode.toUpperCase()

  const isCode = errorCode.includes('_')
  if ( isCode ) {
    const hasCustomLocale = !!locales
    if ( hasCustomLocale ) {
      try {
        if ( !locales[ lang ] ) throwError( `Could'nt read ${ lang } inside custom 'locales' object` )
        if ( !locales[ lang ][ errorCode ] ) throwError( `Could'nt read ${ errorCode } inside custom 'locales[ ${ lang } ]' object` )
        _errorMessage = locales[ lang ][ errorCode ]
      }
      catch( err ) {
        _errorMessage = originalLocales[ 'EN' ][ errorCode ]
        console.error( err )
      }
    } else {
      try {

        if ( !originalLocales ) throwError( "Could'nt read 'originalLocales'" )
        if ( !originalLocales[ lang ] ) throwError( `Could'nt read ${ lang } inside 'originalLocales' object` )
        if ( !originalLocales[ lang ][ errorCode ] ) throwError( `Could'nt read ${ errorCode } inside 'originalLocales[ ${ lang } ]' object` )
        _errorMessage = originalLocales[ lang ][ errorCode ]
    
      } catch( err ) {
        _errorMessage = errorCode
        console.error( err )
      }
    }

    if ( _errorMessage.includes('{{minLength}}') ) {
      _errorMessage = _errorMessage.replace('{{minLength}}', minLength)
    }
    
    if ( _errorMessage.includes('{{maxLength}}') ) {
      _errorMessage = _errorMessage.replace('{{maxLength}}', maxLength)
    }

  }

  return _errorMessage

}

export default ErrorMessage