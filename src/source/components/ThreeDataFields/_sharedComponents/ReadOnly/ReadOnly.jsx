import React from 'react'
import PropTypes from 'prop-types'
import * as El from './ReadOnly.style.js'

const ReadOnly = props => {
  
  function getValue(value) {
    if (typeof value !== 'undefined') {
      if(typeof(value) === 'string') {
        return value || '---'
      }
      else {
        return value.length > 0 ? value.join() : '---'
      }
    }
    return '---'
  }

  return(
    <El.Container> 
      <El.Label>{props.label}</El.Label> 
      <p>{getValue(props.value)}</p>
    </El.Container>
  )

}


ReadOnly.propTypes = {
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ])
}

export default ReadOnly