import styled from 'styled-components'

export const Container = styled.div`
  border-left: 5px solid #eee;
  padding-left: 12px;
  margin-bottom: 24px;
`
export const Label = styled.div`
  font-size: 14px;
  line-height: 24px;
  font-weight: 600;
`