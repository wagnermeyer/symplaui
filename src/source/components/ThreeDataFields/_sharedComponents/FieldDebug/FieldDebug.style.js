import styled from 'styled-components'

export const Container = styled.div`
  font-family: sans-serif;
  font-size: 12px;
  background-color: #e9e9e9;
  padding: 15px;
  pre {
    font-family: sans-serif;
    margin: 0;
  }
`

export const Title = styled.div`
  font-weight: bolder;
  margin-bottom: 10px;
`

export const Box = styled.div`
  border: 1px solid #c9c9c9;
  border-radius: 5px;
  padding: 15px;
  margin-bottom: 15px;
  width: 50%;
`

export const Flex = styled.div`
  display: flex;
`