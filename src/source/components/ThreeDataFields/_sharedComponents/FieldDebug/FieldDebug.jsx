import React from 'react'
import * as El from './FieldDebug.style'

const FieldDebug = props => {

  return (
    <El.Container>
      <El.Title>DEBUG:</El.Title>

      <El.Flex>
        <El.Box>
          <div><strong>Props:</strong></div>
          <pre>{ JSON.stringify(props.parentProps, null, 2) }</pre>
        </El.Box>

        <El.Box>
          <div><strong>State:</strong></div>
          <pre>{ JSON.stringify(props.state, null, 2) }</pre>
        </El.Box>
      </El.Flex>

    </El.Container>
  )

}

export default FieldDebug