
const inputMask = (pattern, value) => {

  function applyMask(val, placeholder) {

    let l = placeholder.length, newValue = '', i, j, isInt, isLetter;
    // strip special characters
    let strippedValue = val.replace(/\D/g, "");
    
    for (i = 0, j = 0; i < l; i++) {

      isInt = !isNaN(parseInt(strippedValue[j]));
      isLetter = strippedValue[j] ? strippedValue[j].match(/[A-Z]/i) : false;
      let matchesNumber = 'XdDmMyY9'.indexOf(placeholder[i]) >= 0;
      let matchesLetter = '_'.indexOf(placeholder[i]) >= 0;
      
      if ((matchesNumber && isInt) || ( matchesLetter && isLetter)) {

        newValue += strippedValue[j++];

      } else if ((!isInt && matchesNumber) || ( (matchesLetter && !isLetter) || (matchesNumber && !isInt))) {
        // masking.errorOnKeyEntry(); // write your own error handling function
        return newValue; 

      } else {
        if(strippedValue.length > 0)
          newValue += placeholder[i];
      } 
      // break if no characters left and the pattern is non-special character
      if (strippedValue[j] === undefined) { 
        break;
      }
    }
    return newValue;
  }

  switch(pattern) {
    case 'DDD_PHONE':
      return applyMask(value,'(99) 9999-99999')
    case 'CEP':
      return applyMask(value,'99999-999')
    case 'CPF':
      return applyMask(value,'999.999.999-99')
    case 'CNPJ':
      return applyMask(value,'99.999.999/9999-99')
    case 'CPF_CNPJ':
      if(value.length <= 14)
        return applyMask(value,'999.999.999-99')
      else
        return applyMask(value,'99.999.999/9999-99')
    case 'DATE':
      return applyMask(value,'dd/mm/yyyy')
    default:
      return applyMask(value, pattern)
  }
}

export default inputMask