class FieldError extends Error {

  constructor( { message, fieldName, componentName } ) {
    super( message )
    this.message = JSON.stringify({
      fieldName,
      componentName,
      message
    }, null, 2)
  }

}

export {
  FieldError
}