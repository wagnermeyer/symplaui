/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @function isValid
 * @param {string} errorMessage The error message
 * @description Defines if the input is valid or not based on the presence of an error message
 * @returns {boolean}
 */
const isValid = errorMessage => errorMessage ? false : true



/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @function isGreaterThanMinLength
 * @param {string} value The input value
 * @param {number} minLength The min length
 * @description Validates the min length
 * @returns {boolean}
 */
const isGreaterThanMinLength = ( value, minLength ) => value.length < minLength ? false : true




/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @function isLessThanMaxLength
 * @param {string} value The input value
 * @param {number} maxLength The max length
 * @description Validates the min length
 * @returns {boolean}
 */
const isLessThanMaxLength = ( value, maxLength ) => value.length > maxLength ? false : true



/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @function isRequiredValid
 * @param {string} value 
 * @description Validates required value
 * @returns {boolean}
 */
const isRequiredValid = value => value && value.length > 0 ? true : false



export {
  isValid,
  isGreaterThanMinLength,
  isLessThanMaxLength,
  isRequiredValid
}