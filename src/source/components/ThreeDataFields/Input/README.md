# Input

The Input component was developed using the *Three Data Field* concept. Click here to understand it better.

## Examples:
### Simple input

Setting up:
```js
import React from 'react'
import { Input } from '@sympla/sympla-ui'

const MySimpleInput = () => {
  return (
    <Input
      name="my-symple-input"
    />
  )
}

export default MySimpleInput

```

Getting the value on blur:
```js
import React from 'react'
import { Input } from '@sympla/sympla-ui'

const MySimpleInput = () => {

  const getValue = fieldObj => {
    console.log( `${ fieldObj.name } value is ${ fieldObj.value } and ${ isValid ? 'is' : 'is not' } valid` )
  }

  return (
    <Input
      name="my-symple-input"
      onBlur={ fieldObj => getValue( fieldObj ) }
    />
  )
}

export default MySimpleInput

```

Setting as required:
```js
import React from 'react'
import { Input } from '@sympla/sympla-ui'

const MySimpleInput = () => {

  const getValue = fieldObj => {
    console.log( `${ fieldObj.name } value is ${ fieldObj.value } and ${ isValid ? 'is' : 'is not' } valid` )
  }

  return (
    <Input
      name="my-symple-input"
      onBlur={ fieldObj => getValue( fieldObj ) }
      isRequired
    />
  )
}

export default MySimpleInput

```

## Main props:

| Prop | Type | Default | Description |
|-|-|-|-|
| name | `string` | `undefined` | Sets the name of the field |
| value | `string` | `empty` | Sets the value of the field |
| label | `string` | `undefined` | Sets the label of the field |
| description | `string` | `undefined` | Sets the description of the field |
| placeholder | `string` | `undefined` | Sets the placeholder of the field |
| errorMessage | `string` | `undefined` | Sets the error message of the field |
| isDisabled | `boolean` | `false` | Sets if the fields is disabled or not |

## Validation props:

| Prop | Type | Default | Description |
|-|-|-|-|
| minLength | `number` | `undefined` | Sets the min length of the field value |
| maxLength | `number` | `undefined` | Sets the max length of the field value |
| isRequired | `boolean` | `false` | Sets if the field is required |
| customValidationFunc | `function` | `undefined` | Allow you to pass a custom validation to the field. Must return an object with an errorMessage that can be `undefined` or a string. |
| externalValidationFunc | `function` | `undefined` | Allow you to pass a promise that validates the value externally. Must return a promise with an object with an errorMessage that can be `undefined` or a string  |

## Method props
| Prop | Type | Default | Description |
|-|-|-|-|
| onBlur | `function` | `undefined` | Function that runs on the "blur" event on input |
| onChange | `function` | `undefined` | Function that runs on the "change" event on input |
| onMount | `function` | `undefined` | Function that runs when the component mounts |
| onUnmount | `function` | `undefined` | Function that runs when the component unmounts |

## Advanced props
| Prop | Type | Default | Description |
|-|-|-|-|
| lang | `string` | EN | Sets the input language |
| locales | `object` | `undefined` | Sets new locales |
| maskFunc | `function` | `undefined` | Sets a function which will return a masked value |
| isErrorVisible | `boolean` | `false` | Sets the error visibility |
| isReadOnly | `boolean` | `false` | Sets if the input will be read-only |
| isPasteAllowed | `boolean` | `true` | Sets if the input allows the paste event |
| isDebug | `boolean` | `false` | Enables the debug box below the input |