import styled from 'styled-components'
import LoadingGIF from '../../../style/icons/loader.gif'

export const InputContainer = styled.div`
  position: relative;
  margin-bottom: 20px;
  .loading-icon {
    position: absolute;
    top: 50%;
    right: 10px;
    transform: translateY(-50%);
    width: 20px;
    height: 20px;
    background-image: url(${ LoadingGIF });
    background-size: contain;
  }
`

export const Input = styled.input`
  color: #333333;
  background-color: #ffffff;
  border: 1px solid #cccccc;
  font-size: 14px;
  height: 40px;
  border-radius: 4px;
  padding: 0px 12px;
  width: 100%;
  box-sizing: border-box;
  
  &[disabled] {
    background-color: #eee;
    opacity: 1;
  }

  &:focus {
    border-color: #3898EC;
    outline: 0;
  }

  &.error {
    border-color: #ff0000;
  }
  
`

export const ErrorMessage = styled.div`
  color: #ff0000;
  font-size: 12px;
  margin-top: -17px;
  height: 18px;
`

export const Debug = styled.div`
  font-size: 11px;
  line-height: 1.5;
  .false {
    color: #ff0000;
  }
  .true {
    color: #58C22E;
  }
`