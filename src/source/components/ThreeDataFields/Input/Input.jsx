import React, {
  useState,
  useEffect,
  useRef
} from 'react'
import PropTypes from 'prop-types'

/* Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
import * as RootEl from '../ThreeDataFields.style'
import * as El from './Input.style'

/* Utils - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
import { FieldError } from '../_utils/errors'
import {
  isValid,
  isGreaterThanMinLength,
  isLessThanMaxLength,
  isRequiredValid
} from '../_utils/validations'

/* Icons - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
import QuestionMarkIcon from '../../../style/icons/ic-question-mark.svg'

/* Components - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
import TooltipHOC from '../../TooltipHOC/TooltipHOC'

// Shared component - - - - - - - - - - - - - -
import ErrorMessage from '../_sharedComponents/ErrorMessage/ErrorMessage'
import FieldDebug from '../_sharedComponents/FieldDebug/FieldDebug'
import ReadOnly from '../_sharedComponents/ReadOnly/ReadOnly'

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */




const Input = props => {
  
  const [ value, setValue ] = useState( props.value || '' )
  const [ isErrorVisible, setIsErrorVisible ] = useState( false )
  const [ errorMessage, setErrorMessage ] = useState()
  const [ isLoading, setIsLoading ] = useState( false )

  // This refValue is set because the `useEffect` that runs when
  // the component unmount needs to get the updated value.
  const refValue = useRef( value )
  const refErrorMessage = useRef()




  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function throwError 
   * @description Default function to throw a field error
   * @param {string} message The error message
   * @throws FieldError
   */
  const throwError = message => {
    const errorInfos = {
      componentName: 'Input',
      fieldName: props.name,
      message
    }
    throw new FieldError( errorInfos )
  }




  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function getFieldObj
   * @param {string} value The input value
   * @description Defines the fieldObj based on value
   * @returns {object} Returns the fieldObj
   */
  const getFieldObj = value => {

    const _errorMessage = getErrorMessage( value ) // Get the error message
    // setErrorMessage( _errorMessage ) // Set the error message state

    // Set and return the fieldObj
    return {
      name: props.name,
      value: value,
      isValid: isValid( _errorMessage ) // The validation is based on the errorMessage
    }

  }




  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function getErrorMessage
   * @param {string} value The input value
   * @description Get the error message
   * @returns {undefined|string} Returns the error message
   */
  const getErrorMessage = value => {

    let isValid = true
    let _errorMessage = undefined

    if ( isValid && props.errorMessage ) {
      isValid = false
      _errorMessage = props.errorMessage
    }

    if ( isValid && props.isRequired ) {
      isValid = isRequiredValid( value )
      _errorMessage = isValid ? undefined : 'REQUIRED_ERROR'
    }

    if ( isValid && value && props.minLength ) {
      isValid = isGreaterThanMinLength( value, props.minLength )
      _errorMessage = isValid ? undefined : 'MIN_LENGTH_ERROR'
    }

    if ( isValid && value && props.maxLength ) {
      isValid = isLessThanMaxLength( value, props.maxLength )
      _errorMessage = isValid ? undefined : 'MAX_LENGTH_ERROR'
    }

    if ( isValid && props.customValidationFunc ) {
      isValid = !props.customValidationFunc( value ).errorMessage
      _errorMessage = isValid ? undefined : props.customValidationFunc( value ).errorMessage
    }

    return _errorMessage

  }




  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function _onBlur
   * @description Every time the input has an `onBlur` event, this function will run
   * @returns {undefined}
   */
  const _onBlur = async () => {

    setIsErrorVisible( true ) // The error visibility is set to true on each blur

    // Get and set the error message
    let _errorMessage = getErrorMessage( value )
    setErrorMessage( _errorMessage )
    refErrorMessage.current = _errorMessage

    // Checking if we need to call the external validation promise
    if ( props.externalValidationFunc // Check if there's an external validation
         && value // Check if there's a value set on *state*
    ) {

      // Here we run the onBlur method before the promise, because we need to dispatch an immediatelly event
      // In this case, we dispatch as invalid because the promise will specify the right validation
      props.onBlur
      && props.onBlur( {
          name: props.name,
          value: value,
          isValid: false
        } )

      try {
        setIsLoading( true )
        const response = await props.externalValidationFunc( {
          name: props.name,
          value: value,
          isValid: false
        } )
        if ( !response ) throwError( 'The response of ExternalValidation is undefined' )
        _errorMessage = refErrorMessage.current || response.errorMessage // Overwrite the error message with the returned error message
        setErrorMessage( _errorMessage ) // Set the new error message on *state*
      } catch( error ) {
        console.error( error )
      } finally {
        setIsLoading( false )
      }
    }

    // Check and run the `props.onBlur` function
    props.onBlur
    && props.onBlur( {
        name: props.name,
        value: value,
        isValid: isValid( _errorMessage )
      } )

  }




  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function _onChange
   * @description Every time the input has an `onChange` event, this function will run
   * @param {string} value 
   */
  const _onChange = value => {

    // Here we update the value on *state*
    setValue( props.maskFunc ? props.maskFunc( value ) : value )
    refValue.current = props.maskFunc ? props.maskFunc( value ) : value

    // WHILE the user inputs a value, we don't want to
    // inform the errors, so we define the errors visibility
    // to `false`. The error visibility will be set to `true`
    // again on the `_onBlur_` function
    setIsErrorVisible( false )

    // Get and set the error message on *state*
    const _errorMessage = getErrorMessage( value )
    setErrorMessage( _errorMessage )
    refErrorMessage.current = _errorMessage

    // Check and run the `props.onChange` function
    props.onChange
    && props.onChange( {
        name: props.name,
        value: refValue.current,
        isValid: isValid( _errorMessage )
      } )
  }



  /**
   * ----------------------------------------------------------------------------------------------
   * - - - - - - - - - - - - - - - - - - - USE EFFECT HOOKS - - - - - - - - - - - - - - - - - - - -
   * ----------------------------------------------------------------------------------------------
   */




  // ----------------------------------------------------------------------------------------------
  // This useEffect simulates the `componentDidMount` lifecycle - - - - - - - - - - - - - - - - - - 
  useEffect( () => {
    const fieldObj = getFieldObj( value ) // Get the `fieldObj`
    const _errorMessage = getErrorMessage( value ) // Get the error message
    setErrorMessage( _errorMessage ) // Set the error message state
    refErrorMessage.current = _errorMessage
    props.onMount && props.onMount( fieldObj ) // Run the `onMount` prop with the `fieldObj`
  }, [] )




  // ---------------------------------------------------------------------------------------------
  // Every time the `props.value` changes, we run the code below - - - - - - - - - - - - - - - - -
  useEffect( () => {
    
    if ( props.value !== value ) {
      setValue( props.value ) // Set the value state with the `props.value`
      const fieldObj = getFieldObj( props.value ) // Get the `fieldObj`
      const _errorMessage = getErrorMessage( props.value ) // Get the error message
      setErrorMessage( _errorMessage ) // Set the error message state
      refErrorMessage.current = _errorMessage
  
      // Here we run `onChange` and `onBlur` props.
      // Why we are running both props?
      // As the `props.value` is setting a new value, we need to inform the parent that the value has
      // changed. So, if `props.onChange` and/or `props.onBlur` are set, then we need to run both.
      props.onChange && props.onChange( fieldObj )
      props.onBlur && props.onBlur( fieldObj )
    }

  }, [props.value] )




  // ---------------------------------------------------------------------------------------------
  // Every time the `value` changes, we run the code below - - - - - - - - - - - - - - - - -
  useEffect( () => {
    // This exists only to update our refValue with the current state value.
    // Needed only because the `useEffect` that runs when the component unmounts
    // needs to get the updated value.
    refValue.current = value
  }, [value] )




  // --------------------------------------------------------------------------------------------
  // Every time the `props.errorMessage` changes, we run the code below - - - - - - - - - - - - -
  useEffect( () => {
    if ( props.errorMessage !== errorMessage ) {
      setIsErrorVisible( true ) // As we are handling an error message, we set enable the error visibility
      const fieldObj = getFieldObj( value ) // Get the `fieldObj`
      const _errorMessage = getErrorMessage( value ) // Get the error message
      setErrorMessage( _errorMessage ) // Set the error message state
      refErrorMessage.current = _errorMessage
      // Here we run `onChange` and `onBlur` props.
      // Why we are running both props?
      // As the `props.value` is setting a new value, we need to inform the parent that the value has
      // changed. So, if `props.onChange` and/or `props.onBlur` are set, then we need to run both.
      props.onChange && props.onChange( fieldObj )
      props.onBlur && props.onBlur( fieldObj )
    }
  }, [props.errorMessage] )



  // --------------------------------------------------------------------------------------------
  // Every time the `props.isErrorVisible` changes, we run the code below - - - - - - - - - - - - -
  useEffect( () => {
    props.isErrorVisible !== undefined &&
    setIsErrorVisible( props.isErrorVisible )
  }, [props.isErrorVisible] )




  // --------------------------------------------------------------------------------------------
  // When the component unmounts, we run the code below - - - - - - - - - - - - - - - - - - - - -
  useEffect( () => {
    return () => {

      // Get the errorMessage
      const _errorMessage = getErrorMessage( refValue.current )

      // Run the onUnmount prop
      props.onUnmount
      && props.onUnmount( {
        name: props.name,
        value: refValue.current,
        isValid: isValid( _errorMessage )
      } )

    }
  }, [] )




  return(

    props.isReadOnly
    ? <ReadOnly
        label={ props.label }
        value={ value }
      />
    : <React.Fragment>

      { props.label &&
        <RootEl.Label>
          { props.label } { props.isRequired && '*' }
          {
            props.label &&
            props.description &&
            <TooltipHOC description={ props.description }>
              <QuestionMarkIcon width={22} height={22} />
            </TooltipHOC>
          }
        </RootEl.Label>
      }

      <El.InputContainer>
        <El.Input
          type={ 'text' }
          name={ props.name }
          value={ value }
          placeholder={ props.placeholder }
          disabled={ props.isDisabled }
          onChange={ e => _onChange( e.target.value ) }
          onBlur={ () => _onBlur() }
          onPaste={ !props.isPasteAllowed ? e => { e.preventDefault() } : null }
          className={ errorMessage && isErrorVisible ? 'error' : undefined }
        />
        {
          isLoading &&
          <div className="loading-icon"></div>
        }
      </El.InputContainer>

      { isErrorVisible &&
        errorMessage &&
          <El.ErrorMessage>
            <ErrorMessage
              fieldName={ props.name }
              errorCode={ errorMessage }
              lang={ props.lang }
              minLength={ props.minLength }
              maxLength={ props.maxLength }
              locales={ props.locales }
            />
          </El.ErrorMessage>
      }

      {
        props.isDebug &&
        <FieldDebug
          state={ {
            value,
            isErrorVisible,
            errorMessage,
            isLoading
          } }
          parentProps={ props }
        />
      }

    </React.Fragment>
  )

}

Input.defaultProps = {
  lang: 'EN',
  value: '',
  isDisabled: false,
  isRequired: false,
  isReadOnly: false,
  isDebug: false,
  isPasteAllowed: true
}


Input.propTypes = {
  // Main props
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  label: PropTypes.string,
  description: PropTypes.string,
  placeholder: PropTypes.string,
  errorMessage: PropTypes.string,
  isDisabled: PropTypes.bool,
  // Advanced props
  lang: PropTypes.string,
  locales: PropTypes.object,
  maskFunc: PropTypes.func,
  isErrorVisible: PropTypes.bool,
  isReadOnly: PropTypes.bool,
  isPasteAllowed: PropTypes.bool,
  isDebug: PropTypes.bool,
  // Methods:
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onMount: PropTypes.func,
  onUnmount: PropTypes.func,
  // Validations:
  minLength: PropTypes.number,
  maxLength: PropTypes.number,
  isRequired: PropTypes.bool,
  customValidationFunc: PropTypes.func,
  externalValidationFunc: PropTypes.func
}

export default React.memo( Input )