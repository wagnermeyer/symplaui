import React from 'react'
import Enzyme, { shallow, mount, render } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Input from './Input'
import ErrorMessage from '../_sharedComponents/ErrorMessage/ErrorMessage'
import ReadOnly from '../_sharedComponents/ReadOnly/ReadOnly'
import { Label } from '../ThreeDataFields.style'
import Tooltip from '../../TooltipHOC/TooltipHOC'
import FieldDebug from '../_sharedComponents/FieldDebug/FieldDebug'

Enzyme.configure( { adapter: new Adapter() } )

const originalError = console.error;

beforeAll(() => {
  console.error = jest.fn();
})

afterAll(() => {
  console.error = originalError;
})

describe( 'Input', () => {

  // When the component mounts, only onMount should be called

  it( `OnMount - Component will be INVALID if mounted with no value and is a required field`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        isRequired
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": false, "name": "my-input", "value": ""} )
    
  } )

  it( `OnMount - Component will be VALID if mounted with value and is a required field`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        value="My custom value"
        isRequired
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": true, "name": "my-input", "value": "My custom value"} )
    
  } )

  it( `OnMount - Component will be INVALID if mounted with a value with length LESS THAN the value set in minLength prop`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        value="Legolas"
        minLength={ 10 }
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": false, "name": "my-input", "value": "Legolas"} )
    
  } )

  it( `OnMount - Component will be VALID if mounted with a value with length GREATHER THAN the value set in minLength prop`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        value="Legolas"
        minLength={ 5 }
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": true, "name": "my-input", "value": "Legolas"} )
    
  } )

  it( `OnMount - Component will be INVALID if mounted with a value with length GREATHER THAN the value set in maxLength prop`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        value="Legolas"
        maxLength={ 5 }
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": false, "name": "my-input", "value": "Legolas"} )
    
  } )

  it( `OnMount - Component will be VALID if mounted with a value with length LESS THAN the value set in maxLength prop`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        value="Legolas"
        maxLength={ 10 }
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": true, "name": "my-input", "value": "Legolas"} )
    
  } )

  it( `OnMount - Component will be VALID if mounted WITHOUT an errorMessage as prop`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": true, "name": "my-input", "value": ""} )
    
  } )

  it( `OnMount - Component will be INVALID if mounted WITH an errorMessage as prop`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        errorMessage="My error message"
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": false, "name": "my-input", "value": ""} )
    
  } )

  it( `OnMount - Component will be VALID if customValidationFunc returns isValid property as TRUE`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    const _customValidationMock = () => ({ isValid: true, errorMessage: undefined })

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        customValidationFunc={ _customValidationMock }
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": true, "name": "my-input", "value": ""} )
    
  } )

  it( `OnMount - Component will be INVALID if customValidationFunc returns isValid property as FALSE`, () => {

    let fieldObjRef
    const _onMountMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    const _customValidationMock = () => ({ isValid: false, errorMessage: 'Custom validation error message' })

    mount(
      <Input
        name="my-input"
        onMount={ fieldObj => _onMountMock( fieldObj ) }
        customValidationFunc={ _customValidationMock }
      />
    )
    
    expect( fieldObjRef ).toEqual( {"isValid": false, "name": "my-input", "value": ""} )
    
  } )

  it( `OnChange - As the user inputs a value, the onChange prop function is fired`, () => {

    let fieldObjRef
    const _onChangeMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    const wrapper = mount(
      <Input
        name="my-input"
        onChange={ fieldObj => _onChangeMock( fieldObj ) }
      />
    )
    
    wrapper.find( 'input' ).simulate( 'change', { target: { value: 'My inputted value' } } )
    expect( fieldObjRef ).toEqual( {"isValid": true, "name": "my-input", "value": "My inputted value"} )
    
  } )

  it( `OnChange - As the user inputs a value, the error message is hidden`, () => {
    const wrapper = mount(
      <Input
        name="my-input"
        minLength={ 10 }
        isRequired
      />
    )
    
    const input = wrapper.find( 'input' )
    input.simulate( 'blur' )
    let errorComponent = wrapper.find( ErrorMessage )
    expect( errorComponent.length ).toBe( 1 )
    input.simulate( 'change', { target: { value: 'My inputted value' } } )
    errorComponent = wrapper.find( ErrorMessage )
    expect( errorComponent.length ).toBe( 0 )
  } )

  it( `OnChange - As the user inputs a value, the maskFunc prop masks the value`, () => {
    
    let fieldObjRef
    const _onChangeMock = fieldObj => {
      fieldObjRef = fieldObj
    }

    const _maskMock = () => 'teste'

    const wrapper = mount(
      <Input
        name="my-input"
        onChange={ _onChangeMock }
        maskFunc={ value => _maskMock( value ) }
      />
    )
    
    const input = wrapper.find( 'input' )
    input.simulate( 'change', { target: { value: '31170330' } } )
    expect( fieldObjRef.value ).toBe( 'teste' )
  } )

  it( `When value prop changes, fire onChange and onBlur`, () => {
    
    let fieldObjRef
    let onChangeFiredCount = 0
    const _onChangeMock = fieldObj => {
      onChangeFiredCount++
      fieldObjRef = fieldObj
    }

    let onBlurFiredCount = 0
    const _onBlurMock = fieldObj => {
      onBlurFiredCount++
      fieldObjRef = fieldObj
    }

    const wrapper = mount(
      <Input
        name="my-input"
        onChange={ _onChangeMock }
        onBlur={ _onBlurMock }
      />
    )
    
    wrapper.setProps( { value: 'New value' } )
    expect( onChangeFiredCount ).toBe( 1 )
    expect( onBlurFiredCount ).toBe( 1 )
    expect( fieldObjRef.value ).toBe( 'New value' )
  } )

  it( `When errorMessage prop changes, fire onChange and onBlur`, () => {
    
    let fieldObjRef
    let onChangeFiredCount = 0
    const _onChangeMock = fieldObj => {
      onChangeFiredCount++
      fieldObjRef = fieldObj
    }

    let onBlurFiredCount = 0
    const _onBlurMock = fieldObj => {
      onBlurFiredCount++
      fieldObjRef = fieldObj
    }

    const wrapper = mount(
      <Input
        name="my-input"
        onChange={ _onChangeMock }
        onBlur={ _onBlurMock }
      />
    )
    
    wrapper.setProps( { errorMessage: 'New error' } )
    expect( onChangeFiredCount ).toBe( 1 )
    expect( onBlurFiredCount ).toBe( 1 )
    expect( fieldObjRef.isValid ).toBe( false )
  } )

  it( `When isErrorVisible prop changes, show error message`, () => {

    const wrapper = mount(
      <Input
        name="my-input"
        isRequired
      />
    )
    
    wrapper.setProps( { isErrorVisible: true } ).update()
    let errorComponent = wrapper.find( ErrorMessage )
    expect( errorComponent.length ).toBe( 1 )
  } )

  it( `OnBlur - Component will call onBlur method when user blurs the input`, () => {

    let onBlurCalled = false
    const _onBlurMock = fieldObj => {
      onBlurCalled = true
    }

    const wrapper = mount(
      <Input
        name="my-input"
        value={ 'custom value' }
        onBlur={ fieldObj => _onBlurMock( fieldObj ) }
      />
    )
    
    const input = wrapper.find( 'input' )
    input.simulate('blur')
    expect( onBlurCalled ).toBe( true )
    
  } )

  it( `onUnmount - Component will call onUnmount method when unmounts`, () => {

    let onMountCallCount = 0
    let fieldObjRef
    const _onUnmountMock = fieldObj => {
      onMountCallCount++
      fieldObjRef = fieldObj
    }

    const wrapper = mount(
      <Input
        name="my-input"
        onUnmount={ fieldObj => _onUnmountMock( fieldObj ) }
      />
    )
    
    wrapper.unmount()
    expect( onMountCallCount ).toBe( 1 )
    expect( fieldObjRef ).toEqual( {"isValid": true, "name": "my-input", "value": ""} )
    
  } )

  it( `onUnmount - Component will call onUnmount method and return the last value`, () => {

    let onMountCallCount = 0
    let fieldObjRef
    const _onUnmountMock = fieldObj => {
      onMountCallCount++
      fieldObjRef = fieldObj
    }

    const wrapper = mount(
      <Input
        name="my-input"
        onUnmount={ fieldObj => _onUnmountMock( fieldObj ) }
      />
    )
    
    wrapper.find( 'input' ).simulate( 'change', { target: { value: 'Elon Musk' } } )
    wrapper.unmount()
    expect( onMountCallCount ).toBe( 1 )
    expect( fieldObjRef ).toEqual( {"isValid": true, "name": "my-input", "value": "Elon Musk"} )
    
  } )

  it( `isReadOnly - Component changes to ReadOnly mode when isReadOnly prop is set as true`, () => {

    const wrapper = mount(
      <Input
        name="my-input"
        value="Teste"
        isReadOnly
      />
    )
    
    const readOnlyComponent = wrapper.find( ReadOnly )
    expect( readOnlyComponent.length ).toBe( 1 )
    
  } )

  it( `isReadOnly - Component changes to ReadOnly mode when isReadOnly prop changes to true`, () => {

    const wrapper = mount(
      <Input
        name="my-input"
        value="Teste"
      />
    )
    
    let readOnlyComponent = wrapper.find( ReadOnly )
    wrapper.setProps( { isReadOnly: true } ).update()
    readOnlyComponent = wrapper.find( ReadOnly )
    expect( readOnlyComponent.length ).toBe( 1 )
    
  } )

  it( `Label - Component has the label rendered`, () => {

    const wrapper = mount(
      <Input
        name="my-input"
        label="My label"
      />
    )

    const scLabel = wrapper.find( Label )
    expect( scLabel.length ).toBe( 1 )
    expect( scLabel.children().text() ).toBe( 'My label ' )
    
  } )

  it( `Label - Component is required and has the label with * rendered`, () => {

    const wrapper = mount(
      <Input
        name="my-input"
        label="My label"
        isRequired
      />
    )

    const scLabel = wrapper.find( Label )
    expect( scLabel.length ).toBe( 1 )
    expect( scLabel.children().text() ).toBe( 'My label *' )
    
  } )

  it( `Description - If component has description, then render the Tooltip`, () => {

    const wrapper = mount(
      <Input
        name="my-input"
        label="My label"
        description="This is my input description"
      />
    )

    const tooltipComponent = wrapper.find( Tooltip )
    expect( tooltipComponent.length ).toBe( 1 )
    expect( tooltipComponent.props().description ).toBe( 'This is my input description' )
    
  } )

  it( `isPasteAllowed - Don't allow paste inside input when the component has the isPasteAllowed prop set to TRUE`, () => {

    const wrapper = mount(
      <Input
        name="my-input"
        label="My label"
        isPasteAllowed={ false }
      />
    )

    let input = wrapper.find( 'input' )
    input.simulate('paste')
    expect( typeof input.props().onPaste ).toBe( 'function' )
    
  } )

  it( `isDebug - If component has the isDebug prop set, then the FieldDebug should render`, () => {

    const wrapper = mount(
      <Input
        name="my-input"
        label="My label"
        isDebug
      />
    )

    const fieldDebugComponent = wrapper.find( FieldDebug )
    expect( fieldDebugComponent.length ).toBe( 1 )
    
  } )
  
  it( `OnBlur - Component with external validation will be invalid if has an error message`, () => {

    const mockfn = jest.fn()
    mockfn.mockImplementation(() => Promise.resolve({ errorMessage: 'External validation error message' }))
    
    const _onBlurMock = fieldObj => {
      expect( fieldObj ).toEqual( {"isValid": false, "name": "my-input", "value": "custom value"} )
    }

    const wrapper = mount(
      <Input
        name="my-input"
        externalValidationFunc={ () => mockfn() }
        value={ 'custom value' }
        onBlur={ fieldObj => _onBlurMock( fieldObj ) }
      />
    )
    
    const input = wrapper.find( 'input' )
    input.simulate('blur')
    
  } )

  it( `OnBlur - Component with external validation with undefined response will keep working`, () => {

    const mockLoginfn = jest.fn()
    mockLoginfn.mockImplementation(() => Promise.resolve())

    let i = 0
    const _onBlurMock = fieldObj => {
      // First run will be false
      i === 0 && expect( fieldObj ).toEqual( {"isValid": false, "name": "my-input", "value": "custom value"} )
      // After the promise, will be true
      i === 1 && expect( fieldObj ).toEqual( {"isValid": true, "name": "my-input", "value": "custom value"} )
      i++
    }

    const wrapper = mount(
      <Input
        name="my-input"
        externalValidationFunc={ () => mockLoginfn() }
        value={ 'custom value' }
        onBlur={ fieldObj => _onBlurMock( fieldObj ) }
      />
    )
    
    const input = wrapper.find( 'input' )
    input.simulate('blur')
    
  } )

  describe( '1. When the component mounts', () => {

    describe( 'Visual changes', () => {

      // - - - - 
      const customErrorMessage = 'My custom error message'
      it( `renders ErrorMessage component with the right props`, () => {
        
        const wrapper = mount( <Input name="my-input" errorMessage={ customErrorMessage } /> )
        const errorComponent = wrapper.find( ErrorMessage )
        const { fieldName, errorCode, lang, minLength, maxLength, locales } = errorComponent.props()

        expect( fieldName ).toBe( 'my-input' )
        expect( errorCode ).toBe( 'My custom error message' )
        expect( lang ).toBe( 'EN' )
        expect( minLength ).toBe( undefined )
        expect( maxLength ).toBe( undefined )
        expect( locales ).toBe( undefined )
        expect( !!errorComponent ).toBe( true )

      } )
    

      // - - - - 
      const customInputValue = 'My custom input value'
      it( `prints "${ customInputValue }" as the input value if there's a value prop`, () => {
        const wrapper = mount(
          <Input
            name="my-input"
            value={ customInputValue }
          />
        )
        const input = wrapper.find( 'input' )
        expect( input.props().value ).toBe( customInputValue )
      } )

    } )


    describe( 'Calling prop methods', () => {

      describe( 'onMount', () => {

        // - - - - 
        it( `calls "onMount" prop and send the fieldObj to the parent`, () => {

          let fieldObjRef
          const _onMountFunc = fieldObj => { fieldObjRef = fieldObj }

          mount(
            <Input
              name="my-input"
              onMount={ fieldObj => _onMountFunc( fieldObj ) }
            />
          )

          const expectedFieldObj = { name: 'my-input', value: '', isValid: true }
          expect( fieldObjRef ).toEqual( expectedFieldObj )

        } )


        // - - - - 
        it( `calls "onMount" prop and the outputed value is equal to the internal`, () => {

          let fieldObjRef
          const _onMountFunc = fieldObj => { fieldObjRef = fieldObj }

          mount(
            <Input
              name="my-input"
              onMount={ fieldObj => _onMountFunc( fieldObj ) }
            />
          )

          const expectedFieldObj = { name: 'my-input', value: '', isValid: true }
          expect( fieldObjRef ).toEqual( expectedFieldObj )

        } )

      } )

    } )

  } )

  describe( '2. Props changing', () => {


    // - - - - 
    it( `on value change, a new input value is set`, () => {
      const wrapper = mount(
        <Input
          name="my-input"
          value=""
        />
      )
      wrapper.setProps( { value: 'My new input value' } ).update()
      let input = wrapper.find( 'input' )
      expect( input.props().value ).toBe( 'My new input value' )
    } )


    // - - - - 
    it( `on errorMessage change, a new errorMessage is set`, () => {
      const wrapper = mount(
        <Input
          name="my-input"
          errorMessage=""
        />
      )
      wrapper.setProps( { errorMessage: 'My new error message' } ).update()
      const errorComponent = wrapper.find( ErrorMessage )
      expect( errorComponent.props().errorCode ).toBe( 'My new error message' )
    } )


  } )

} )