import styled, { css } from 'styled-components'

const createBreakpoints = breakpointsObj => {
  let breakpoints = ``
  for ( const breakpointMinWidth in breakpointsObj ) {
    breakpoints = `
      ${ breakpoints }
      @media( min-width: ${ breakpointMinWidth }px ) {
        width: ${ breakpointsObj[ breakpointMinWidth ] }
      }
    `
  }
  return css`${ breakpoints }`
}

export const FieldGroupLabel = styled.div`
  font-size: 14px;
  line-height: 24px;
  font-weight: 600;
  margin-bottom: 5px;

  svg {
    vertical-align: top;
  }

`

export const FlexContainer = styled.div`
  margin: 0 -8px;
  display: flex;
  flex-wrap: wrap;
  align-items: flex-end;
`

export const FormColumn = styled.div`
  width: ${ props => props.width || '100%' };
  padding: 0 8px;
  box-sizing: border-box;
  ${ props => props.breakpointsObj && createBreakpoints( props.breakpointsObj ) };
`

export const GroupTitle = styled.div`
  color: rgb(0, 0, 0);
  font-size: 13px;
  line-height: 24px;
  font-weight: 500;
  letter-spacing: 1.5px;
  text-transform: uppercase;
  font-family: Raleway, sans-serif;
  margin: 10px 0px 5px;
`

export const SectionTitle = styled.div`
  color: rgb(0, 0, 0);
  font-size: 22px;
  line-height: 24px;
  letter-spacing: 1px;
  margin: 40px 0px 15px;
`

export const ButtonsContainer = styled.div`
  margin-top: 20px;
`