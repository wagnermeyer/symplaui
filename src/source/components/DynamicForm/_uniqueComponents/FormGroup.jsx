import React from 'react'
import PropTypes from 'prop-types'
import FieldsGroup from './FieldsGroup'
import * as El from '../DynamicForm.style'
import { withFormData } from '../DynamicForm.provider'
import CustomComponentProvider from '../helpers/CustomComponent.provider'
import TooltipHOC from 'assets/styleguide/components/Tooltip'
import QuestionMark from 'assets/styleguide/styles/icons/ic-question-mark.svg'

const FormGroup = props => {
  return (
    <React.Fragment>
      <El.GroupTitle>{ props.title }</El.GroupTitle>
      <El.FlexContainer>
        {
          props.fieldsGroupsArr
          && props.fieldsGroupsArr.map( ( fieldsGroup, fieldsGroupIndex ) => {

            const indexesObj = {
              section: props.indexesObj.section,
              formGroup: props.indexesObj.formGroup,
              fieldsGroup: fieldsGroupIndex
            }

            // Custom component has priority
            if ( fieldsGroup.customComponent ) {
              const CustomComponent = fieldsGroup.customComponent
              return (
                <CustomComponentProvider
                  key={ fieldsGroupIndex }
                  lang={ props.provider.lang }
                  updateCustomFieldsGroupFunc={ fieldObj => props.provider.updateCustomFieldsGroup( fieldObj, indexesObj ) }
                  removeCustomFieldsGroupFunc={ fieldObj => props.provider.removeCustomFieldsGroup( fieldObj, indexesObj ) }
                  updateFormSectionsArrFunc={ newSectionsArr => props.provider.updateFormSectionsArr( newSectionsArr ) }
                  isErrorVisible={ props.provider.isErrorsVisible }
                  isReadOnly={ props.provider.isReadOnly }
                  { ...fieldsGroup.customProps }
                >
                  <El.FormColumn>
                    <El.FieldGroupLabel>
                      { fieldsGroup.label } { fieldsGroup.isRequired ? '*' : null }
                      { fieldsGroup.description && 
                        <TooltipHOC description={ fieldsGroup.description }>
                          <QuestionMark width={22} height={22} />
                        </TooltipHOC>
                      }
                    </El.FieldGroupLabel>
                  </El.FormColumn>
                  <CustomComponent
                    key={ fieldsGroupIndex }
                    lang={ props.provider.lang }
                    isErrorVisible={ props.provider.isErrorsVisible }
                    isReadOnly={ props.provider.isReadOnly }
                    updateCustomFieldsGroupFunc={ fieldObj => props.provider.updateCustomFieldsGroup( fieldObj, indexesObj ) }
                    removeCustomFieldsGroupFunc={ fieldObj => props.provider.removeCustomFieldsGroup( fieldObj, indexesObj ) }
                    { ...fieldsGroup.customProps } //FIXME: Change this to 'customComponent' as a prop name
                  />
                </CustomComponentProvider>
              )
            }

            // Default
            return (
              <FieldsGroup
                key={ fieldsGroupIndex }
                indexesObj={ indexesObj }
                id={ fieldsGroup.id }
                label={ fieldsGroup.label }
                description={ fieldsGroup.description }
                fieldsArr={ fieldsGroup.fieldsArr }
                isRequired={ fieldsGroup.isRequired }
                width={ fieldsGroup.width && fieldsGroup.width.default ? fieldsGroup.width.default : null }
                breakpointsObj={ fieldsGroup.width && fieldsGroup.width.breakpoints ? fieldsGroup.width.breakpoints : null }
              />
            )

          } ) }
      </El.FlexContainer>
    </React.Fragment>
  )
}

FormGroup.propTypes = {
  title: PropTypes.string,
  fieldsGroupsArr: PropTypes.array.isRequired,
  indexesObj: PropTypes.object.isRequired,
  provider: PropTypes.object.isRequired
}

export default withFormData( FormGroup )