import React from 'react'
import PropTypes from 'prop-types'
import Input from '../../../assets/styleguide/components/Input/Input'
import Textarea from '../../../assets/styleguide/components/Textarea/Textarea'
import NativeSelect from '../../../assets/styleguide/components/NativeSelect/NativeSelect'
import RadioButtons from '../../../assets/styleguide/components/RadioButtons/RadioButtons'
import CheckBox from '../../../assets/styleguide/components/CheckBox/CheckBox'
import Autocomplete from '../../../assets/styleguide/components/Autocomplete/Autocomplete'
import { withFormData } from '../DynamicForm.provider'

const FieldSwitch = props => {
  
  switch ( props.field.type ) {
    case 'RADIO_BUTTON':
      return(
        <RadioButtons
          lang={ props.lang }
          name={ props.field.name }
          description={ props.field.description }
          label={ props.field.label }
          value={ props.field.value ? props.field.value : '' }
          isRequired={ props.field.isRequired }
          isErrorVisible={ props.provider.isErrorsVisible }
          errorMessage={ props.field.errorMessage }
          optionsArr={ props.field.optionsArr }
          onChange={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          onMount={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          isReadOnly={ props.provider.isReadOnly }
        />
      )

    case 'SELECT':
      return(
        <NativeSelect
          lang={ props.lang }
          name={ props.field.name }
          description={ props.field.description }
          label={ props.field.label }
          value={ props.field.value ? props.field.value : '' }
          maxLength={ props.field.maxLength }
          minLength={ props.field.minLength }
          isRequired={ props.field.isRequired }
          validationType={ props.field.validationType }
          isErrorVisible={ props.provider.isErrorsVisible }
          errorMessage={ props.field.errorMessage }
          optionsArr={ props.field.optionsArr }
          onChange={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          onMount={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          isReadOnly={ props.provider.isReadOnly }
          visibleOnWriteOnly={ props.field.visibleOnWriteOnly }
        />
      )

    case 'CHECKBOX':
      return(
        <CheckBox
          lang={ props.lang }
          name={ props.field.name }
          description={ props.field.description }
          label={ props.field.label }
          maxChecks={ props.field.maxLength }
          minChecks={ props.field.minLength }
          isRequired={ props.field.isRequired }
          optionsArr={ props.field.optionsArr }
          errorMessage={ props.field.errorMessage }
          isErrorVisible={ props.provider.isErrorsVisible }
          onChange={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          onMount={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          isReadOnly={ props.provider.isReadOnly }
        />
      )
  
    case 'TEXTAREA':
      return(
        <Textarea
          lang={ props.lang }
          name={ props.field.name }
          description={ props.field.description }
          label={ props.field.label }
          value={ props.field.value ? props.field.value : '' }
          maxLength={ props.field.maxLength }
          minLength={ props.field.minLength }
          isRequired={ props.field.isRequired }
          validationType={ props.field.validationType }
          onBlur={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          externalValidationFunc={ props.field.externalValidationFunc }
          customValidationFunc={ props.field.customValidationFunc ? fieldObj => props.field.customValidationFunc( fieldObj ) : null }
          isErrorVisible={ props.provider.isErrorsVisible }
          errorMessage={ props.field.errorMessage }
          maskFunc={ props.field.maskFunc }
          isDisabled={ props.field.isDisabled }
          isReadOnly={ props.provider.isReadOnly }
          // isDebug
        />
      )
  
    case 'CUSTOM': {
      let CustomComponent = props.field.customComponent
      return (
        <CustomComponent
          name={ props.field.name }
          onMount={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          onUnmount={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          onChange={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          onBlur={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          onFocus={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          { ...props.field.customProps }
        />
      )
    }

    default:
      if(props.field.optionsArr && props.field.optionsArr.length > 0 && props.field.type === 'TEXT') {
        return(
          <Autocomplete
            lang={ props.lang }
            name={ props.field.name }
            description={ props.field.description }
            label={ props.field.label }
            value={ props.field.value ? props.field.value : '' }
            isRequired={ props.field.isRequired }
            onBlur={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
            isErrorVisible={ props.provider.isErrorsVisible }
            errorMessage={ props.field.errorMessage }
            optionsArr={ props.field.optionsArr }
            isDisabled={ props.field.isDisabled }
            isReadOnly={ props.provider.isReadOnly }
            // isDebug
          />
        )
      }
      return(
        <Input
          lang={ props.lang }
          name={ props.field.name }
          description={ props.field.description }
          label={ props.field.label }
          value={ props.field.value ? props.field.value : '' }
          maxLength={ props.field.maxLength }
          minLength={ props.field.minLength }
          isRequired={ props.field.isRequired }
          validationType={ props.field.validationType }
          onBlur={ fieldObj => props.provider.handleOutput( fieldObj, props.indexesObj ) }
          externalValidationFunc={ props.field.externalValidationFunc }
          customValidationFunc={ props.field.customValidationFunc ? fieldObj => props.field.customValidationFunc( fieldObj ) : null }
          isErrorVisible={ props.provider.isErrorsVisible }
          errorMessage={ props.field.errorMessage }
          maskFunc={ props.field.maskFunc }
          isDisabled={ props.field.isDisabled }
          isReadOnly={ props.provider.isReadOnly }
          noPaste={ props.field.noPaste }
          // isDebug
        />
      )
      
  }

}

FieldSwitch.propTypes = {
  lang: PropTypes.string,
  indexesObj: PropTypes.object,
  field: PropTypes.object.isRequired,
  provider: PropTypes.object // withFormData
}

export default withFormData( FieldSwitch )