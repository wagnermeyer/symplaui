import React from 'react'
import PropTypes from 'prop-types'
import FieldSwitch from '../_uniqueComponents/FieldSwitch'
import * as El from '../DynamicForm.style'
import TooltipHOC from 'assets/styleguide/components/Tooltip'
import QuestionMark from 'assets/styleguide/styles/icons/ic-question-mark.svg'

const FieldsGroup = props => {

	return(
    <El.FormColumn
      width={ props.width }
      breakpointsObj={ props.breakpointsObj }
    >
      { props.label &&
        <El.FieldGroupLabel>
          { props.label } { props.isRequired ? '*' : null }
          { props.description && 
            <TooltipHOC description={ props.description }>
              <QuestionMark width={22} height={22} />
            </TooltipHOC>
          }
        </El.FieldGroupLabel>
      }
      <El.FlexContainer>
        {
          props.fieldsArr
          && props.fieldsArr.map( ( field, fieldIndex ) => (
            <El.FormColumn
              key={ fieldIndex }
              breakpointsObj={ field.width && field.width.breakpoints ? field.width.breakpoints : null }
              width={ field.width && field.width.default ? field.width.default : null }
            >
              <FieldSwitch
                key={ fieldIndex }
                field= { field }
                indexesObj={ props.indexesObj }
                lang={ 'PT' }
              />
            </El.FormColumn>
          ) )
        }
      </El.FlexContainer>
    </El.FormColumn>
	)

}

FieldsGroup.propTypes = {
  width: PropTypes.string,
  breakpointsObj: PropTypes.object,
  label: PropTypes.string,
  description: PropTypes.string,
  isRequired: PropTypes.bool,
  fieldsArr: PropTypes.array  ,
  indexesObj: PropTypes.object.isRequired,
  isErrorsVisible: PropTypes.bool
}

export default FieldsGroup