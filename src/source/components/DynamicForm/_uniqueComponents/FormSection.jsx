import React from 'react'
import PropTypes from 'prop-types'
import * as El from '../DynamicForm.style'
import FormGroup from './FormGroup'
import { withFormData } from '../DynamicForm.provider'
import CustomComponentProvider from '../helpers/CustomComponent.provider'

const FormSection = props => {

  return(
    <React.Fragment>
      <El.SectionTitle>{ props.title }</El.SectionTitle>
      {
        props.formGroupsArr
        && props.formGroupsArr.map( ( formGroup, formGroupIndex ) => {

        const indexesObj = {
          section: props.indexesObj.section,
          formGroup: formGroupIndex
        }

        // Custom component has priority
        if ( formGroup.customComponent ) {
          const CustomComponent = formGroup.customComponent
          return (
            <CustomComponentProvider
              key={ formGroupIndex }
              lang={ props.provider.lang }
              updateCustomFormGroupFunc={ fieldObj => props.provider.updateCustomFormGroup( fieldObj, indexesObj ) }
              removeCustomFormGroupFunc={ fieldObj => props.provider.removeCustomFormGroup( fieldObj, indexesObj ) }
              updateFormSectionsArrFunc={ newSectionsArr => props.provider.updateFormSectionsArr( newSectionsArr ) }
              isErrorVisible={ props.provider.isErrorsVisible }
              isReadOnly={ props.provider.isReadOnly }
              { ...formGroup.customProps }
            >
              <CustomComponent
                key={ formGroupIndex }
                lang={ props.provider.lang }
                isErrorVisible={ props.provider.isErrorsVisible }
                isReadOnly={ props.provider.isReadOnly }
                updateCustomFormGroupFunc={ fieldObj => props.provider.updateCustomFormGroup( fieldObj, indexesObj ) }
                removeCustomFormGroupFunc={ fieldObj => props.provider.removeCustomFormGroup( fieldObj, indexesObj ) }
                { ...formGroup.customProps } //FIXME: Change this to 'customComponent' as a prop name
              />
            </CustomComponentProvider>
          )
        }

        // Default
        return (
          <FormGroup
            key={ formGroupIndex }
            indexesObj={ indexesObj }
            title={ formGroup.title }
            fieldsGroupsArr={ formGroup.fieldsGroupsArr }
          />
        )

      } ) }
    </React.Fragment>
  )

}

FormSection.propTypes = {
  title: PropTypes.string,
  formGroupsArr: PropTypes.array,
  indexesObj: PropTypes.object.isRequired,
  provider: PropTypes.object
}

export default withFormData(FormSection)