import React from 'react'
import PropTypes from 'prop-types'
import * as El from '../DynamicForm.style'
import { Button } from '../../../assets/styleguide/styles'
import { withFormData } from '../DynamicForm.provider'
import FormSection from './FormSection'

const FormContainer = props => {
  return (
    <React.Fragment>
      <h1>{ props.provider.title }</h1>
      { props.provider.sectionsArr.map( ( section, sectionIndex ) => (
          <FormSection
            key={ sectionIndex }
            indexesObj={ {
              section: sectionIndex
            } }
            title={ section.title }
            formGroupsArr={ section.formGroupsArr }
          />
        ) )
      }
      <El.ButtonsContainer>
        <Button
          type="button"
          big 
          success
          onClick={ () => props.provider.onSubmit() }
        >
          { props.provider.labels.submit || 'Enviar' }
        </Button>
        <Button
          type="button"
          big 
          link
          onClick={ () => props.provider.onCancel() }
        >
          { props.provider.labels.cancel || 'Cancelar' }
        </Button>
      </El.ButtonsContainer>
    </React.Fragment>
  )
}

FormContainer.propTypes = {
  provider: PropTypes.object
}

export default withFormData( FormContainer )