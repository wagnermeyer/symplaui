import React, { createContext, Component } from 'react'
import PropTypes from 'prop-types'
import { DynamicFormStructure } from './index'
import update from 'immutability-helper'
import locales from './lang/DynamicForm.json'

const DynamicFormContext = createContext()
const DynamicFormConsumer = DynamicFormContext.Consumer

class DynamicFormProvider extends Component {

  constructor(props) {
    super(props)
    this.handleOutput = this.handleOutput.bind( this )
    this.onSubmit = this.onSubmit.bind( this )
    this.onCancel = this.onCancel.bind( this )
    this.updateCustomFormGroup = this.updateCustomFormGroup.bind( this )
    this.removeCustomFormGroup = this.removeCustomFormGroup.bind( this )
    this.updateCustomFieldsGroup = this.updateCustomFieldsGroup.bind( this )
    this.updateSectionsArr = this.updateSectionsArr.bind( this )
    this.customFormGroupsObj = {}
    this.customFieldsGroupsObj = {}
    this.formattedCustomFormGroupsArr = []
    this.formTempStorageObj = {}
    this.uniqueFieldsObj = {}
    this.uniqueFieldsMapObj = {}
    this.uniqueFieldsLocationsObj = {}
    this.formOutput = this.updateFormOutput( this.props )
    this.state = {
      formStructure: {
        sectionsArr: props.sectionsArr
      },
      isErrorsVisible: false
    }
  }

  componentDidUpdate( prevProps, prevState ) {
    // Update form structure
    if ( JSON.stringify(prevProps.sectionsArr) !== JSON.stringify(this.props.sectionsArr) ) {
      this.setState( { formStructure: { sectionsArr: this.props.sectionsArr } } )
    }
    // Recreate formOutput
    if ( JSON.stringify( prevState.formStructure ) !== JSON.stringify( this.state.formStructure ) ) {
      this.updateFormOutput( this.state.formStructure )
    }
  }

  getValueFromField(tempFieldObj) {
    if(tempFieldObj && tempFieldObj.value) {
      return tempFieldObj.value  
    }
    else if(tempFieldObj && tempFieldObj.valuesArr) {
      return tempFieldObj.valuesArr 
    }
    return ""
  }

  updateFormOutput( formStructure ) {

    const { sectionsArr } = formStructure
    let formOutput = { sections: [] }
    
    // Create first level
    sectionsArr.forEach( ( section, sectionIndex ) => {

      formOutput
        .sections
        .push({
          _id: section.id,
          formGroups: []
        })
        
      // Create second level
      section.formGroupsArr
      && section.formGroupsArr.forEach( ( formGroup, formGroupIndex ) => {

        formOutput
          .sections[ sectionIndex ]
          .formGroups
          .push({
            _id: formGroup.id,
            fieldsGroups: []
          })

        // Create third level
        formGroup.fieldsGroupsArr
        && formGroup.fieldsGroupsArr.forEach( ( fieldsGroup, fieldsGroupIndex ) => {
          
          formOutput
            .sections[ sectionIndex ]
            .formGroups[ formGroupIndex ]
            .fieldsGroups
            .push({
              _id: fieldsGroup.id,
              fields: []
            })

          // Create fourth level
          fieldsGroup.fieldsArr
          && fieldsGroup.fieldsArr.forEach( ( field ) => {

            const tempFieldObj = 
              (
                this.formTempStorageObj[ section.id ]
                && this.formTempStorageObj[ section.id ][ formGroup.id ]
                && this.formTempStorageObj[ section.id ][ formGroup.id ][ fieldsGroup.id ]
                && this.formTempStorageObj[ section.id ][ formGroup.id ][ fieldsGroup.id ][ field.name ]
              )
              && this.formTempStorageObj[ section.id ][ formGroup.id ][ fieldsGroup.id ][ field.name ]

            const validation = tempFieldObj ? { isValid: tempFieldObj.isValid } : null

            formOutput
              .sections[ sectionIndex ]
              .formGroups[ formGroupIndex ]
              .fieldsGroups[ fieldsGroupIndex ]
              .fields
              .push( {
                name: field.name,
                value: this.getValueFromField(tempFieldObj),
                ...validation
              } )

          } )
          
        } )
      })

    } )

    // Add custom form groups
    Object.keys( this.customFormGroupsObj ).forEach( customFormGroupKey => {
      Object.keys( this.customFormGroupsObj[ customFormGroupKey ] ).forEach( ( key, keyIndex ) => {
        const { locationIndexes } = this.customFormGroupsObj[ customFormGroupKey ][ key ]
        const { fieldObj } = this.customFormGroupsObj[ customFormGroupKey ][ key ]
        
        formOutput
          .sections[ locationIndexes.section ]
          .formGroups[ locationIndexes.formGroup ]
          .fieldsGroups.push( {
            id: `CUSTOM_FIELDS_GROUP_${ keyIndex }`,
            fields: [
              {
                name: fieldObj.name,
                value: fieldObj.value || fieldObj.valuesArr,
                isValid: fieldObj.isValid
              }
            ]
          } )
      } )
    } )

    // Add custom fields groups
    Object.keys( this.customFieldsGroupsObj ).forEach( customFormGroupKey => {
      Object.keys( this.customFieldsGroupsObj[ customFormGroupKey ] ).forEach( ( key ) => {
        const { locationIndexes } = this.customFieldsGroupsObj[ customFormGroupKey ][ key ]
        const { fieldObj } = this.customFieldsGroupsObj[ customFormGroupKey ][ key ]
        
        formOutput
          .sections[ locationIndexes.section ]
          .formGroups[ locationIndexes.formGroup ]
          .fieldsGroups[ locationIndexes.fieldsGroup ]
          .fields.push(
            {
              name: fieldObj.name,
              value: fieldObj.value || fieldObj.valuesArr,
              isValid: fieldObj.isValid
            }
          )
      } )
    } )

    this.formOutput = formOutput
    this.props.isDebug && this.setState( { formOutput } )
  }

  onSubmit() {

    this.setState( { isErrorsVisible: true } )

    let validationsArr = []
    const validationErrorsObj = {}
    
    this.formOutput.sections.forEach( ( section, sectionIndex ) => {
      return section.formGroups && section.formGroups.forEach( ( formGroup, formGroupIndex ) => {
        return formGroup.fieldsGroups && formGroup.fieldsGroups.forEach( ( fieldGroup, fieldGroupIndex ) => {
          return fieldGroup.fields && fieldGroup.fields.forEach( ( field, fieldIndex ) => {
            if ( !field.isValid ) validationErrorsObj[ `${sectionIndex}${formGroupIndex}${fieldGroupIndex}${fieldIndex}_${field.name}` ] = field.isValid
            validationsArr.push( field.isValid )
          } )
        } )
      } )
    } )

    const isFormValid = !validationsArr.filter( isValid => !isValid ).length > 0

    if ( !this.props.allowSubmitWithErrors ) {

      if ( !isFormValid ) {

        const getDistanceToTop = el => el.getBoundingClientRect().top
        const firstFieldError = Object.keys(validationErrorsObj)[0].replace(/^[0-9]*_/, '')
        const inputErrorEl = document.querySelector(`input[name="${ firstFieldError }"]`)
        const selectErrorEl = document.querySelector(`select[name="${ firstFieldError }"]`)
        const textareaErrorEl = document.querySelector(`textarea[name="${ firstFieldError }"]`)
        const fieldEl = inputErrorEl || selectErrorEl || textareaErrorEl
        const offsetTop = 100
        const fieldDistanceToTop = getDistanceToTop( fieldEl ) - offsetTop

        window.scrollBy( {
          top: fieldDistanceToTop,
          left: 0,
          behavior: 'smooth'
        } )
    
        const checkIfDone = setInterval(() => {
          const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2
          if ( getDistanceToTop( fieldEl ) === offsetTop || atBottom) {
            inputErrorEl && inputErrorEl.focus()
            textareaErrorEl && textareaErrorEl.focus()
            clearInterval(checkIfDone)
          }
        }, 100)

      }
    }

    (this.props.allowSubmitWithErrors || isFormValid)
    && this.props.onSubmit
    && this.props.onSubmit( this.formOutput )

  }

  onCancel() {
    this.setState( { isErrorsVisible: false } )

    this.props.onCancel
    && this.props.onCancel()
  }

  updateCustomFieldsGroup( fieldObj, locationIndexes ) {
    const formStructure = new DynamicFormStructure( this.state.formStructure.sectionsArr )
    const sectionID = formStructure.getSectionIdByIndex( locationIndexes.section )
    this.customFieldsGroupsObj[ sectionID ] = { ...this.customFieldsGroupsObj[ sectionID ] }
    this.customFieldsGroupsObj[ sectionID ][ fieldObj.name ] = { fieldObj, locationIndexes }
    this.updateFormOutput( this.state.formStructure )
  }

  removeCustomFieldsGroup( fieldObj, locationIndexes ) {
    if ( this.state ) {
      const formStructure = new DynamicFormStructure( this.state.formStructure.sectionsArr )
      const sectionID = formStructure.getSectionIdByIndex( locationIndexes.section )
      if ( this.customFieldsGroupsObj[ sectionID ] ) delete this.customFieldsGroupsObj[ sectionID ][ fieldObj.name ]
      this.updateFormOutput( this.state.formStructure )
    }
  }

  updateCustomFormGroup( fieldObj, locationIndexes ) {
    const sectionId =
      this.state
        .formStructure
        .sectionsArr[ locationIndexes.section ]
        .id
    
    this.customFormGroupsObj[ sectionId ] = { ...this.customFormGroupsObj[ sectionId ] }
    this.customFormGroupsObj[ sectionId ][ fieldObj.name ] = { fieldObj, locationIndexes }
    this.updateFormOutput( this.state.formStructure )
  }

  removeCustomFormGroup( fieldObj, locationIndexes ) {
    const formStructure = new DynamicFormStructure( this.state.formStructure.sectionsArr )
    const sectionID = formStructure.getSectionIdByIndex( locationIndexes.section )
    if ( this.customFormGroupsObj[ sectionID ] ) delete this.customFormGroupsObj[ sectionID ][ fieldObj.name ]
    this.updateFormOutput( this.state.formStructure )
  }

  handleOutput( outputObj, locationIndexes ) {

    const sectionIndex = locationIndexes.section
    const formGroupIndex = locationIndexes.formGroup
    const fieldsGroupIndex = locationIndexes.fieldsGroup

    const formStructure = new DynamicFormStructure( this.state.formStructure.sectionsArr )
    const sectionID = formStructure.getSectionIdByIndex( sectionIndex )
    const formGroupID = formStructure.getFormGroupIdByIndex( sectionIndex, formGroupIndex )
    const fieldsGroupID = formStructure.getFieldsGroupIdByIndex( sectionIndex, formGroupIndex, fieldsGroupIndex )

    // Populate the temporary storage
    this.formTempStorageObj = {
      ...this.formTempStorageObj,
      [ sectionID ]: {
        ...this.formTempStorageObj[ sectionID ],
        [ formGroupID ]: {
          ...this.formTempStorageObj[ sectionID ]
          && this.formTempStorageObj[ sectionID ][ formGroupID ],
          [ fieldsGroupID ]: {
            ...this.formTempStorageObj[ sectionID ]
            && this.formTempStorageObj[ sectionID ][ formGroupID ]
            && this.formTempStorageObj[ sectionID ][ formGroupID ][ fieldsGroupID ],
            [ outputObj.name ]: outputObj
          }
        }
      }
    }

    // Handle unique field
    const structureFieldsArr =
      this.state
        .formStructure
        .sectionsArr[ sectionIndex ]
        .formGroupsArr[ formGroupIndex ]
        .fieldsGroupsArr[ fieldsGroupIndex ]
        .fieldsArr

    const structureFieldObj = structureFieldsArr.find( field => field.name === outputObj.name )

    if ( structureFieldObj.uniqueRef ) {

      const oldValue = this.uniqueFieldsMapObj[ structureFieldObj.uniqueRef ]
        && this.uniqueFieldsMapObj[ structureFieldObj.uniqueRef ][ outputObj.name ]

      this.uniqueFieldsMapObj[ structureFieldObj.uniqueRef ] = {
        ...this.uniqueFieldsMapObj[ structureFieldObj.uniqueRef ],
        [ outputObj.name ]: outputObj.value
      }

      this.uniqueFieldsLocationsObj[ structureFieldObj.uniqueRef ] = {
        ...this.uniqueFieldsLocationsObj[ structureFieldObj.uniqueRef ],
        [ outputObj.name ]: locationIndexes
      }

      if ( outputObj.value ) { // If there's a value on the input

        const isFieldNameAlreadySet = this.uniqueFieldsObj[ structureFieldObj.uniqueRef ]
          && this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ outputObj.value ]
          && this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ outputObj.value ].includes( outputObj.name )

        if (
          this.uniqueFieldsObj[ structureFieldObj.uniqueRef ]
          && this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ]
        ) {

          let newSectionsArr = this.state.formStructure.sectionsArr

          this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ].forEach( fieldName => {
            
            const thisFieldLocation = this.uniqueFieldsLocationsObj[ structureFieldObj.uniqueRef ][ fieldName ]

            const thisStructureFieldsArr =
              this.state
                .formStructure
                .sectionsArr[ thisFieldLocation.section ]
                .formGroupsArr[ thisFieldLocation.formGroup ]
                .fieldsGroupsArr[ thisFieldLocation.fieldsGroup ]
                .fieldsArr

            const fieldIndex =
              thisStructureFieldsArr
                .findIndex( field => field.name === fieldName )

            const thisFieldObj = thisStructureFieldsArr[ fieldIndex ]

            const newFieldObj = {
              ...thisFieldObj,
              errorMessage: undefined
            }

            newSectionsArr = update( newSectionsArr, {
              [ thisFieldLocation.section ]: {
                formGroupsArr: {
                  [ thisFieldLocation.formGroup ]: {
                    fieldsGroupsArr: {
                      [ thisFieldLocation.fieldsGroup ]: {
                        fieldsArr: {
                          [ fieldIndex ]: {
                            $set: newFieldObj
                          }
                        }
                      }
                    }
                  }
                }
              }
            } )

          } )
 
          if ( oldValue && outputObj.value !== oldValue ) {
  
            const fieldNameIndex = this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ].indexOf( outputObj.name )
            this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ].splice( fieldNameIndex, 1 )
  
            if ( this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ].length === 0 ) {
              delete this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ]
            }
  
          }

          if (
            this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ]
            && this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ].length > 1
          ) {
            this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ oldValue ].forEach( fieldName => {
              
              const thisFieldLocation = this.uniqueFieldsLocationsObj[ structureFieldObj.uniqueRef ][ fieldName ]

              const thisStructureFieldsArr =
                this.state
                  .formStructure
                  .sectionsArr[ thisFieldLocation.section ]
                  .formGroupsArr[ thisFieldLocation.formGroup ]
                  .fieldsGroupsArr[ thisFieldLocation.fieldsGroup ]
                  .fieldsArr

              const fieldIndex =
                thisStructureFieldsArr
                  .findIndex( field => field.name === fieldName )

              const thisFieldObj = thisStructureFieldsArr[ fieldIndex ]

              const newFieldObj = {
                ...thisFieldObj,
                errorMessage: locales[ this.props.lang ][ 'UNIQUE_FIELD_ERROR' ]
              }

              newSectionsArr = update( newSectionsArr, {
                [ thisFieldLocation.section ]: {
                  formGroupsArr: {
                    [ thisFieldLocation.formGroup ]: {
                      fieldsGroupsArr: {
                        [ thisFieldLocation.fieldsGroup ]: {
                          fieldsArr: {
                            [ fieldIndex ]: {
                              $set: newFieldObj
                            }
                          }
                        }
                      }
                    }
                  }
                }
              } )

            } )
          }

          this.setState({
            formStructure: {
              sectionsArr: newSectionsArr
            }
          })

        }


        if ( !isFieldNameAlreadySet ) {

          const uniqueFieldNamesArr =
            this.uniqueFieldsObj[ structureFieldObj.uniqueRef ]
            && this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ outputObj.value ]
              ? this.uniqueFieldsObj[ structureFieldObj.uniqueRef ][ outputObj.value ]
              : []

          this.uniqueFieldsObj[ structureFieldObj.uniqueRef ] = {
            ...this.uniqueFieldsObj[ structureFieldObj.uniqueRef ],
            [ outputObj.value ]: [
              ...uniqueFieldNamesArr,
              outputObj.name
            ]
          }

        }
        
      }

    }

    // Send the output to the parent
    this.props.onFieldChange
    && this.props.onFieldChange( {
      ...outputObj,
      locationIndexes
    } )

    this.updateFormOutput( this.state.formStructure )
    this.props.onFormChange
    && this.props.onFormChange( this.formOutput )

  }

  updateSectionsArr( sectionsArr ) {
    this.setState( {
      formStructure: {
        sectionsArr
      }
    } )
  }

  render() {
    return(
      <DynamicFormContext.Provider
        value={
          {
            provider: {
              lang: this.props.lang,
              labels: this.props.labels,
              sectionsArr: this.state.formStructure.sectionsArr,
              handleOutput: this.handleOutput,
              updateCustomFormGroup: this.updateCustomFormGroup,
              removeCustomFormGroup: this.removeCustomFormGroup,
              updateCustomFieldsGroup: this.updateCustomFieldsGroup,
              removeCustomFieldsGroup: this.removeCustomFieldsGroup,
              title: this.props.title,
              isErrorsVisible: this.state.isErrorsVisible,
              onSubmit: this.onSubmit,
              onCancel: this.onCancel,
              isReadOnly: this.props.isReadOnly,
              updateFormSectionsArr: this.updateSectionsArr
            }
          }
        }
      >
        { this.props.children }
        {
          this.props.isDebug &&
          <pre>
            { JSON.stringify(this.state.formOutput, null, 4) }
          </pre>
        }
      </DynamicFormContext.Provider>
    )
  }

}

DynamicFormProvider.propTypes = {
  lang: PropTypes.string,
  title: PropTypes.string,
  labels: PropTypes.object,
  sectionsArr: PropTypes.array.isRequired,
  onFieldChange: PropTypes.func,
  onFormChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  children: PropTypes.node,
  isReadOnly: PropTypes.bool,
  isDebug: PropTypes.bool,
  allowSubmitWithErrors: PropTypes.bool
}

export default DynamicFormProvider

const withFormData = Component => {
  return function ComponentWrapper( props ) {
    return (
      <DynamicFormConsumer>
        {
          ( contexts ) => (
            <Component
              { ...contexts }
              { ...props }
            />
          )
        }
      </DynamicFormConsumer>
    )
  }
}

export { withFormData }