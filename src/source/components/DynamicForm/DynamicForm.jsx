import React from 'react'
import PropTypes from 'prop-types'
import FormProvider from './DynamicForm.provider'
import FormContainer from './_uniqueComponents/FormContainer'

const DynamicForm = props => {

  return (
    <FormProvider
      lang={ props.lang }
      labels={ props.labels }
      title={ props.title }
      sectionsArr={ props.sectionsArr }
      onSubmit={ props.onSubmit ? formOutput => props.onSubmit( formOutput ) : null }
      onCancel={ props.onCancel ? formOutput => props.onCancel( formOutput ) : null }
      onFormChange={ props.onFormChange ? formOutput => props.onFormChange( formOutput ) : null }
      onFieldChange={ props.onFieldChange ? fieldObj => props.onFieldChange( fieldObj ) : null }
      isReadOnly={ props.isReadOnly }
      allowSubmitWithErrors={ props.allowSubmitWithErrors }
      isDebug={ props.isDebug } 
    >
      <FormContainer/>
    </FormProvider>
  )

}

DynamicForm.propTypes = {
  lang: PropTypes.string,
  labels: PropTypes.object,
  onFormChange: PropTypes.func,
  onFieldChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  title: PropTypes.string,
  sectionsArr: PropTypes.array.isRequired,
  isReadOnly: PropTypes.bool,
  isDebug: PropTypes.bool,
  allowSubmitWithErrors: PropTypes.bool
}

export default DynamicForm