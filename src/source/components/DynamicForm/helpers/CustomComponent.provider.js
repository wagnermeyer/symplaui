import React, { createContext, Component } from 'react'
import PropTypes from 'prop-types'

const CustomComponentContext = createContext()
const CustomComponentConsumer = CustomComponentContext.Consumer

class CustomComponentProvider extends Component {

  constructor( props ) {
    super( props )
    this.state = {
      propsWithoutChildren: this.props
    }
  }
  
  componentDidUpdate( prevProps ) {
    if ( prevProps !== this.props ) {
      this.setState( { propsWithoutChildren: this.props } )
    }
  }

  render() {
    return (
      <CustomComponentContext.Provider
        value={ { customComponent: this.state.propsWithoutChildren } }
      >
        { this.props.children }
      </CustomComponentContext.Provider>
    )
  }
}

CustomComponentProvider.propTypes = {
  children: PropTypes.node
}

export default CustomComponentProvider

const withCustomComponent = Component => {
  return function ComponentWrapper( props ) {
    return (
      <CustomComponentConsumer>
        {
          ( contexts ) => (
            <Component
              { ...contexts }
              { ...props }
            />
          )
        }
      </CustomComponentConsumer>
    )
  }
}

export { withCustomComponent }