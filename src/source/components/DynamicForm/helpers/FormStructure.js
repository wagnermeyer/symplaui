import update from 'immutability-helper'

class FormStructure {
  
  constructor( sectionsArr ) {
    this.sectionsArr = sectionsArr
  }

  
  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method _getSectionIndexByID
   * @description Gets the section index based on the section ID
   * @param {string} id 
   * @returns {number}
   */
  _getSectionIndexByID( id ) {
    const sectionIndex = this.sectionsArr.findIndex( sectionObj => sectionObj.id === id )
    return sectionIndex
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method _getSectionIndex
   * @description Gets the section index
   * @param {string|number} reference 
   * @returns {number}
   */
  _getSectionIndex( reference ) {
    const sectionIndex =
      typeof reference === 'string'
      ? this._getSectionIndexByID( reference )
      : reference
    return sectionIndex
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method _getInsertIndex
   * @description Sets the index where the object will be inserted, based on the position and reference
   * @param {string} position 
   * @param {number|string} referenceIndex 
   */
  _getInsertIndex( position, referenceIndex ) {
    const insertIndex =
      position === 'BEFORE'
      ? referenceIndex
      : referenceIndex + 1
    return insertIndex
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method _getFormGroupIndexByID
   * @description Gets the form group index based on the form group ID
   * @param {number} sectionIndex 
   * @param {string} formGroupID 
   * @returns {number}
   */
  _getFormGroupIndexByID( sectionIndex, formGroupID ) {
    return this.sectionsArr[ sectionIndex ].formGroupsArr.findIndex( formGroupObj => formGroupObj.id === formGroupID )
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method _getFormGroupIndex
   * @description Gets the form group index
   * @param {number} sectionIndex 
   * @param {number|string} formGroupReference
   * @returns {number}
   */
  _getFormGroupIndex( sectionIndex, formGroupReference ) {
    const formGroupIndex =
      typeof formGroupReference === 'string'
      ? this._getFormGroupIndexByID( sectionIndex, formGroupReference )
      : formGroupReference
    return formGroupIndex
  }

  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method getSectionIdByIndex
   * @description Returns the section ID based on the index
   * @param {number} sectionIndex
   * @returns {string}
   */
  getSectionIdByIndex( sectionIndex ) {
    const sectionID = this.sectionsArr[ sectionIndex ].id
    return sectionID
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @method getFormGroupIdByIndex
   * @description Returns the form group ID based on the section index and form group index
   * @param {number} sectionIndex 
   * @param {number} formGroupIndex
   * @returns {string}
   */
  getFormGroupIdByIndex( sectionIndex, formGroupIndex ) {
    const formGroupID =
      this.sectionsArr[ sectionIndex ]
        .formGroupsArr[ formGroupIndex ]
        .id
    return formGroupID
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @method getFieldsGroupIdByIndex
   * @param {number} sectionIndex 
   * @param {number} formGroupIndex 
   * @param {number} fieldsGroupIndex 
   * @returns {string}
   */
  getFieldsGroupIdByIndex( sectionIndex, formGroupIndex, fieldsGroupIndex ) {
    const fieldsGroupID =
      this.sectionsArr[ sectionIndex ]
        .formGroupsArr[ formGroupIndex ]
        .fieldsGroupsArr[ fieldsGroupIndex ]
        .id
    return fieldsGroupID
  }
  

  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method addSection
   * @description Adds a form section
   * @param {object} sectionObj
   * @param {object} options
   * @param {function} callback
   * @returns {array}
   */
  addSection(
    sectionObj,
    options = {
      position: 'AFTER',
      reference: this.sectionsArr.length - 1
    },
    callback
  ) {

    const { position, reference } = options
    const referenceIndex = this._getSectionIndex( reference )
    const insertIndex = this._getInsertIndex( position, referenceIndex )
    const newSectionsArr = [...this.sectionsArr]

    newSectionsArr.splice( insertIndex, 0, sectionObj ) //FIXME: immutability-helper
    callback && callback( newSectionsArr )
    return newSectionsArr

  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method removeSection
   * @description Removes a form section
   * @param {string|number} reference,
   * @param {function} callback
   * @return {array}
   */
  removeSection( reference, callback ) {

    const referenceIndex = this._getSectionIndex( reference )
    const newSectionsArr = [...this.sectionsArr]
    referenceIndex >= 0 && newSectionsArr.splice( referenceIndex, 1 ) //FIXME: immutability-helper
    callback && callback( newSectionsArr )
    return newSectionsArr
    
  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method addFormGroup
   * @description Adds a new form group
   * @param {object} formGroupObj
   * @param {object} options
   * @param {function} callback
   * @returns {array}
   */
  addFormGroup(
    formGroupObj,
    options = {
      position: 'AFTER',
      sectionReference: this.sectionsArr.length - 1,
      formGroupReference: this.sectionsArr[ this.sectionsArr.length - 1 ].formGroupsArr.length - 1
    },
    callback
  ) {

    const {
      position,
      sectionReference,
      formGroupReference
    } = options

    const sectionIndex = this._getSectionIndex( sectionReference )
    const formGroupIndex = this._getFormGroupIndex( sectionIndex, formGroupReference )
    const insertFormGroupIndex = this._getInsertIndex( position, formGroupIndex )

    const newSectionsArr = 
      update( this.sectionsArr, {
        [ sectionIndex ]: {
          formGroupsArr: {
            $splice: [ [ insertFormGroupIndex, 0, formGroupObj ] ]
          }
        }
      } )

    callback && callback( newSectionsArr )
    return newSectionsArr

  }


  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   * @method removeFormGroup
   * @description Removes a form group
   * @param {object} sectionObj
   * @param {object} options
   * @param {function} callback
   * @returns {array}
   */
  removeFormGroup( { sectionReference, formGroupReference }, callback ) {

    const sectionIndex = this._getSectionIndex( sectionReference )
    const formGroupIndex = this._getFormGroupIndex( sectionIndex, formGroupReference )

    const newSectionsArr =
      update( this.sectionsArr, {
        [ sectionIndex ]: {
          formGroupsArr: {
            $splice: [ [ formGroupIndex, 1 ] ]
          }
        }
      } )

    callback && callback( newSectionsArr )
    return newSectionsArr

  }

  updateFormGroupCustomProps( { sectionReference, formGroupReference, newCustomProp }, callback ) {

    // Handling errors
    if ( !sectionReference ) {
      console.error( 'updateFormGroupCustomProps', '"sectionID" is required' )
      return
    }
    
    if ( !formGroupReference ) {
      console.error( 'updateFormGroupCustomProps', '"formGroupID" is required' )
      return
    }

    if ( !newCustomProp ) {
      console.error( 'updateFormGroupCustomProps', '"newCustomProp" is required' )
      return
    }

    const sectionIndex = this._getSectionIndex( sectionReference )
    const formGroupIndex = this._getFormGroupIndex( sectionIndex, formGroupReference )
    const customPropsCopy = {
      ...this.sectionsArr[ sectionIndex ]
        .formGroupsArr[ formGroupIndex ]
        .customProps
    }

    const newSectionsArr =
      update( this.sectionsArr, {
        [ sectionIndex ]: {
          formGroupsArr: {
            [ formGroupIndex ]: {
              customProps: {
                $set: {
                  ...customPropsCopy,
                  ...newCustomProp
                }
              }
            }
          }
        }
      } )

    callback && callback( newSectionsArr )
    return newSectionsArr

  }

}

export default FormStructure