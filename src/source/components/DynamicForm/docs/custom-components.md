# Custom Components
The `DynamicForm` was designed to allow to inject your own components. Today we can implement 2 types of custom components:
1. Custom Form Group Component
2. Custom Field Component

## 1. Custom Form Group Component
There's a little difference between a **Custom Form Group Component** WITH fields and a **Custom Form Group Component** WITHOUT fields.

When you insert a custom component WITH fields, you need to use some props that allow the `DynamicForm` engine to understand the new fields that are being used inside the custom component.

Otherwise, if you insert a custom component WITHOUT fields, you don't need to inform the `DynamicForm`, because there's nothing he should care about. In this case it would pretty much simple to insert.

### 1.1. With fields
If you need something more specific inside your `DynamicForm`, you can create your own components exactly the way you want.

To achieve this, you have some simple rules to follow while you create your own component.

In this example, we are building a simple **Custom Form Group Component** with a '**First name**' and a '**Last name**' input.

#### 1.1.1. Writing the component
First, we write our component exactly the way we want:

```javascript
// WhateverNameYouWant.jsx

import React from 'react'
import Input from '...'

const WhateverNameYouWant = props => {
  return (
    <div className='my-class-1'>
      <div className='my-class-2'>
        <Input
          label="First name"
          name="FIRST_NAME"
          isRequired={ true }
        />
      </div>
      <div className='my-class-2'>
        <Input
          label="Last name"
          name="LAST_NAME"
          isRequired={ true }
        />
      </div>
    </div>
  )
}

export default WhateverNameYouWant
```

> **Disclaimer:** If you're creating a **Custom Form Group Component** with fields, it will only work if you use `three-data-field` components. If you want to understand more about the `three-data-field` concept, click here [TO DO].


#### 1.1.2. The simple rules
Ok, the component structure is done and is looking good. Now we need to use some `props` that will be available when we inject this component inside the `DynamicForm`.

First, let's see which `props` are available by default and understand their porpuse:

| Prop name | Type | Porpuse |
|-|-|-|
| isErrorVisible | `boolean` | Shows or hides the errors from each input. It's `false` by default and is set to `true` when the `DynamicForm` is submitted.
| updateCustomFormGroupFunc | `function` | A method that receives the `fieldObj` and is used to update the field reference on the form output tree.
| removeCustomFormGroupFunc | `function` | A method that receives the `fieldObj` and is used to remove the field reference on the form output tree.

That said, now we add this props to our component. This will allow the `DynamicForm` to understand there's some new fields he needs to handle.

```jsx
// WhateverNameYouWant.jsx

import React from 'react'
import Input from '...'

const WhateverNameYouWant = props => {
  return (
    <div className='my-class-1'>
      <div className='my-class-2'>
        <Input
          label="First name"
          name="FIRST_NAME"
          isRequired={ true }
          onBlur={ fieldObj => props.updateCustomFormGroupFunc( fieldObj ) }
          onUnmount={ fieldObj => props.removeCustomFormGroupFunc( fieldObj ) }
          isErrorVisible={ props.isErrorVisible }
        />
      </div>
      <div className='my-class-2'>
        <Input
          label="Last name"
          name="LAST_NAME"
          isRequired={ true }
          onBlur={ fieldObj => props.updateCustomFormGroupFunc( fieldObj ) }
          onUnmount={ fieldObj => props.removeCustomFormGroupFunc( fieldObj ) }
          isErrorVisible={ props.isErrorVisible }
        />
      </div>
    </div>
  )
}

export default WhateverNameYouWant
```

How you can see, our `Input` component has some props that allow us to run some functions inside it, as `onBlur` and `onUnmount`.

On `onBlur` the component will run the `updateCustomFormGroupFunc` which will update the `DynamicForm` output object on each blur.

On `onUnmount` the component will run the `removeCustomFormGroupFunc` which will remove the **field** reference from the `DynamicForm` output object.

#### 1.1.3. Inserting the component
The component is ready to be inserted in our `DynamicForm`, but first, let's see how our `DynamicForm` is set.

```jsx
// Form.jsx

import React from 'react'

const Form = () => {

  const formStructure = {
    sectionsArr: [
      {
        id: 'SECTION_1',
        title: 'EXAMPLE 1',
        formGroupsArr: [
          id: 'FORM_GROUP_1',
          title: 'Personal data',
          fieldsGroupsArr: [
            {
              id: 'EMAIL',
              label: 'E-mail',
              isRequired: true,
              fieldsArr: [
                {
                  type: 'TEXT',
                  name: 'USER_EMAIL',
                  isRequired: true
                }
              ]
            }
          ]
        ]
      }
    ]
  }
  
  return (
    <DynamicForm
      title={ 'DynamicForm ' }
      sectionsArr={ formStructure.sectionsArr }
    />
  )

}

export default Form
```

We can see that the form has only one `E-mail` field.
Now we will insert our **Custom Form Group Component** before the `EMAIL` group. Above, you can check how the code will look:

```jsx
// Form.jsx

import React from 'react'
import WhateverNameYouWant from './WhateverNameYouWant' // Importing our **Custom Form Group Component**

const Form = () => {

  const formStructure = {
    sectionsArr: [
      {
        id: 'SECTION_1',
        title: 'EXAMPLE 1',
        formGroupsArr: [
          id: 'FORM_GROUP_1',
          title: 'Personal data',
          fieldsGroupsArr: [
            // Below, we write the object representing and inserting out custom component
            {
              id: 'OUR_CUSTOM_FORM_GROUP_COMPONENT',
              customComponent: WhateverNameYouWant
            },
            {
              id: 'EMAIL',
              label: 'E-mail',
              isRequired: true,
              fieldsArr: [
                {
                  type: 'TEXT',
                  name: 'USER_EMAIL',
                  isRequired: true
                }
              ]
            }
          ]
        ]
      }
    ]
  }
  
  return (
    <DynamicForm
      title={ 'DynamicForm ' }
      sectionsArr={ formStructure.sectionsArr }
    />
  )

}

export default Form
```

#### 1.1.4. Passing custom props to a custom component
If you need to pass some props to your component, you can do something like:

```js
{
  id: 'OUT_CUSTOM_FORM_GROUP_COMPONENT',
  customComponent: WhateverNameYouWant,
  customProps: {
    gameOfThrones: 'Last season sucks!',
    randomProp: 'Random string',
    objectProp: {
      param1: 'Spider-man',
      param2: 'Batman',
      param3: 'Whatever'
    }
  }
},
```

Applied to the code:

```jsx
// Form.jsx

import React from 'react'
import WhateverNameYouWant from './WhateverNameYouWant' // Importing our **Custom Form Group Component**

const Form = () => {

  const formStructure = {
    sectionsArr: [
      {
        id: 'SECTION_1',
        title: 'EXAMPLE 1',
        formGroupsArr: [
          id: 'FORM_GROUP_1',
          title: 'Personal data',
          fieldsGroupsArr: [
            {
              id: 'OUT_CUSTOM_FORM_GROUP_COMPONENT',
              customComponent: WhateverNameYouWant,
              // Passing props to our custom component
              customProps: {
                gameOfThrones: 'Last season sucks!',
                randomProp: 'Random string',
                objectProp: {
                  param1: 'Spider-man',
                  param2: 'Batman',
                  param3: 'Whatever'
                }
              }
            },
            {
              id: 'EMAIL',
              label: 'E-mail',
              isRequired: true,
              fieldsArr: [
                {
                  type: 'TEXT',
                  name: 'USER_EMAIL',
                  isRequired: true
                }
              ]
            }
          ]
        ]
      }
    ]
  }
  
  return (
    <DynamicForm
      title={ 'DynamicForm ' }
      sectionsArr={ formStructure.sectionsArr }
    />
  )

}

export default Form
```

And you'll be able to access this props inside your custom component:

```jsx
// WhateverNameYouWant.jsx

import React from 'react'
import Input from '...'

const WhateverNameYouWant = props => {

  // Testing the passed props
  console.log( 'gameOfThrones', props.gameOfThrones )
  console.log( 'randomProp', props.randomProp )
  console.log( 'objectProp', props.objectProp )

  return (
    <div className='my-class-1'>
      <div className='my-class-2'>
        <Input
          label="First name"
          name="FIRST_NAME"
          isRequired={ true }
          onBlur={ fieldObj => props.updateCustomFormGroupFunc( fieldObj ) }
          onUnmount={ fieldObj => props.removeCustomFormGroupFunc( fieldObj ) }
          isErrorVisible={ props.isErrorVisible }
        />
      </div>
      <div className='my-class-2'>
        <Input
          label="Last name"
          name="LAST_NAME"
          isRequired={ true }
          onBlur={ fieldObj => props.updateCustomFormGroupFunc( fieldObj ) }
          onUnmount={ fieldObj => props.removeCustomFormGroupFunc( fieldObj ) }
          isErrorVisible={ props.isErrorVisible }
        />
      </div>
    </div>
  )
}

export default WhateverNameYouWant
```

### 1.2. Without fields
You can also inject components without any field. In this case, it's very simple and doesn't require too much configuration.

#### 1.2.1 Writing the component
Again, we write our component exactly the way we want:

```js
// MyComponent.jsx

import React from 'react'

const MyComponent = () => {
  return (
    <div className='some-class-name'>
      <h2>A Custom Form Group Component without fields</h2>
    </div>
  )
}

export default MyComponent
```

#### 1.2.2. No rules
Unlike the **Custom Form Group Component** WITH fields, when we work without fields, the implementation is very easy and we don't have any rule to follow.

#### 1.2.3. Inserting the component
Let's see out `DynamicForm` setup:

```jsx
// Form.jsx

import React from 'react'

const Form = () => {

  const formStructure = {
    sectionsArr: [
      {
        id: 'SECTION_1',
        title: 'EXAMPLE 1',
        formGroupsArr: [
          id: 'FORM_GROUP_1',
          title: 'Personal data',
          fieldsGroupsArr: [
            {
              id: 'EMAIL',
              label: 'E-mail',
              isRequired: true,
              fieldsArr: [
                {
                  type: 'TEXT',
                  name: 'USER_EMAIL',
                  isRequired: true
                }
              ]
            }
          ]
        ]
      }
    ]
  }
  
  return (
    <DynamicForm
      title={ 'DynamicForm ' }
      sectionsArr={ formStructure.sectionsArr }
    />
  )

}

export default Form
```

Again, we can see that the form has only one E-mail field.
Now we will insert our **Custom Form Group Component** after the EMAIL group. Above, you can check how the code will look:

```jsx
// Form.jsx

import React from 'react'
import MyComponent from './MyComponent'

const Form = () => {

  const formStructure = {
    sectionsArr: [
      {
        id: 'SECTION_1',
        title: 'EXAMPLE 1',
        formGroupsArr: [
          id: 'FORM_GROUP_1',
          title: 'Personal data',
          fieldsGroupsArr: [
            {
              id: 'EMAIL',
              label: 'E-mail',
              isRequired: true,
              fieldsArr: [
                {
                  type: 'TEXT',
                  name: 'USER_EMAIL',
                  isRequired: true
                }
              ]
            },
            {
              id: 'MY_COMPONENT_WITHOUT_FIELDS',
              customComponent: MyComponent
            }
          ]
        ]
      }
    ]
  }
  
  return (
    <DynamicForm
      title={ 'DynamicForm ' }
      sectionsArr={ formStructure.sectionsArr }
    />
  )

}

export default Form
```

#### 1.2.4. Passing custom props to a custom component
If you need to pass custom props to your custom component, please check the **"1.1.4. Passing custom props to a custom component"** section.