# Sympla-UI
 React components that implement Sympla's Design
 *Inspired by Google's Material Design project*

## Getting started

Install via npm:
```sh
npm install @sympla/sympla-ui
```

#### Read installation docs

## Usage
All Sympla-UI components work in isolation. See a example below and lear how to start using it:
```js
import React from 'react'
import { Input } from '@sympla/sympla-ui/form'

const App = () => {
  <Input
    name="my-input"
    value="Default value"
  />
}

```

