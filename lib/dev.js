'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var styled = _interopDefault(require('styled-components'));
var ReactDOM = _interopDefault(require('react-dom'));

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;

  try {
    Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}

function _construct(Parent, args, Class) {
  if (isNativeReflectConstruct()) {
    _construct = Reflect.construct;
  } else {
    _construct = function _construct(Parent, args, Class) {
      var a = [null];
      a.push.apply(a, args);
      var Constructor = Function.bind.apply(Parent, a);
      var instance = new Constructor();
      if (Class) _setPrototypeOf(instance, Class.prototype);
      return instance;
    };
  }

  return _construct.apply(null, arguments);
}

function _isNativeFunction(fn) {
  return Function.toString.call(fn).indexOf("[native code]") !== -1;
}

function _wrapNativeSuper(Class) {
  var _cache = typeof Map === "function" ? new Map() : undefined;

  _wrapNativeSuper = function _wrapNativeSuper(Class) {
    if (Class === null || !_isNativeFunction(Class)) return Class;

    if (typeof Class !== "function") {
      throw new TypeError("Super expression must either be null or a function");
    }

    if (typeof _cache !== "undefined") {
      if (_cache.has(Class)) return _cache.get(Class);

      _cache.set(Class, Wrapper);
    }

    function Wrapper() {
      return _construct(Class, arguments, _getPrototypeOf(this).constructor);
    }

    Wrapper.prototype = Object.create(Class.prototype, {
      constructor: {
        value: Wrapper,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    return _setPrototypeOf(Wrapper, Class);
  };

  return _wrapNativeSuper(Class);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _taggedTemplateLiteral(strings, raw) {
  if (!raw) {
    raw = strings.slice(0);
  }

  return Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArrayLimit(arr, i) {
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

function unwrapExports (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var reactIs_production_min = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports,"__esModule",{value:!0});
var b="function"===typeof Symbol&&Symbol.for,c=b?Symbol.for("react.element"):60103,d=b?Symbol.for("react.portal"):60106,e=b?Symbol.for("react.fragment"):60107,f=b?Symbol.for("react.strict_mode"):60108,g=b?Symbol.for("react.profiler"):60114,h=b?Symbol.for("react.provider"):60109,k=b?Symbol.for("react.context"):60110,l=b?Symbol.for("react.async_mode"):60111,m=b?Symbol.for("react.concurrent_mode"):60111,n=b?Symbol.for("react.forward_ref"):60112,p=b?Symbol.for("react.suspense"):60113,q=b?Symbol.for("react.memo"):
60115,r=b?Symbol.for("react.lazy"):60116;function t(a){if("object"===typeof a&&null!==a){var u=a.$$typeof;switch(u){case c:switch(a=a.type,a){case l:case m:case e:case g:case f:case p:return a;default:switch(a=a&&a.$$typeof,a){case k:case n:case h:return a;default:return u}}case r:case q:case d:return u}}}function v(a){return t(a)===m}exports.typeOf=t;exports.AsyncMode=l;exports.ConcurrentMode=m;exports.ContextConsumer=k;exports.ContextProvider=h;exports.Element=c;exports.ForwardRef=n;
exports.Fragment=e;exports.Lazy=r;exports.Memo=q;exports.Portal=d;exports.Profiler=g;exports.StrictMode=f;exports.Suspense=p;exports.isValidElementType=function(a){return "string"===typeof a||"function"===typeof a||a===e||a===m||a===g||a===f||a===p||"object"===typeof a&&null!==a&&(a.$$typeof===r||a.$$typeof===q||a.$$typeof===h||a.$$typeof===k||a.$$typeof===n)};exports.isAsyncMode=function(a){return v(a)||t(a)===l};exports.isConcurrentMode=v;exports.isContextConsumer=function(a){return t(a)===k};
exports.isContextProvider=function(a){return t(a)===h};exports.isElement=function(a){return "object"===typeof a&&null!==a&&a.$$typeof===c};exports.isForwardRef=function(a){return t(a)===n};exports.isFragment=function(a){return t(a)===e};exports.isLazy=function(a){return t(a)===r};exports.isMemo=function(a){return t(a)===q};exports.isPortal=function(a){return t(a)===d};exports.isProfiler=function(a){return t(a)===g};exports.isStrictMode=function(a){return t(a)===f};
exports.isSuspense=function(a){return t(a)===p};
});

unwrapExports(reactIs_production_min);
var reactIs_production_min_1 = reactIs_production_min.typeOf;
var reactIs_production_min_2 = reactIs_production_min.AsyncMode;
var reactIs_production_min_3 = reactIs_production_min.ConcurrentMode;
var reactIs_production_min_4 = reactIs_production_min.ContextConsumer;
var reactIs_production_min_5 = reactIs_production_min.ContextProvider;
var reactIs_production_min_6 = reactIs_production_min.Element;
var reactIs_production_min_7 = reactIs_production_min.ForwardRef;
var reactIs_production_min_8 = reactIs_production_min.Fragment;
var reactIs_production_min_9 = reactIs_production_min.Lazy;
var reactIs_production_min_10 = reactIs_production_min.Memo;
var reactIs_production_min_11 = reactIs_production_min.Portal;
var reactIs_production_min_12 = reactIs_production_min.Profiler;
var reactIs_production_min_13 = reactIs_production_min.StrictMode;
var reactIs_production_min_14 = reactIs_production_min.Suspense;
var reactIs_production_min_15 = reactIs_production_min.isValidElementType;
var reactIs_production_min_16 = reactIs_production_min.isAsyncMode;
var reactIs_production_min_17 = reactIs_production_min.isConcurrentMode;
var reactIs_production_min_18 = reactIs_production_min.isContextConsumer;
var reactIs_production_min_19 = reactIs_production_min.isContextProvider;
var reactIs_production_min_20 = reactIs_production_min.isElement;
var reactIs_production_min_21 = reactIs_production_min.isForwardRef;
var reactIs_production_min_22 = reactIs_production_min.isFragment;
var reactIs_production_min_23 = reactIs_production_min.isLazy;
var reactIs_production_min_24 = reactIs_production_min.isMemo;
var reactIs_production_min_25 = reactIs_production_min.isPortal;
var reactIs_production_min_26 = reactIs_production_min.isProfiler;
var reactIs_production_min_27 = reactIs_production_min.isStrictMode;
var reactIs_production_min_28 = reactIs_production_min.isSuspense;

var reactIs_development = createCommonjsModule(function (module, exports) {



if (process.env.NODE_ENV !== "production") {
  (function() {

Object.defineProperty(exports, '__esModule', { value: true });

// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var hasSymbol = typeof Symbol === 'function' && Symbol.for;

var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace;
var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;

function isValidElementType(type) {
  return typeof type === 'string' || typeof type === 'function' ||
  // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
  type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE);
}

/**
 * Forked from fbjs/warning:
 * https://github.com/facebook/fbjs/blob/e66ba20ad5be433eb54423f2b097d829324d9de6/packages/fbjs/src/__forks__/warning.js
 *
 * Only change is we use console.warn instead of console.error,
 * and do nothing when 'console' is not supported.
 * This really simplifies the code.
 * ---
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var lowPriorityWarning = function () {};

{
  var printWarning = function (format) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var argIndex = 0;
    var message = 'Warning: ' + format.replace(/%s/g, function () {
      return args[argIndex++];
    });
    if (typeof console !== 'undefined') {
      console.warn(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };

  lowPriorityWarning = function (condition, format) {
    if (format === undefined) {
      throw new Error('`lowPriorityWarning(condition, format, ...args)` requires a warning ' + 'message argument');
    }
    if (!condition) {
      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        args[_key2 - 2] = arguments[_key2];
      }

      printWarning.apply(undefined, [format].concat(args));
    }
  };
}

var lowPriorityWarning$1 = lowPriorityWarning;

function typeOf(object) {
  if (typeof object === 'object' && object !== null) {
    var $$typeof = object.$$typeof;
    switch ($$typeof) {
      case REACT_ELEMENT_TYPE:
        var type = object.type;

        switch (type) {
          case REACT_ASYNC_MODE_TYPE:
          case REACT_CONCURRENT_MODE_TYPE:
          case REACT_FRAGMENT_TYPE:
          case REACT_PROFILER_TYPE:
          case REACT_STRICT_MODE_TYPE:
          case REACT_SUSPENSE_TYPE:
            return type;
          default:
            var $$typeofType = type && type.$$typeof;

            switch ($$typeofType) {
              case REACT_CONTEXT_TYPE:
              case REACT_FORWARD_REF_TYPE:
              case REACT_PROVIDER_TYPE:
                return $$typeofType;
              default:
                return $$typeof;
            }
        }
      case REACT_LAZY_TYPE:
      case REACT_MEMO_TYPE:
      case REACT_PORTAL_TYPE:
        return $$typeof;
    }
  }

  return undefined;
}

// AsyncMode is deprecated along with isAsyncMode
var AsyncMode = REACT_ASYNC_MODE_TYPE;
var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
var ContextConsumer = REACT_CONTEXT_TYPE;
var ContextProvider = REACT_PROVIDER_TYPE;
var Element = REACT_ELEMENT_TYPE;
var ForwardRef = REACT_FORWARD_REF_TYPE;
var Fragment = REACT_FRAGMENT_TYPE;
var Lazy = REACT_LAZY_TYPE;
var Memo = REACT_MEMO_TYPE;
var Portal = REACT_PORTAL_TYPE;
var Profiler = REACT_PROFILER_TYPE;
var StrictMode = REACT_STRICT_MODE_TYPE;
var Suspense = REACT_SUSPENSE_TYPE;

var hasWarnedAboutDeprecatedIsAsyncMode = false;

// AsyncMode should be deprecated
function isAsyncMode(object) {
  {
    if (!hasWarnedAboutDeprecatedIsAsyncMode) {
      hasWarnedAboutDeprecatedIsAsyncMode = true;
      lowPriorityWarning$1(false, 'The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
    }
  }
  return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
}
function isConcurrentMode(object) {
  return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
}
function isContextConsumer(object) {
  return typeOf(object) === REACT_CONTEXT_TYPE;
}
function isContextProvider(object) {
  return typeOf(object) === REACT_PROVIDER_TYPE;
}
function isElement(object) {
  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
}
function isForwardRef(object) {
  return typeOf(object) === REACT_FORWARD_REF_TYPE;
}
function isFragment(object) {
  return typeOf(object) === REACT_FRAGMENT_TYPE;
}
function isLazy(object) {
  return typeOf(object) === REACT_LAZY_TYPE;
}
function isMemo(object) {
  return typeOf(object) === REACT_MEMO_TYPE;
}
function isPortal(object) {
  return typeOf(object) === REACT_PORTAL_TYPE;
}
function isProfiler(object) {
  return typeOf(object) === REACT_PROFILER_TYPE;
}
function isStrictMode(object) {
  return typeOf(object) === REACT_STRICT_MODE_TYPE;
}
function isSuspense(object) {
  return typeOf(object) === REACT_SUSPENSE_TYPE;
}

exports.typeOf = typeOf;
exports.AsyncMode = AsyncMode;
exports.ConcurrentMode = ConcurrentMode;
exports.ContextConsumer = ContextConsumer;
exports.ContextProvider = ContextProvider;
exports.Element = Element;
exports.ForwardRef = ForwardRef;
exports.Fragment = Fragment;
exports.Lazy = Lazy;
exports.Memo = Memo;
exports.Portal = Portal;
exports.Profiler = Profiler;
exports.StrictMode = StrictMode;
exports.Suspense = Suspense;
exports.isValidElementType = isValidElementType;
exports.isAsyncMode = isAsyncMode;
exports.isConcurrentMode = isConcurrentMode;
exports.isContextConsumer = isContextConsumer;
exports.isContextProvider = isContextProvider;
exports.isElement = isElement;
exports.isForwardRef = isForwardRef;
exports.isFragment = isFragment;
exports.isLazy = isLazy;
exports.isMemo = isMemo;
exports.isPortal = isPortal;
exports.isProfiler = isProfiler;
exports.isStrictMode = isStrictMode;
exports.isSuspense = isSuspense;
  })();
}
});

unwrapExports(reactIs_development);
var reactIs_development_1 = reactIs_development.typeOf;
var reactIs_development_2 = reactIs_development.AsyncMode;
var reactIs_development_3 = reactIs_development.ConcurrentMode;
var reactIs_development_4 = reactIs_development.ContextConsumer;
var reactIs_development_5 = reactIs_development.ContextProvider;
var reactIs_development_6 = reactIs_development.Element;
var reactIs_development_7 = reactIs_development.ForwardRef;
var reactIs_development_8 = reactIs_development.Fragment;
var reactIs_development_9 = reactIs_development.Lazy;
var reactIs_development_10 = reactIs_development.Memo;
var reactIs_development_11 = reactIs_development.Portal;
var reactIs_development_12 = reactIs_development.Profiler;
var reactIs_development_13 = reactIs_development.StrictMode;
var reactIs_development_14 = reactIs_development.Suspense;
var reactIs_development_15 = reactIs_development.isValidElementType;
var reactIs_development_16 = reactIs_development.isAsyncMode;
var reactIs_development_17 = reactIs_development.isConcurrentMode;
var reactIs_development_18 = reactIs_development.isContextConsumer;
var reactIs_development_19 = reactIs_development.isContextProvider;
var reactIs_development_20 = reactIs_development.isElement;
var reactIs_development_21 = reactIs_development.isForwardRef;
var reactIs_development_22 = reactIs_development.isFragment;
var reactIs_development_23 = reactIs_development.isLazy;
var reactIs_development_24 = reactIs_development.isMemo;
var reactIs_development_25 = reactIs_development.isPortal;
var reactIs_development_26 = reactIs_development.isProfiler;
var reactIs_development_27 = reactIs_development.isStrictMode;
var reactIs_development_28 = reactIs_development.isSuspense;

var reactIs = createCommonjsModule(function (module) {

if (process.env.NODE_ENV === 'production') {
  module.exports = reactIs_production_min;
} else {
  module.exports = reactIs_development;
}
});

/*
object-assign
(c) Sindre Sorhus
@license MIT
*/
/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

var objectAssign = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

var ReactPropTypesSecret_1 = ReactPropTypesSecret;

var printWarning = function() {};

if (process.env.NODE_ENV !== 'production') {
  var ReactPropTypesSecret$1 = ReactPropTypesSecret_1;
  var loggedTypeFailures = {};
  var has = Function.call.bind(Object.prototype.hasOwnProperty);

  printWarning = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

/**
 * Assert that the values match with the type specs.
 * Error messages are memorized and will only be shown once.
 *
 * @param {object} typeSpecs Map of name to a ReactPropType
 * @param {object} values Runtime values that need to be type-checked
 * @param {string} location e.g. "prop", "context", "child context"
 * @param {string} componentName Name of the component for error messages.
 * @param {?Function} getStack Returns the component stack.
 * @private
 */
function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
  if (process.env.NODE_ENV !== 'production') {
    for (var typeSpecName in typeSpecs) {
      if (has(typeSpecs, typeSpecName)) {
        var error;
        // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.
        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          if (typeof typeSpecs[typeSpecName] !== 'function') {
            var err = Error(
              (componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' +
              'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.'
            );
            err.name = 'Invariant Violation';
            throw err;
          }
          error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret$1);
        } catch (ex) {
          error = ex;
        }
        if (error && !(error instanceof Error)) {
          printWarning(
            (componentName || 'React class') + ': type specification of ' +
            location + ' `' + typeSpecName + '` is invalid; the type checker ' +
            'function must return `null` or an `Error` but returned a ' + typeof error + '. ' +
            'You may have forgotten to pass an argument to the type checker ' +
            'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' +
            'shape all require an argument).'
          );
        }
        if (error instanceof Error && !(error.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error.message] = true;

          var stack = getStack ? getStack() : '';

          printWarning(
            'Failed ' + location + ' type: ' + error.message + (stack != null ? stack : '')
          );
        }
      }
    }
  }
}

/**
 * Resets warning cache when testing.
 *
 * @private
 */
checkPropTypes.resetWarningCache = function() {
  if (process.env.NODE_ENV !== 'production') {
    loggedTypeFailures = {};
  }
};

var checkPropTypes_1 = checkPropTypes;

var has$1 = Function.call.bind(Object.prototype.hasOwnProperty);
var printWarning$1 = function() {};

if (process.env.NODE_ENV !== 'production') {
  printWarning$1 = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

function emptyFunctionThatReturnsNull() {
  return null;
}

var factoryWithTypeCheckers = function(isValidElement, throwOnDirectAccess) {
  /* global Symbol */
  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

  /**
   * Returns the iterator method function contained on the iterable object.
   *
   * Be sure to invoke the function with the iterable as context:
   *
   *     var iteratorFn = getIteratorFn(myIterable);
   *     if (iteratorFn) {
   *       var iterator = iteratorFn.call(myIterable);
   *       ...
   *     }
   *
   * @param {?object} maybeIterable
   * @return {?function}
   */
  function getIteratorFn(maybeIterable) {
    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
    if (typeof iteratorFn === 'function') {
      return iteratorFn;
    }
  }

  /**
   * Collection of methods that allow declaration and validation of props that are
   * supplied to React components. Example usage:
   *
   *   var Props = require('ReactPropTypes');
   *   var MyArticle = React.createClass({
   *     propTypes: {
   *       // An optional string prop named "description".
   *       description: Props.string,
   *
   *       // A required enum prop named "category".
   *       category: Props.oneOf(['News','Photos']).isRequired,
   *
   *       // A prop named "dialog" that requires an instance of Dialog.
   *       dialog: Props.instanceOf(Dialog).isRequired
   *     },
   *     render: function() { ... }
   *   });
   *
   * A more formal specification of how these methods are used:
   *
   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
   *   decl := ReactPropTypes.{type}(.isRequired)?
   *
   * Each and every declaration produces a function with the same signature. This
   * allows the creation of custom validation functions. For example:
   *
   *  var MyLink = React.createClass({
   *    propTypes: {
   *      // An optional string or URI prop named "href".
   *      href: function(props, propName, componentName) {
   *        var propValue = props[propName];
   *        if (propValue != null && typeof propValue !== 'string' &&
   *            !(propValue instanceof URI)) {
   *          return new Error(
   *            'Expected a string or an URI for ' + propName + ' in ' +
   *            componentName
   *          );
   *        }
   *      }
   *    },
   *    render: function() {...}
   *  });
   *
   * @internal
   */

  var ANONYMOUS = '<<anonymous>>';

  // Important!
  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
  var ReactPropTypes = {
    array: createPrimitiveTypeChecker('array'),
    bool: createPrimitiveTypeChecker('boolean'),
    func: createPrimitiveTypeChecker('function'),
    number: createPrimitiveTypeChecker('number'),
    object: createPrimitiveTypeChecker('object'),
    string: createPrimitiveTypeChecker('string'),
    symbol: createPrimitiveTypeChecker('symbol'),

    any: createAnyTypeChecker(),
    arrayOf: createArrayOfTypeChecker,
    element: createElementTypeChecker(),
    elementType: createElementTypeTypeChecker(),
    instanceOf: createInstanceTypeChecker,
    node: createNodeChecker(),
    objectOf: createObjectOfTypeChecker,
    oneOf: createEnumTypeChecker,
    oneOfType: createUnionTypeChecker,
    shape: createShapeTypeChecker,
    exact: createStrictShapeTypeChecker,
  };

  /**
   * inlined Object.is polyfill to avoid requiring consumers ship their own
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
   */
  /*eslint-disable no-self-compare*/
  function is(x, y) {
    // SameValue algorithm
    if (x === y) {
      // Steps 1-5, 7-10
      // Steps 6.b-6.e: +0 != -0
      return x !== 0 || 1 / x === 1 / y;
    } else {
      // Step 6.a: NaN == NaN
      return x !== x && y !== y;
    }
  }
  /*eslint-enable no-self-compare*/

  /**
   * We use an Error-like object for backward compatibility as people may call
   * PropTypes directly and inspect their output. However, we don't use real
   * Errors anymore. We don't inspect their stack anyway, and creating them
   * is prohibitively expensive if they are created too often, such as what
   * happens in oneOfType() for any type before the one that matched.
   */
  function PropTypeError(message) {
    this.message = message;
    this.stack = '';
  }
  // Make `instanceof Error` still work for returned errors.
  PropTypeError.prototype = Error.prototype;

  function createChainableTypeChecker(validate) {
    if (process.env.NODE_ENV !== 'production') {
      var manualPropTypeCallCache = {};
      var manualPropTypeWarningCount = 0;
    }
    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
      componentName = componentName || ANONYMOUS;
      propFullName = propFullName || propName;

      if (secret !== ReactPropTypesSecret_1) {
        if (throwOnDirectAccess) {
          // New behavior only for users of `prop-types` package
          var err = new Error(
            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
            'Use `PropTypes.checkPropTypes()` to call them. ' +
            'Read more at http://fb.me/use-check-prop-types'
          );
          err.name = 'Invariant Violation';
          throw err;
        } else if (process.env.NODE_ENV !== 'production' && typeof console !== 'undefined') {
          // Old behavior for people using React.PropTypes
          var cacheKey = componentName + ':' + propName;
          if (
            !manualPropTypeCallCache[cacheKey] &&
            // Avoid spamming the console because they are often not actionable except for lib authors
            manualPropTypeWarningCount < 3
          ) {
            printWarning$1(
              'You are manually calling a React.PropTypes validation ' +
              'function for the `' + propFullName + '` prop on `' + componentName  + '`. This is deprecated ' +
              'and will throw in the standalone `prop-types` package. ' +
              'You may be seeing this warning due to a third-party PropTypes ' +
              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.'
            );
            manualPropTypeCallCache[cacheKey] = true;
            manualPropTypeWarningCount++;
          }
        }
      }
      if (props[propName] == null) {
        if (isRequired) {
          if (props[propName] === null) {
            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
          }
          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
        }
        return null;
      } else {
        return validate(props, propName, componentName, location, propFullName);
      }
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
  }

  function createPrimitiveTypeChecker(expectedType) {
    function validate(props, propName, componentName, location, propFullName, secret) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== expectedType) {
        // `propValue` being instance of, say, date/regexp, pass the 'object'
        // check, but we can offer a more precise error message here rather than
        // 'of type `object`'.
        var preciseType = getPreciseType(propValue);

        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createAnyTypeChecker() {
    return createChainableTypeChecker(emptyFunctionThatReturnsNull);
  }

  function createArrayOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
      }
      var propValue = props[propName];
      if (!Array.isArray(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
      }
      for (var i = 0; i < propValue.length; i++) {
        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret_1);
        if (error instanceof Error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!isValidElement(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!reactIs.isValidElementType(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement type.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createInstanceTypeChecker(expectedClass) {
    function validate(props, propName, componentName, location, propFullName) {
      if (!(props[propName] instanceof expectedClass)) {
        var expectedClassName = expectedClass.name || ANONYMOUS;
        var actualClassName = getClassName(props[propName]);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createEnumTypeChecker(expectedValues) {
    if (!Array.isArray(expectedValues)) {
      if (process.env.NODE_ENV !== 'production') {
        if (arguments.length > 1) {
          printWarning$1(
            'Invalid arguments supplied to oneOf, expected an array, got ' + arguments.length + ' arguments. ' +
            'A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z]).'
          );
        } else {
          printWarning$1('Invalid argument supplied to oneOf, expected an array.');
        }
      }
      return emptyFunctionThatReturnsNull;
    }

    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      for (var i = 0; i < expectedValues.length; i++) {
        if (is(propValue, expectedValues[i])) {
          return null;
        }
      }

      var valuesString = JSON.stringify(expectedValues, function replacer(key, value) {
        var type = getPreciseType(value);
        if (type === 'symbol') {
          return String(value);
        }
        return value;
      });
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + String(propValue) + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createObjectOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
      }
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
      }
      for (var key in propValue) {
        if (has$1(propValue, key)) {
          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
          if (error instanceof Error) {
            return error;
          }
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createUnionTypeChecker(arrayOfTypeCheckers) {
    if (!Array.isArray(arrayOfTypeCheckers)) {
      process.env.NODE_ENV !== 'production' ? printWarning$1('Invalid argument supplied to oneOfType, expected an instance of array.') : void 0;
      return emptyFunctionThatReturnsNull;
    }

    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
      var checker = arrayOfTypeCheckers[i];
      if (typeof checker !== 'function') {
        printWarning$1(
          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
          'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.'
        );
        return emptyFunctionThatReturnsNull;
      }
    }

    function validate(props, propName, componentName, location, propFullName) {
      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];
        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret_1) == null) {
          return null;
        }
      }

      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createNodeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      if (!isNode(props[propName])) {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      for (var key in shapeTypes) {
        var checker = shapeTypes[key];
        if (!checker) {
          continue;
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
        if (error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createStrictShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      // We need to check all keys in case some are required but missing from
      // props.
      var allKeys = objectAssign({}, props[propName], shapeTypes);
      for (var key in allKeys) {
        var checker = shapeTypes[key];
        if (!checker) {
          return new PropTypeError(
            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
            '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
          );
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
        if (error) {
          return error;
        }
      }
      return null;
    }

    return createChainableTypeChecker(validate);
  }

  function isNode(propValue) {
    switch (typeof propValue) {
      case 'number':
      case 'string':
      case 'undefined':
        return true;
      case 'boolean':
        return !propValue;
      case 'object':
        if (Array.isArray(propValue)) {
          return propValue.every(isNode);
        }
        if (propValue === null || isValidElement(propValue)) {
          return true;
        }

        var iteratorFn = getIteratorFn(propValue);
        if (iteratorFn) {
          var iterator = iteratorFn.call(propValue);
          var step;
          if (iteratorFn !== propValue.entries) {
            while (!(step = iterator.next()).done) {
              if (!isNode(step.value)) {
                return false;
              }
            }
          } else {
            // Iterator will provide entry [k,v] tuples rather than values.
            while (!(step = iterator.next()).done) {
              var entry = step.value;
              if (entry) {
                if (!isNode(entry[1])) {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  function isSymbol(propType, propValue) {
    // Native Symbol.
    if (propType === 'symbol') {
      return true;
    }

    // falsy value can't be a Symbol
    if (!propValue) {
      return false;
    }

    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
    if (propValue['@@toStringTag'] === 'Symbol') {
      return true;
    }

    // Fallback for non-spec compliant Symbols which are polyfilled.
    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
      return true;
    }

    return false;
  }

  // Equivalent of `typeof` but with special handling for array and regexp.
  function getPropType(propValue) {
    var propType = typeof propValue;
    if (Array.isArray(propValue)) {
      return 'array';
    }
    if (propValue instanceof RegExp) {
      // Old webkits (at least until Android 4.0) return 'function' rather than
      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
      // passes PropTypes.object.
      return 'object';
    }
    if (isSymbol(propType, propValue)) {
      return 'symbol';
    }
    return propType;
  }

  // This handles more types than `getPropType`. Only used for error messages.
  // See `createPrimitiveTypeChecker`.
  function getPreciseType(propValue) {
    if (typeof propValue === 'undefined' || propValue === null) {
      return '' + propValue;
    }
    var propType = getPropType(propValue);
    if (propType === 'object') {
      if (propValue instanceof Date) {
        return 'date';
      } else if (propValue instanceof RegExp) {
        return 'regexp';
      }
    }
    return propType;
  }

  // Returns a string that is postfixed to a warning about an invalid type.
  // For example, "undefined" or "of type array"
  function getPostfixForTypeWarning(value) {
    var type = getPreciseType(value);
    switch (type) {
      case 'array':
      case 'object':
        return 'an ' + type;
      case 'boolean':
      case 'date':
      case 'regexp':
        return 'a ' + type;
      default:
        return type;
    }
  }

  // Returns class name of the object, if any.
  function getClassName(propValue) {
    if (!propValue.constructor || !propValue.constructor.name) {
      return ANONYMOUS;
    }
    return propValue.constructor.name;
  }

  ReactPropTypes.checkPropTypes = checkPropTypes_1;
  ReactPropTypes.resetWarningCache = checkPropTypes_1.resetWarningCache;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

function emptyFunction() {}
function emptyFunctionWithReset() {}
emptyFunctionWithReset.resetWarningCache = emptyFunction;

var factoryWithThrowingShims = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret_1) {
      // It is still safe when called from React.
      return;
    }
    var err = new Error(
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
    err.name = 'Invariant Violation';
    throw err;
  }  shim.isRequired = shim;
  function getShim() {
    return shim;
  }  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    elementType: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim,

    checkPropTypes: emptyFunctionWithReset,
    resetWarningCache: emptyFunction
  };

  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

var propTypes = createCommonjsModule(function (module) {
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (process.env.NODE_ENV !== 'production') {
  var ReactIs = reactIs;

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = factoryWithTypeCheckers(ReactIs.isElement, throwOnDirectAccess);
} else {
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = factoryWithThrowingShims();
}
});

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  font-size: 14px;\n  line-height: 24px;\n  font-weight: 600;\n  margin-bottom: 5px;\n  & svg {\n    vertical-align: top;\n    cursor: pointer;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}
var Label = styled.div(_templateObject());

var LoadingGIF = "data:image/gif;base64,R0lGODlhPgA+AOZ/AH3J/3bG/128/1W5/y2p//D5/5nV/7De/0Wy/+b1/022/4nO/2nB/yKk/+/4/0Cw/4XN/9rw/waY/+r2/wSY/77k/+Dy/9Ls/zWs/+X0/2XA/3HE/ymn/7Tg/3rI/wqa/wyb/9bu/9zx/2zC/47Q/6ba/5zW/8/r/9jv/7bh/xCd/+j2/6rc/8zq/w+c/7ji/5bU/xSe/x6i/7vj/8fo/zKr/8Pm/8Dl/xqh/zCq/6HY/+Lz/67e/2K+/6Ta/8bo/zyv/xaf/9Ds/1q7/zit/6PZ/1i6/4LM/8Xn/zuu/ySl/xig/0i0/5TT/0u1/5LS/2C+/z6w/6zd/1O4/1C3/4zQ/yem/xyi/57X/4DK//7//wGW//3+//z+//r9/wOX//v9/wKX//f8//n9//b7//L6//X7//j8//T6//P6/wiZ/+34/xKd/xOe/97x/wma/9/y/5DR/+z3/5HS/9Tt/yCj/2/E/9Xu/yuo/8vq/zSr/8np/6fb/wCW/////////yH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjYwMEQxOTRCRTdFRDExRTdCMDQwQ0ExRjYzN0M3NzA4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjYwMEQxOTRDRTdFRDExRTdCMDQwQ0ExRjYzN0M3NzA4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NjAwRDE5NDlFN0VEMTFFN0IwNDBDQTFGNjM3Qzc3MDgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NjAwRDE5NEFFN0VEMTFFN0IwNDBDQTFGNjM3Qzc3MDgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQFBAB/ACwAAAAAPgA+AAAHn4B+goOEhYaHiImKi4yNjo+QkZKTlINlPENMAzpklZ6LZSQqFF8UOBCdn6qFN0B9r31bRCmrtYIHVrCvOHG2tQdKun28vqszRMJJM8WqZgY4sG1HXcyqaC9QQEMpY9WrXWJmYtTe5ebn6Onq6+zt7u/w8fLz9PX29/j5+vv8/f7/AAMKHEiwoMGDCBMqXMiwocOHECNKnEixosWLGPUFAgAh+QQFBAB/ACwaAAMAEAAKAAAHY4B+goN+XWknKReEi35gcFVJNUlDHWiMfglPBBRhXy5ULVqMJz1sfadhSnGWixUPaqenSwFui142QLGnQQByi1wWAS6xb0All14XIxxBIERztZddKCYeBjcJopd+XF5jZ12DgQAh+QQFBAB/ACwfAAMAGwAUAAAHxYB+goOEhYaHiH5aiYyFXGIJIRONjShYPVQDWGOUh3JFGCASIDkecJ2FP0MxfX1bXzUzXaiCaT54W62tDXFetH5gBzm6uyRyv10oCrmtIA98v4IOWEQqXx8ERxHRflxnLBo9UCZ03INeaG5mY4uDYH4oYg6dWu2pC0YPC3lm5idTQSRQCKNACBduAZZ8aRVGRpM10SwwkMAwiBMsBaIViJNERhAOVAzcIROtixwpCyA0SREBjT1aWsoU2OEATZeX0erVGxQIACH5BAUEAH8ALCUABAAZAB4AAAfygH6Cg4SFhoeIiYqLjI2OhF5dXY+GEUgpewmUg3RzDwRAGiibKCQcLn1fMgJuj2A3RCB9s2F4OmSOXSk5s70qC2O5eUS9sw1PXo5aIkNtvkwvYo9eN04NHw0ITRmbXidSWSQv3Jt+XGZeXNLl7JRyO2OT7FgAPQwha+VZNS5qH3ovClDiQSTMFlpAdmhRBiPIwV4ydMhjJKbJkmJ9JCww4whMBxwY1QSQo+yFkoez1AyYsJCRFiFAUGYUoKmRlgJTPhTbMqJmowQLrIAI8yWMkgBlXCEBgIBAtg0t0FA6g+JGER0pRDjgsokLGTQFznhpGQgAIfkEBQQAfwAsKgAIABQAJwAAB/+AfoKDhIWGh4iJiouMjY6DXmJgXI8RMz4pIo1lNkcPHA12NotaOwwNIH1fQVBjimAVSX2zfWE5fIpnKTi0fVtLMGaJXQc1vVscJWCJWg5TbFt9FEEDLVqKXi8abTINRixyjF0RRXFYM2mOWmNcBY/v8I9d8BEHM3tc14tmBlE5SghghFOk5cADNV+2SGDzooyiNQCg0QojIIGiMgMkHGtAR9+hCFR6+UrSYpiIGmF6hXkQwWOhLh04RKOlRkCGRGLmxJg568MRd4jW9FAjMkiReYe82ODwpdcXPCeQGpoQoA3PPmp67HA5SMsFBCL7sKlyBlEXFjnC1pghtZCXG0QyRLbxkIArITlHckjY8kVCjhRiFI0RMScJKARxdjDysuOGCRg6RLhitM7BBAeT1WmxGwgAIfkEBQQAfwAsKQANABUAKwAAB/+AfoKDhIWGh4iJiouMjY6PkIIOGWKPXhcpMBA+GV6MWhEARHV1eAF0jGRFNWp9fV9KC4taCQFvrq5qVLMOA1u4r0wRi2J2QcAgGiKLYAdOrRJsQD6eiloOHQM1NVMwaI5ddyl8Nwlaj1rVkevs7YkZDo9ne0dTGnE0jhYaDRIfSwjgdFnEpUk/XG8gTFjkZkArXFucoFBEhkUDYH3CWFmRiBYDChi3CDCHyEsHAmEw4oBhJhEKASowUniQD9GEI1e+YGQDoMAhLRPiKEkJbEuSPeoITTCBgSiwIE0cnCukRUcUEBj7fDASgUuhMSc81ADxC5gEIgcGEgIjhEEdCVlc+9QhsWLqIDQmcOjM+iYACjCG0gA4ljXICCGVDJUh0aCsqzAyRpxISuhMBSYSHLdhsOdMRwcwrKhRAyIKFhRm7B7qIqIJFQQMDshRu2hMBiE/UPh8xAXMGMCNAgEAIfkEBQQAfwAsHAATACEAKQAAB/+AfoKDhIWGh4iJiouMjY6PkJGSk5SVloJmMwcHN2WXg2gGREpWQCSel10vOH2tfSo8n2JQrq0UQ59mQLV9X0yyQ7wUA59dKW2uWzg6n35jR0k4OEQQZM1+XTNxcSnW19/g4eLjkVw6Rg8aSNdcMHVqfSAEzJdaNjK1Ww1plloRRLx6tbBUYACFgF9uVMogAETAPi52TNLS4oHDgGEAgJFUZk6OgwG3OEkQSUuKKawebrEy0JG/JwOswFNp5cbGQ1zErEBxQQcEI3pwzKRpExGYCBCi1MhRZ8nQh31WFkU0oUmdMFBVMpl6qMuJKViz8gLhAcVNRGNSEBCbD4gUkos+znTgwDZqjjh0xDTyggTBlqwgBvA54eCRlh1ZVPxNlmNAHBsR9EYaE2EOgx7b+Nw4EaFwpS4Odlhw4ECypUAAIfkEBQQAfwAsDQAaAC4AIwAAB/+AfoKDhIWGh4iJiouMjY6PkJGSk5SVlpBcXpeUXhY2FScJYJuOXRcBQA89T3BdpItcISMufWpsBFVpr4peJhx9wH0USSe7iWMeQcF9YTUpxohnBiDLX0kX0IddN0RvwGEuQ9mICXNAQUtKVB3jh1puJQABcS1o7Ylybmha9/3+/+5eaenCpZEWMQ5ERChgCUyBFnsSuFKkpcAeGAEClMgwStKYFhsGaDAQggw/d2hu9MBT5wqeAVjuiCnlRYQOBC5c4ACSBUVHQ15a2JEh4UuYLS4wMMASYUWak0B3nIBhxIqaLVsogEiiowyiNAYwqFm2RU0MJUaOSKET4UICLQnnLuxAAmNDFBwgviwLgyMAHKiEzGTBEWZZsC0gcFghYqSHBgEahiDQ00BFny2GgX2AQqegITQlamDOvEyrChAgXICgQKEwaWBqRsgBPKgLHBg5XuveDewLCAQvaBPykmAGAwIfeCvXzGHDDV2LyLSYg8Db8tdbghg5kOCMQTErLhQRkCP5dWYqGgxoEqGLcEYF8rA44iTHEtKuQaioQyXAARQOeCaJFmeUkUAFWCxQBQMK4JFDEkBw4AQDDMTRQQsZjOHeJlqQsYYcE4QgxAlIpHCDEBeIsEMZZ0yUDRdgFOSFGZrEaEkgACH5BAUEAH8ALAQAHwA3AB4AAAf/gH6Cg4SFhoeIiYqLjI2Oj5CRkpOUjVpdYGBdWpWdhVxkIignGQ6ep2AiPEc9GgFSaaeMmLGFWismTHUxbQ0KB2OyiFwOKAc+P3KEWnlTMVt9fRJtIwnCh2dYCjl4Q0UoXIJcLXVf0dFhOR3B14NeLETQWzEYWGKCWhFDaud9WypGLrTDh0aDin4geiTgpCUNjCD9/NVpUmagFjc9zJ2TQCXEIC02gETsA4IICy8DzUD5cG4LiAETCE0gIWNkQhRg2o0xQQBEtC85sBQas4fKSAoyoJxop4XOkQcNGtTwwI5QARNWwoysoyFFgXYR+JCIMwPOIS9wNkgYKSHIkAor/sJdk+OlSyIvSKCwGfklyIMnKNYMVKSlQAoiI6OBUDKgRIgMgxOtkOLEZ2I2HIYc4QMnQwEtZ7yIOSOmi11hXNboIMIycR8XS/REYRAARhEsBprEObAGpbAuCTowcY3uiwQVberIaHAFBw4qLCZwuubghp0GxFv20QpNwhINJ3xfM7NnDoLW2SN+AdKh4kAyGUwIIJBePYYi7gd6mbCnSYAc9aGThBRfReaHGQVcwAMAOShxkFYjbRGEAEjkZKAgY5yxwwkGeCCAFXqAEIQabYDg3AB8xHVhIV3IsQIKexzAQxVNHGGAASm08NmKh4QzhhcIOuBAAWh4ocV0kwQCACH5BAUEAH8ALAIAFAA4ACkAAAf/gH6CflpiYmZnWoOLjI2Oj5COXCI0Byl7Fl6KkZydnVoXC1QITD0GLWOeqquLBSZWKhIgMkBVcKy4nyJEan2+FEEKd7nEj10zeL7KalBpxc+LXgsuyr5vdqnQ0HIAYdV9QVJi2tAJPb3KWzIpXOTFXBNUX981N+7PIkbeyl81d5v3cIkYsM9Xv38BcXFZoWBeOiI/EuLSAkdetTA18gCU6AmOhi3VvhDYA4ajKi0O7LyptqUBli4mVZFZEORbEAgxVaHho+LbGwUhcnbyUoGIw181bAjlRLHHtz4yPBRYGqnAETY+h0RoR9WRGRs5njYgsbHroggbeoZk0oGjli5d/0pWLdHm25cgG1ok9CIiT4obKDjRYfBUTQMTGe55uTGESA4iJOhA0mLGR5KnIHIUSayNywkndfuA4DAnAiQvFtI+daGZ8zMzUhpUc/EAiRdIZlowedqHNZYT0Lxk+fCNQAqYkApIIcLbhZINHbTIVUhCNj8ge5A/AuOmSdjCQRAsOLEml5gXCNSGkaEhQScxITxYt/ulDpUjEZyxytCESQMVeAgQmCdl2MDAfJgRwMAJZOCSwQtPLKCDG6w4gEQAGPDmixpAlOBAWe95MUaDuBQQAQzM8RYGATAUAKJQYlhwAAJYfSMBAXys8aJQXRTQwgbMgdTHFioMcAOJZi1ihjAbJdjRBhthuMDiDjt25QUZEfCRBRNDzEEHGFUmWUYGIgiRQBdhJikIGFykqaZZgQAAIfkEBQQAfwAsAgAHADQANQAAB/+AfoKDhIRaYmBchYuMjY6PflwZNDZCCWRakJqbjGgzC04PGgYhYJmcqJBmewNbXxQxCE93YKm2i1pgJXV9vX0SS1Qdaae3tw5NHBS+vRI1cTvFxqhjNlHMvhR1RzuK06haLw1h2L3aLF3fqJIMIOWuVHfqqXJZau9LARG185tgLUDeBYGSpx8qOAw+lJOgoYBBTmA6KFnohAa/h4+0ZBjhjtmWBgcwbuJSYRw2FwwmiNREpooLbFuitFgJScsODV+wgdjggGZGGhjI+dpC4IUXn47GlOjoSwKVNEgdmcnCtNcHDw6j4oKD02MDLOm0FtLi5oEEj0QueBM7SMuPa8z/nO5gW4jLDzzYvgyxQJfQmRcYTmqQ03cQlxY5sEkYMLewnzEziOiEksFxJCE1hPb6wiSCZS0XBGBTw2SmY409qvbBcyNs4QRHysmAYNnPmSJWdGqgY5nMjAfY3iQ5sLavCAFvsMmYs8KygyLlQDw4ccYxlxtMZGdJ4BjMjgUv42L4Ub1wmg4IYG7L2heMiBFBmIVRESCDNLYOpKT3pSbHkzX9aNHFGaZoYlMTGGyxhRpLaHBBed944cABKVSwQxmanIECBEEEoUcPHaAxTxcvTFEDDkkwYMMmYlzwBAQGnGBGcbdoEYEGbPjSwBGe1eRFGgmIcdQ8YxTRxha+SDfDRZAGPsRFHDJgw4EPYlhWABYNLLNZAylcVNgMRgShYBhB2CECjXSlwcIUHCyRAxQr1iaIHC2UAAMfY8hpCBhm6Onnnz4FAgAh+QQFBAB/ACwCAAIALwA5AAAH/4B+goOEhYaGWlp+ioeNjo+DWlxcY24RGWsnEWJiYJCfjZJdFzoeGxoDNTgPVFRPJwWMoJ9aEzZHTjUNQRR9fVu+WyAcAXmes49cBTQATDJqvtHSfWEuDzpkyI5iNxtK0+DSFEoQCVzahV0JJkkg4e99FFZHIufogmkvU73w7xI5cRLIQqbFywwn7voFC7PlS0MXTlKIuadFDgwrX/ptCfNFzQMBqRA4oQLDwjGCCZpYgdeQAhEPM8SsEWHBS4YTbs6AGQiKTBEM/KZtUfGgyo57n7wgyREmnIQ6DGZMsIe0kZc9TKCBazNgRlVaOzwknCZhyI1sXx+ROZADmDRhPf8y8ExbSEsGDUGDtbFzlK7aEnXcRnszhUYXv4660KGSt88XAizOIHaEhg8OoS5GoJ2MKIOdptIkPLhAlTMhLkiIgPYVpsGcMaYPlTHxTZoaJnliH5pwZKwvFwHQ6E6HYgA4JQckDyd0gqk4IDaWEzrzgsg0EFD6SveThsXKaFtcMNg+iAwLcHV8CCePRoeM0ElSHCZfoAQRNVu2qLESAAV5QWLsYQcRHCiRRABn/bdIGUKU8IQBHdBRQGnbceFFAQlMMEYXc/2XiIIghijiiCSWaOKJKKao4orbfTiiFmCI4UACZHQonQMH2DFAAEVE4AWIXXTgRBBbvFEDABcoqMVGHQNo1YcaRKSgoBcp1DBNHTA4oCAfNUggTR0QZKDgDVOwEc0XePgw0X8JwAAECB0pEYCYSqLhgwZMULEAHT+G6IUIEUASCAAh+QQFBAB/ACwCAAIANAA5AAAH/4B+goOEhYaHiImKi4paXFxdkZGPWlqMl4taXWc7B1l2D1GiThsLKRFgXZaYrI4FMz1KfbO0tbMUdRopZVyrrIhaYygBS7bGx31LHheqv4ZedBogyNTGKlB0XM6CWhNVxdXhtktHab6XXkhJ4uy2FElC2pdlJQ3t97V1OvKKZVjT1baE+UKhoEEKX8JsCYejyjlDZUwARBYGRI0NUkQQAtNphJWE1awcSBRxorEwQewIYSSGhgYXYZCBgDLhEBkWJmttacMggTM4LxfaAjFExEMte+wdk5AEyTZBYl7UiFkrCIAVD0UMkSnA59NBcHpQ7UMBwwsyhcTwYHMMRA80X78LJfDwZYsEAibkPHRgQolQWm7hxi20BgaCADPWPPRDpoKCN3/dmhl8qEsBB14W+9Gyhg8CFxK+vHGSgfJXLWVoLBjgxIMb04O5kHGwZjLs27hz697Nu7fv38CDCx9OvLjx48iTK1/OvLnz59CjS59Ovbr169i7iDEjpsvyMSmGAIHyQvDxLkfa0MJhwLbxGetqEZmBPA4OW0pGHrdvSyTyFET81QcQNyBHBgQ4IESBCiSUkRwZOgzAxBA8OLhNIAAh+QQFBAB/ACwGAAMANgA4AAAH/4B+goOEhYNaXV5cYhNpXmVyilpahpWWl5VcYwVCLwcAGwEDVCNHWU1FF11clJiurmcWKSNMShIqEn19ubsyDTVOAB0rrK/GhGBwVQ0gYbrP0NBqS0QjPihox8ZaawdMH9Hh4n1XRnxlXNqYXBlzObzj8X1bKgoHa+nqhloJVRwU8gL2CZNjgZtW+g45KELAGbQtECEKnBdkBIp8Cf1oSXNjiIowW75QUKEkiJUkOdR8ARlQwoMDaTIKArOjCBU8OR54MMBCxIUKP0TM8ECEwsot4z44STFGph8vCSoUmbHGgZdCCFcUGbKEZbg3CoR0cerHzFVjY+DoQKACaTgKSf9QjCWrD8URPF/EgRgRga4+LitMNAAYbYuVJjH9quNygYmacBIQ0ECo2NiZDggeR3MRIHHlYwV84HH7jAIeKZQ/u3JAAkc4FyMsqNY2QYNmaBhSnJ3takyKGq+PlOFtbEcAF9HUMMlA/NWYDnqifcnBJ3VzQxmG3NaFA8bw65fK6AAHjU2AAuAvnanwgLSEBgHIpL9kAUobCVskXJlybr4lMSxQQYASRPRQhAhN+bfPBB3AAAMfNMgBhoKWcHEGGWUUcIZ1FGLV4YcghijiiCSWaOKJKKao4oostujii8Z0ccYYipTIzw0GeGCCXCS6MQcRIATBwQgX7AZiCUC88QxFZxZg1KEcAAQRDRA2GEmhGwEsIc0DFYiIRhxKONQHGz2cIKIWLVDhwkoUEPBEAiOi0cEQSdSQRBVwTFjiBSmckMZcswUCACH5BAUEAH8ALA4AAwAvADgAAAf/gH6Cg4SFhoeIiYqLhFxaXYyRkoNaZDt5dxFyWpOdh1picCYCOUZZUmRcnqt+XDsmCktfWyA5UwddnKyRWmY0UEsSfcN9LgQjLY67jAUsCCDE0UsIJQ66y59rJXjC0dEPJnDX2IRaaQcPb97eSwsF4+SCXiJxNbPrxG89KPDxfhkXqlwJg2+YjCwW+sXTAsZMAAoE8alZ4EWhPz90BtzzFoaDjgIXEZ0xoSSitwd7LF5MYMDeOhcaMqj0h2aBiy3rcPiYudDBHBnr1AhIwDPenQHdiG1ZYuJMSERdIgDB6U3DmqeICgBQFy1MjhdFyXU5ESXpMBdHQGI1hOYItGhq/4yoXVuog5J1QFLSLRdhCNVhW+pgAbOXUIEsLrypOFKm8KAzfN4SU9OjsWM/Wmbo8UZhiJvLfsa6JCbhwYSw2LSEcPK3DwUEe0B3QcHEZB8JRCKgxhaCijc1TlqA9nJCzxe4TBLIvvHA2wcBEUCfA9EahAAzoOVAwOENx4I0l2dv8PbFChYvoG0kWYfhBegJTVr3UWEEvGMtcARINvhEzmUwJwxBQTQfPMADaPh5YMWAfYShAgPRgVaADRrIoMIXbDygA2iCaLHGDBAYwQQVMEyQiBa7RcKFGBH8MMMPO3wyhhluoIHeU11wQUYuh9BhAhQ9aMDCGapwGMERBHzwhVUKRGDhAId+8PGAZFsogAIkl8lBQgPe5HAAYZd5EQeXSuHhg32OdTGDPVTFMMQPUMLhQQ4gSAACBkX4B+UYWAxARQ9YoAAlIROEkIAYRQ5KiaKMGhIIACH5BAUEAH8ALAkABAA0ADcAAAf/gH6Cg4SFhoeIiYqLjI2Oj5CRkpOUlZaXmJmam5ydnp+goaKjoVpdXmJnY1qkiFpmE0IzM3QrrK2EWmUiOhtUTENFFre4fmMWc1QyIGouNS9pxYJdN0ZsX33ZFA9rxK1gSEwS2eRBJF7SfgkKauTkUBnpXS9BW+5bMgferSgD4+4g4hRI5ydOO3cSpkxIp+VGDnd9tkQRsW+UlhUIKEBs0KHiqAkjPkBUwQJMOjRV/pFrEyeaNC8wlkAEkcWjKC8lZMwEYDOUGD5vRnoYKE1MiSAzPZBJN8bAlZEQeoIqAEGkuyBzlkrbsUFlRBlSzkjrQucBNnJh8LxIJ+ZFDYgU/5jckaZFDgAQENloMCONS4shELc0MFFG2goYVsmpqWFDGhkkTvACHLHDkBguXsxw6bQjTgOIfZTAMJThBYksUk6g0+TAIWgVUBJsHpShCYIGHxo4ubH6UoEWCsIE5tCEqKC2TFSsHELR9wkBMiHGoBKhkJcnn90RydPFkoMWAp7OJDJjTKExC5S7y5Gi+yQwK2w4ie5OjRIIFgyR0YFHeDYQRNxgkiRg7HBAEl5lI0EQGvyAiBsCyICNCxyQgMIkWjgwAxMHIRTEECkogoIGQBDwwBx0UNLFBBBkh1AbRoS4SAJ7pIBEdZRoYYEHMdhDzoJGHOBIF6dYokUCcajgYzgfanwoIy5aiHEAAj6CYAUDTxbTxQpHcBBGEERUsQdBgpzhBgkaDDBDfmT6ocUYBUSwUJu5SHVIIAAh+QQFBAB/ACwFABAANwAuAAAH/4B+goOEhYaHiImKi4yNjo+QkZKTlJWWl5iZmpucnZ6foKGio6SlpqeoqYlaYGBeYFqqhlplCXQobmhesbKCYDRPHnZHfChevYIFcRgyQVdJC3RgvVooQ299W1suGDDTvTcEfePjIAArvWRYLuTlI2KFBVxjvKFyASDtfS4LZYNpM7DEKRKhS6guNJhsabeFAwteclgYaSCjjYYXxz55aaJCX58HKARpaTEgCIVsbKY4qLdpzJ0havSBGOHAVwkOC8nVOGCw05ombHKOCyNDBxlBZmAsEdoHR4ozndL8YOJRwpSQg/jkCNMuSYVvmrhcsJOvXZggcaAOGgMlyJc+IP8aMNjB8pIYEQtkeAShYA+XQjbsNODw4IgNf5q8RFig5G07Chx0/DUkIoWPGRE4cUHRhIjHLUF6YDXEBYyYjJrkCAnQ0aMaJ3vqhioQwQeVGB77fMATp6YoMGYKoDjAgENrfRJ4F/jURYuDMnBQ8NAhoE2b3OOUjLjjiVWGPSYWbECQhE027CCseMjwiYucA0MaBOkjAfs4CiqIkNjRs9OYEwHUYR85W3yAgxNNjNaJFmakQERMA6oBAhAjHADWJ2WUgIFjuUkAQg5DmJAAKWkc8GBuagSBxxRV0GEKGDcIgJs2X7jgQgNE2GGCDai4ZwATNSiBQQ09BADDBSiMoUoVFzvscYANB9AhwgR+TNbLX9PINkkgACH5BAUEAH8ALAMAHwA4AB8AAAf/gH6Cg4SFhoeIiYqGXlqLj5CRWgk3fCl3XZGam4RoMFM1NQMdDo6cp4pePkBsEn1qTgdgqLSHIhogfbp9QXZitcCDEUxfu31bA6XBwVRqxm8BCabLtAtKxa81RWTUwHQBeHV1RAAR092nXhk+EDApF17owWIZDvL3+IRaYmljXOf5NHnJ04FGBjMAAy4qQGMDFQEk9hRIqNBQFzkpmIAAEYNAgDzxKiaaVAKBq2NqCMwpA4xLF4qpHNwxgeCNsT4qetyZdUoLGDkidqDxwgVSlwgsBKjYcrOPiyF7inLScmZHhyMbDCBxs8anVKl+0iRAccNDFBVNdS0JkAFVlwx85KbUcdGGw4gFN1qgCGpBRIgTPCAMCJJW1xYVCGaA3TQGiQAQTHVJwKHkgQABGgIwcJLDyhUXkZtSWDLFxhlaDrDowWYsjORcfcKwLtxHhoYftbRkIGElNO3fur6o4GAHCTdaWuTAaOAbeGE1MqjwWbOMyw8qKiQ0d75FgosrDwDYQNMNzY0AGGRAdv2bggSOeKYcoeFg8bIxCWwsgEKgQQwXfUjwBlNqbKSCFURMsUETNyQgEgotYJEFAwg8gAAROTwwgAAMVCEFHTyJJEgmZrgBRwQ32HBHHjTcUYCIi0wDEy2BAAAh+QQFBAB/ACwDABsAMgAjAAAH/4B+ZClxcTNdfomKi4yNjo+OZBBEODhJR2OQmpucOjhbfaFtKYicpqd+AxShrENiqLCaTF+soUBmsbmNQ6u1UK+6wX48KrU4L6XCuWUkQFZKRAZoysJlNwcHM7jU3N3e3+Dh4uPkylpdXl7lnFwOITcVblrrj1w7RQI1HFDy9I1ycZT06tMjg79FWmbkoFUrgMGDfrRUCVKLVQAR8w5KKVYxlIAWwOidQQCqYx8CLPrRo4OnZEcQApDsAEPvBQeXHYOM6EAnwzZGYLpkFGYTp0kgG3TcoHGiaQsbN+gUGBrsBYGBJlltWZKjqxUQDQS8mEYNyQMQWdOG2hJDAx0u3FNEaGhgVK1WAj5CUtORZIldk0uOFPiWYAERv39ZBQEwGFxhZzH+brFigsy4NC8APMih5IqKzyA+q1hihcEJuOuESGmywY6dHq7tLJAygSrE2/QCAQAh+QQFBAB/ACwBABQAJQAoAAAH/4B+gn5aaG5yg4mKi4yMaC1xAQAlblqNl5iCHVRKS0FAcwmZo4tDLmF9fW9EN12krxdJX6mpIAZnr6QpNai0QR5juaMnSRS0fRwmXsKZaVUEbGp9LiMoXMyYXXBPPQ9AAReu2JhgCScVNhbL469e1+zw8fLslvOXXGVrZmfv9oNgKEpgYYFETj1/Xm700NPAihEWYvwJmtCDzawvbIbQOTgvgowtx4IYQOMPDBIVx1JNEdFPHo0HvWjFwELSnhgGIFJ+obIHjL8DDWKmwgEhgz84AqSl1MOiAEd2ZvhwAHmMAoYZa+ztGOEiZR8KTA4UsIeEiNJjLqZ0cNBynBgYDYhmpfzApESEMm2ZRdiwxGsqNhtSGIQ3Bokpv2GuTMFCB81TYTeYBJGbcgsIKHuCsePSQkAQobS2NDCQFR6ZGxr6+lUTIMFjYWNaMLCignItDQ7sodExRTUtHAByz9My4QWDHEtAfAnz4IAZiX68zAjARAaeJivyyiMT4UAcHhbEQU9kRvx4eIEAACH5BAUEAH8ALAAADgAZAC0AAAf/gH6Cg4JdXnKEiYqKcDNxJHwRi5ODYx41DQ0PR3RalItYOV99fSAEJmOfiRMDIFukfR9QZqqEIVQSsH1fPW6etVoJPSC6Khpov7ViWBgxr1tELF61g1woRUN4OQpYZ9SJcj8+BygOXN+KaWBd6O3u735cXl3n71wJLScXa6ntYzsaqDCxwyJCMmoAXVBQo0IPlgLoxHRQo8vFEBHtABDTpeTGwU9i4uTSVefAR0pcngTR1UcFDFrUxrzA8QoWiA0OTk46EaUmLCYt0CUYMkoXDinTqJU5woaliwAZ6qkak6JBGF1hOMzwRi0ClaKw1ARI8K0MAGdYMaQoE3MGkJFhXodckKrKQ1OWVkzI0ZmIS4ocH1i+QSCCLqUycZQ4hTL324UBbWpKUAJgjWFKZ148kECBAg4BM2B+SxMnCYEkIw4ga6elTIUmB+gUuEyNDJoCYODp3s27t+/fwIMrCgQAIfkEBQQAfwAsAAAJABMAKgAAB/+AfoKDhIJkY15ihYuDbjoLTy8ZjIUoAngqDUxNk5R+CRoyYX19Kggvioxde0BfpKQNJFyUXSkEr6QfWV6UXkgPLrgNUmaeEXMcIKRtTieznnQkRDlEQze8noIoNyl5ItjZgmBdXVqe5uGDYGIZCQ7gnmUoGxobUiiplGQXSW8SOElgoHjGqIwCCa9AYHiBxhMSIrj6XImTr5AWKTi24FKxgIy+Em0iuvDhkZGYKioiXmEBr9ACNhGtHEC3KE2cYLhwvBhDySbOVzgOVCxEIkjEOli6UDpj4orGVy6qNGQ0ZoYVCrhANCngiYYTF1i3SNBTpCUhOVWA1MGxBEEcOQQoF3XJowNCABgz1ijN1sVBhgxyvNAMpyVuusOIEytezLix48eQI1MKBAAh+QQFBAB/ACwAAAUAGAAjAAAH/4B+goOEhYNmYFqGi4ZjfDAlLXKMlDZQOUscAyxplIVcInZBYVtbQUYznoRgKQ1ffbAUDVgFqoJiPhywuzJxXLZ+XjMPILtbbUVjwH4RRw27bBoRiss2DEk4NVMvXYy/1INlOxUpBw5ei2UJNzYoBb+DWmBnXeCEDgdMDUAjNmvdwMDoaBOmz5crGm6IWWZGgIRdFHAYmGCPkRg+Dbbs6hMkQAJgZey82djnA4latkQMeLixzoGKi7qEcKLiFSw1AnbANKSFjgcMNSlIsFJioS0tZH4ccaKHw4MREZb50ZImwg0fLDoUACNVEBcxZdCMgde1rNmzaNOqXcu2rdu3cATjUgoEACH5BAUEAH8ALAEAAwAdABgAAAd+gH6Cg4SFhoIRIl6HjIdedAtUTBo+aFqNmH4ZAUpqXyBAMAmZjGI+eF99qmxTN6SHGRB1qqoSNXyvhg4ws7R9NSmLuYQpRGq0agN3l8ODFwA1b1tBTh1dzYReEUUBA3YHDtiFWmQJDmJgzOKEWurr7/Dx8vP09fb3+Pn6+/GBADs=";

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  font-size: 11px;\n  line-height: 1.5;\n  .false {\n    color: #ff0000;\n  }\n  .true {\n    color: #58C22E;\n  }\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  color: #ff0000;\n  font-size: 12px;\n  margin-top: -17px;\n  height: 18px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  color: #333333;\n  background-color: #ffffff;\n  border: 1px solid #cccccc;\n  font-size: 14px;\n  height: 40px;\n  border-radius: 4px;\n  padding: 0px 12px;\n  width: 100%;\n  box-sizing: border-box;\n  \n  &[disabled] {\n    background-color: #eee;\n    opacity: 1;\n  }\n\n  &:focus {\n    border-color: #3898EC;\n    outline: 0;\n  }\n\n  &.error {\n    border-color: #ff0000;\n  }\n  \n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject$1() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  margin-bottom: 20px;\n  .loading-icon {\n    position: absolute;\n    top: 50%;\n    right: 10px;\n    transform: translateY(-50%);\n    width: 20px;\n    height: 20px;\n    background-image: url(", ");\n    background-size: contain;\n  }\n"]);

  _templateObject$1 = function _templateObject() {
    return data;
  };

  return data;
}
var InputContainer = styled.div(_templateObject$1(), LoadingGIF);
var Input = styled.input(_templateObject2());
var ErrorMessage = styled.div(_templateObject3());
var Debug = styled.div(_templateObject4());

var FieldError =
/*#__PURE__*/
function (_Error) {
  _inherits(FieldError, _Error);

  function FieldError(_ref) {
    var _this;

    var message = _ref.message,
        fieldName = _ref.fieldName,
        componentName = _ref.componentName;

    _classCallCheck(this, FieldError);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FieldError).call(this, message));
    _this.message = JSON.stringify({
      fieldName: fieldName,
      componentName: componentName,
      message: message
    }, null, 2);
    return _this;
  }

  return FieldError;
}(_wrapNativeSuper(Error));

/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @function isValid
 * @param {string} errorMessage The error message
 * @description Defines if the input is valid or not based on the presence of an error message
 * @returns {boolean}
 */
var isValid = function isValid(errorMessage) {
  return errorMessage ? false : true;
};
/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @function isGreaterThanMinLength
 * @param {string} value The input value
 * @param {number} minLength The min length
 * @description Validates the min length
 * @returns {boolean}
 */


var isGreaterThanMinLength = function isGreaterThanMinLength(value, minLength) {
  return value.length < minLength ? false : true;
};
/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @function isLessThanMaxLength
 * @param {string} value The input value
 * @param {number} maxLength The max length
 * @description Validates the min length
 * @returns {boolean}
 */


var isLessThanMaxLength = function isLessThanMaxLength(value, maxLength) {
  return value.length > maxLength ? false : true;
};
/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @function isRequiredValid
 * @param {string} value 
 * @description Validates required value
 * @returns {boolean}
 */


var isRequiredValid = function isRequiredValid(value) {
  return value && value.length > 0 ? true : false;
};

var QuestionMarkIcon = "export default \"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCI+CiAgICA8cGF0aCBkPSJNMTguOTI3IDcuOTg0QTcuODMgNy44MyAwIDAgMSAyMCAxMmMwIDEuNDUxLS4zNTggMi43OS0xLjA3MyA0LjAxNmE3Ljk2NyA3Ljk2NyAwIDAgMS0yLjkxMSAyLjkxMUE3LjgyNSA3LjgyNSAwIDAgMSAxMiAyMGE3LjgzIDcuODMgMCAwIDEtNC4wMTYtMS4wNzMgNy45NjUgNy45NjUgMCAwIDEtMi45MTEtMi45MTFBNy44MjYgNy44MjYgMCAwIDEgNCAxMmMwLTEuNDUxLjM1OC0yLjc5IDEuMDczLTQuMDE2YTcuOTYgNy45NiAwIDAgMSAyLjkxMi0yLjkxQTcuODI0IDcuODI0IDAgMCAxIDEyIDRjMS40NTIgMCAyLjc5LjM1OCA0LjAxNiAxLjA3M2E3Ljk2NiA3Ljk2NiAwIDAgMSAyLjkxMSAyLjkxMXptLTMuMDU3IDIuODZhMi43OTcgMi43OTcgMCAwIDAtLjQ0OC0yLjU0MiAzLjk0NSAzLjk0NSAwIDAgMC0xLjQ0Mi0xLjIwOCAzLjk1MiAzLjk1MiAwIDAgMC0xLjc3MS0uNDI4Yy0xLjY4OCAwLTIuOTc2Ljc0LTMuODY1IDIuMjItLjEwNC4xNjYtLjA3Ni4zMTIuMDgzLjQzN2wxLjM3NSAxLjA0MmEuMjk3LjI5NyAwIDAgMCAuMTk4LjA2MmMuMTExIDAgLjE5OC0uMDQyLjI2LS4xMjUuMzY5LS40NzIuNjY3LS43OTIuODk3LS45NTkuMjM2LS4xNjYuNTM0LS4yNS44OTYtLjI1LjMzMyAwIC42My4wOS44OS4yNzEuMjYuMTgxLjM5LjM4Ni4zOS42MTUgMCAuMjY0LS4wNjkuNDc2LS4yMDguNjM1LS4xMzguMTYtLjM3NC4zMTYtLjcwOC40N2EzLjU2NiAzLjU2NiAwIDAgMC0xLjIwMy45Yy0uMzY1LjQwNi0uNTQ3Ljg0Mi0uNTQ3IDEuMzA3di4zNzZjMCAuMDk3LjAzMS4xNzcuMDk0LjI0QS4zMjQuMzI0IDAgMCAwIDExIDE0aDJhLjMyNi4zMjYgMCAwIDAgLjI0LS4wOTQuMzI1LjMyNSAwIDAgMCAuMDk0LS4yNGMwLS4xMzEuMDc1LS4zMDMuMjI0LS41MTUuMTQ5LS4yMTIuMzM4LS4zODMuNTY3LS41MTYuMjIyLS4xMjUuMzkyLS4yMjMuNTEtLjI5Ni4xMTktLjA3My4yNzktLjE5NS40OC0uMzY1YTIuNDMgMi40MyAwIDAgMCAuNDYzLS41Yy4xMDgtLjE2My4yMDUtLjM3My4yOTItLjYzek0xMy4zMzQgMTd2LTJhLjMyMi4zMjIgMCAwIDAtLjMzMy0uMzMzSDExYS4zMjcuMzI3IDAgMCAwLS4yNC4wOTMuMzI1LjMyNSAwIDAgMC0uMDkzLjI0djJjMCAuMDk3LjAzMS4xNzcuMDkzLjI0YS4zMjUuMzI1IDAgMCAwIC4yNC4wOTNoMmEuMzIuMzIgMCAwIDAgLjMzNC0uMzMzeiIvPgo8L3N2Zz4K\"";

function _templateObject$2() {
  var data = _taggedTemplateLiteral(["\n\n  max-width: 300px;\n  padding: 12px 16px;\n  background-color: #ffffff;\n  border: 1px solid #ccc;\n  border: 1px solid rgba(0, 0, 0, 0.2);\n  border-radius: 0;\n  color: #777;\n  font-size: 11px;\n  font-weight: bold;\n  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);\n          box-shadow: 0 5px 10px rgba(0, 0, 0, .2);\n\n  &:after {\n    content: '';\n    display: block;\n    width: 15px;\n    height: 15px;\n    position: absolute;\n    left: 50%;\n    background-color: #ffffff;\n    border-bottom-width: 1px;\n  }\n\n  &.above {\n    &:after {\n      top: 100%;\n      transform: translate(-50%,-48%) rotate(45deg);\n      border: 1px solid #ccc;\n      border-top: 0;\n      border-left: 0;\n    }\n  }\n\n  &.below {\n    &:after {\n      top: 0%;\n      transform: translate(-50%,-52%) rotate(45deg);\n      border: 1px solid #ccc;\n      border-bottom: 0;\n      border-right: 0;\n    }\n  }\n\n  &.left {\n    &:after {\n      left: 20px\n    }\n  }\n\n  &.right {\n    &:after {\n      left: auto;\n      right: 3px;\n    }\n  }\n\n"]);

  _templateObject$2 = function _templateObject() {
    return data;
  };

  return data;
}
var StyledTooltip = styled.div(_templateObject$2());

var Tooltip =
/*#__PURE__*/
function (_Component) {
  _inherits(Tooltip, _Component);

  function Tooltip(props) {
    var _this;

    _classCallCheck(this, Tooltip);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Tooltip).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "alignBallonX", function (parentLeftPosition, ballonWidth, parentWidth, windowWidth) {
      var leftPosition;
      var className;

      if (parentLeftPosition + ballonWidth / 2 <= windowWidth) {
        leftPosition = parentLeftPosition - ballonWidth / 2 + parentWidth / 2;
        className = '';
      } else if (parentLeftPosition + ballonWidth / 2 > windowWidth) {
        leftPosition = parentLeftPosition - ballonWidth + parentWidth / 2 + 20;
        className = 'right';
      }

      if (leftPosition < 0) {
        leftPosition = parentLeftPosition + parentWidth / 2 - 20;
        className = 'left';
      }

      return {
        left: leftPosition,
        class: className
      };
    });

    _this.refCard = React__default.createRef();
    _this.state = {
      position: {
        top: 0,
        left: 0
      }
    };
    return _this;
  }

  _createClass(Tooltip, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.width = this.refCard.current.getBoundingClientRect().width;
      this.height = this.refCard.current.getBoundingClientRect().height;
      this.abovePosition = this.props.parentTopPosition - this.height - 15;
      this.belowPosition = this.props.parentTopPosition + this.props.parentHeight + 15;
      this.yPosition = this.abovePosition >= 0 ? 'above' : 'below';
      this.setState({
        position: {
          top: this.abovePosition >= 0 ? this.abovePosition : this.belowPosition,
          left: this.alignBallonX(this.props.parentLeftPosition, this.width, this.props.parentWidth, window.innerWidth).left,
          className: this.alignBallonX(this.props.parentLeftPosition, this.width, this.props.parentWidth, window.innerWidth).class
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      return ReactDOM.createPortal(React__default.createElement(StyledTooltip, {
        ref: this.refCard,
        className: "".concat(this.yPosition, " ").concat(this.state.position.className),
        style: {
          position: 'absolute',
          top: this.state.position.top,
          left: this.state.position.left,
          zIndex: 999
        }
      }, this.props.description), document.getElementById("bottom-portal"));
    }
  }]);

  return Tooltip;
}(React.Component);

Tooltip.propTypes = {
  description: propTypes.node.isRequired,
  parentTopPosition: propTypes.number.isRequired,
  parentLeftPosition: propTypes.number.isRequired,
  parentWidth: propTypes.number.isRequired,
  parentHeight: propTypes.number.isRequired
};

var allowedEvents = ['onMouseEnter', 'onMouseLeave', 'onClick', 'onFocus', 'onBlur', 'onScroll'];

var TooltipHOC =
/*#__PURE__*/
function (_Component) {
  _inherits(TooltipHOC, _Component);

  function TooltipHOC(props) {
    var _this;

    _classCallCheck(this, TooltipHOC);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TooltipHOC).call(this, props));
    _this.state = {
      show: false
    };
    _this.toolTipRef = React__default.createRef();
    _this.top = 0;
    _this.left = 0;
    _this.width = 0;
    _this.height = 0;
    _this.setShowToFalse = _this.setShowToFalse.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(TooltipHOC, [{
    key: "setShowToFalse",
    value: function setShowToFalse() {
      this.setState({
        show: false
      });
    }
  }, {
    key: "showToolTip",
    value: function showToolTip(e, bool) {
      window.addEventListener('scroll', this.setShowToFalse);

      if (bool) {
        this.top = e.currentTarget.getBoundingClientRect().top + window.scrollY;
        this.left = e.currentTarget.getBoundingClientRect().left;
        this.width = e.currentTarget.getBoundingClientRect().width;
        this.height = e.currentTarget.getBoundingClientRect().height;
      }

      this.setState({
        show: bool
      });
    }
  }, {
    key: "_onMouseEnter",
    value: function _onMouseEnter(e, parentFunction, HOCFunction) {
      this.props.showEvent === allowedEvents[0] && this.showToolTip(e, true);
      parentFunction && parentFunction();
      HOCFunction && HOCFunction();
    }
  }, {
    key: "_onMouseLeave",
    value: function _onMouseLeave(e, parentFunction, HOCFunction) {
      this.props.hideEvent === allowedEvents[1] && this.showToolTip(e, false);
      parentFunction && parentFunction();
      HOCFunction && HOCFunction();
    }
  }, {
    key: "_onFocus",
    value: function _onFocus(e, parentFunction) {
      this.props.showEvent == allowedEvents[3] && this.showToolTip(e, true);
      parentFunction && parentFunction();
    }
  }, {
    key: "_onBlur",
    value: function _onBlur(e, parentFunction) {
      this.props.hideEvent == allowedEvents[4] && this.showToolTip(e, false);
      parentFunction && parentFunction();
    }
  }, {
    key: "_onClick",
    value: function _onClick(e, parentFunction) {
      this.props.showEvent === allowedEvents[2] && this.props.showOn && this.showToolTip(e, true);
      this.props.showEvent === allowedEvents[0] && this.showToolTip(e, true);
      parentFunction && parentFunction();
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var elPortal = document.getElementById('bottom-portal');

      if (!elPortal) {
        elPortal = document.createElement('div');
        elPortal.setAttribute('id', 'bottom-portal');
        document.body.appendChild(elPortal);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('scroll', this.setShowToFalse);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var fn = function fn(child) {
        return React__default.cloneElement(child, {
          onMouseEnter: function onMouseEnter(e) {
            return _this2._onMouseEnter(e, child.props.onMouseEnter, _this2.props.onMouseEnter);
          },
          onMouseLeave: function onMouseLeave(e) {
            return _this2._onMouseLeave(e, child.props.onMouseLeave, _this2.props.onMouseLeave);
          },
          onFocus: function onFocus(e) {
            return _this2._onFocus(e, child.props.onFocus);
          },
          onBlur: function onBlur(e) {
            return _this2._onBlur(e, child.props.onBlur);
          },
          onClick: function onClick(e) {
            return _this2._onClick(e, child.props.onClick);
          }
        });
      };

      return React__default.createElement(React__default.Fragment, null, React__default.Children.map(this.props.children, fn), this.state.show ? React__default.createElement(Tooltip, {
        description: this.props.description,
        parentTopPosition: this.top,
        parentLeftPosition: this.left,
        parentWidth: this.width,
        parentHeight: this.height
      }) : null);
    }
  }]);

  return TooltipHOC;
}(React.Component);

TooltipHOC.defaultProps = {
  showEvent: allowedEvents[0],
  //onMouseEnter
  hideEvent: allowedEvents[1],
  //onMouseLeave
  showOn: true
};
TooltipHOC.propTypes = {
  children: propTypes.oneOfType([propTypes.object, propTypes.array]),
  description: propTypes.oneOfType([propTypes.object, propTypes.string]).isRequired,
  showEvent: propTypes.oneOf(allowedEvents),
  hideEvent: propTypes.oneOf(allowedEvents),
  showOn: propTypes.bool,
  onMouseLeave: propTypes.func,
  onMouseEnter: propTypes.func
};

var EN = {
	MAX_LENGTH_ERROR: "The text entered exceeds the maximum length",
	MIN_LENGTH_ERROR: "The text entered needs to have at least {{minLength}} characters",
	REQUIRED_ERROR: "This field is required"
};
var originalLocales = {
	EN: EN
};

var ErrorMessage$1 = function ErrorMessage(_ref) {
  var fieldName = _ref.fieldName,
      errorCode = _ref.errorCode,
      lang = _ref.lang,
      minLength = _ref.minLength,
      maxLength = _ref.maxLength,
      locales = _ref.locales;

  var throwError = function throwError(message) {
    var errorInfos = {
      fieldName: fieldName,
      message: message,
      componentName: 'ErrorMessage'
    };
    throw new FieldError(errorInfos);
  };

  var _errorMessage = errorCode;
  lang = lang.toUpperCase();
  errorCode = errorCode.toUpperCase();
  var isCode = errorCode.includes('_');

  if (isCode) {
    var hasCustomLocale = !!locales;

    if (hasCustomLocale) {
      try {
        if (!locales[lang]) throwError("Could'nt read ".concat(lang, " inside custom 'locales' object"));
        if (!locales[lang][errorCode]) throwError("Could'nt read ".concat(errorCode, " inside custom 'locales[ ").concat(lang, " ]' object"));
        _errorMessage = locales[lang][errorCode];
      } catch (err) {
        _errorMessage = originalLocales['EN'][errorCode];
        console.error(err);
      }
    } else {
      try {
        if (!originalLocales) throwError("Could'nt read 'originalLocales'");
        if (!originalLocales[lang]) throwError("Could'nt read ".concat(lang, " inside 'originalLocales' object"));
        if (!originalLocales[lang][errorCode]) throwError("Could'nt read ".concat(errorCode, " inside 'originalLocales[ ").concat(lang, " ]' object"));
        _errorMessage = originalLocales[lang][errorCode];
      } catch (err) {
        _errorMessage = errorCode;
        console.error(err);
      }
    }

    if (_errorMessage.includes('{{minLength}}')) {
      _errorMessage = _errorMessage.replace('{{minLength}}', minLength);
    }

    if (_errorMessage.includes('{{maxLength}}')) {
      _errorMessage = _errorMessage.replace('{{maxLength}}', maxLength);
    }
  }

  return _errorMessage;
};

function _templateObject4$1() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n"]);

  _templateObject4$1 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3$1() {
  var data = _taggedTemplateLiteral(["\n  border: 1px solid #c9c9c9;\n  border-radius: 5px;\n  padding: 15px;\n  margin-bottom: 15px;\n  width: 50%;\n"]);

  _templateObject3$1 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2$1() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bolder;\n  margin-bottom: 10px;\n"]);

  _templateObject2$1 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject$3() {
  var data = _taggedTemplateLiteral(["\n  font-family: sans-serif;\n  font-size: 12px;\n  background-color: #e9e9e9;\n  padding: 15px;\n  pre {\n    font-family: sans-serif;\n    margin: 0;\n  }\n"]);

  _templateObject$3 = function _templateObject() {
    return data;
  };

  return data;
}
var Container = styled.div(_templateObject$3());
var Title = styled.div(_templateObject2$1());
var Box = styled.div(_templateObject3$1());
var Flex = styled.div(_templateObject4$1());

var FieldDebug = function FieldDebug(props) {
  return React__default.createElement(Container, null, React__default.createElement(Title, null, "DEBUG:"), React__default.createElement(Flex, null, React__default.createElement(Box, null, React__default.createElement("div", null, React__default.createElement("strong", null, "Props:")), React__default.createElement("pre", null, JSON.stringify(props.parentProps, null, 2))), React__default.createElement(Box, null, React__default.createElement("div", null, React__default.createElement("strong", null, "State:")), React__default.createElement("pre", null, JSON.stringify(props.state, null, 2)))));
};

function _templateObject2$2() {
  var data = _taggedTemplateLiteral(["\n  font-size: 14px;\n  line-height: 24px;\n  font-weight: 600;\n"]);

  _templateObject2$2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject$4() {
  var data = _taggedTemplateLiteral(["\n  border-left: 5px solid #eee;\n  padding-left: 12px;\n  margin-bottom: 24px;\n"]);

  _templateObject$4 = function _templateObject() {
    return data;
  };

  return data;
}
var Container$1 = styled.div(_templateObject$4());
var Label$1 = styled.div(_templateObject2$2());

var ReadOnly = function ReadOnly(props) {
  function getValue(value) {
    if (typeof value !== 'undefined') {
      if (typeof value === 'string') {
        return value || '---';
      } else {
        return value.length > 0 ? value.join() : '---';
      }
    }

    return '---';
  }

  return React__default.createElement(Container$1, null, React__default.createElement(Label$1, null, props.label), React__default.createElement("p", null, getValue(props.value)));
};

ReadOnly.propTypes = {
  label: propTypes.string,
  value: propTypes.oneOfType([propTypes.string, propTypes.array])
};

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

var Input$1 = function Input$1(props) {
  var _useState = React.useState(props.value || ''),
      _useState2 = _slicedToArray(_useState, 2),
      value = _useState2[0],
      setValue = _useState2[1];

  var _useState3 = React.useState(false),
      _useState4 = _slicedToArray(_useState3, 2),
      isErrorVisible = _useState4[0],
      setIsErrorVisible = _useState4[1];

  var _useState5 = React.useState(),
      _useState6 = _slicedToArray(_useState5, 2),
      errorMessage = _useState6[0],
      setErrorMessage = _useState6[1];

  var _useState7 = React.useState(false),
      _useState8 = _slicedToArray(_useState7, 2),
      isLoading = _useState8[0],
      setIsLoading = _useState8[1]; // This refValue is set because the `useEffect` that runs when
  // the component unmount needs to get the updated value.


  var refValue = React.useRef(value);
  var refErrorMessage = React.useRef();
  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function throwError 
   * @description Default function to throw a field error
   * @param {string} message The error message
   * @throws FieldError
   */

  var throwError = function throwError(message) {
    var errorInfos = {
      componentName: 'Input',
      fieldName: props.name,
      message: message
    };
    throw new FieldError(errorInfos);
  };
  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function getFieldObj
   * @param {string} value The input value
   * @description Defines the fieldObj based on value
   * @returns {object} Returns the fieldObj
   */


  var getFieldObj = function getFieldObj(value) {
    var _errorMessage = getErrorMessage(value); // Get the error message
    // setErrorMessage( _errorMessage ) // Set the error message state
    // Set and return the fieldObj


    return {
      name: props.name,
      value: value,
      isValid: isValid(_errorMessage) // The validation is based on the errorMessage

    };
  };
  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function getErrorMessage
   * @param {string} value The input value
   * @description Get the error message
   * @returns {undefined|string} Returns the error message
   */


  var getErrorMessage = function getErrorMessage(value) {
    var isValid = true;
    var _errorMessage = undefined;

    if (isValid && props.errorMessage) {
      isValid = false;
      _errorMessage = props.errorMessage;
    }

    if (isValid && props.isRequired) {
      isValid = isRequiredValid(value);
      _errorMessage = isValid ? undefined : 'REQUIRED_ERROR';
    }

    if (isValid && value && props.minLength) {
      isValid = isGreaterThanMinLength(value, props.minLength);
      _errorMessage = isValid ? undefined : 'MIN_LENGTH_ERROR';
    }

    if (isValid && value && props.maxLength) {
      isValid = isLessThanMaxLength(value, props.maxLength);
      _errorMessage = isValid ? undefined : 'MAX_LENGTH_ERROR';
    }

    if (isValid && props.customValidationFunc) {
      isValid = !props.customValidationFunc(value).errorMessage;
      _errorMessage = isValid ? undefined : props.customValidationFunc(value).errorMessage;
    }

    return _errorMessage;
  };
  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function _onBlur
   * @description Every time the input has an `onBlur` event, this function will run
   * @returns {undefined}
   */


  var _onBlur =
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var _errorMessage, response;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setIsErrorVisible(true); // The error visibility is set to true on each blur
              // Get and set the error message

              _errorMessage = getErrorMessage(value);
              setErrorMessage(_errorMessage);
              refErrorMessage.current = _errorMessage; // Checking if we need to call the external validation promise

              if (!(props.externalValidationFunc // Check if there's an external validation
              && value // Check if there's a value set on *state*
              )) {
                _context.next = 22;
                break;
              }

              // Here we run the onBlur method before the promise, because we need to dispatch an immediatelly event
              // In this case, we dispatch as invalid because the promise will specify the right validation
              props.onBlur && props.onBlur({
                name: props.name,
                value: value,
                isValid: false
              });
              _context.prev = 6;
              setIsLoading(true);
              _context.next = 10;
              return props.externalValidationFunc({
                name: props.name,
                value: value,
                isValid: false
              });

            case 10:
              response = _context.sent;
              if (!response) throwError('The response of ExternalValidation is undefined');
              _errorMessage = refErrorMessage.current || response.errorMessage; // Overwrite the error message with the returned error message

              setErrorMessage(_errorMessage); // Set the new error message on *state*

              _context.next = 19;
              break;

            case 16:
              _context.prev = 16;
              _context.t0 = _context["catch"](6);
              console.error(_context.t0);

            case 19:
              _context.prev = 19;
              setIsLoading(false);
              return _context.finish(19);

            case 22:
              // Check and run the `props.onBlur` function
              props.onBlur && props.onBlur({
                name: props.name,
                value: value,
                isValid: isValid(_errorMessage)
              });

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[6, 16, 19, 22]]);
    }));

    return function _onBlur() {
      return _ref.apply(this, arguments);
    };
  }();
  /**
   * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   * @function _onChange
   * @description Every time the input has an `onChange` event, this function will run
   * @param {string} value 
   */


  var _onChange = function _onChange(value) {
    // Here we update the value on *state*
    setValue(props.maskFunc ? props.maskFunc(value) : value);
    refValue.current = props.maskFunc ? props.maskFunc(value) : value; // WHILE the user inputs a value, we don't want to
    // inform the errors, so we define the errors visibility
    // to `false`. The error visibility will be set to `true`
    // again on the `_onBlur_` function

    setIsErrorVisible(false); // Get and set the error message on *state*

    var _errorMessage = getErrorMessage(value);

    setErrorMessage(_errorMessage);
    refErrorMessage.current = _errorMessage; // Check and run the `props.onChange` function

    props.onChange && props.onChange({
      name: props.name,
      value: refValue.current,
      isValid: isValid(_errorMessage)
    });
  };
  /**
   * ----------------------------------------------------------------------------------------------
   * - - - - - - - - - - - - - - - - - - - USE EFFECT HOOKS - - - - - - - - - - - - - - - - - - - -
   * ----------------------------------------------------------------------------------------------
   */
  // ----------------------------------------------------------------------------------------------
  // This useEffect simulates the `componentDidMount` lifecycle - - - - - - - - - - - - - - - - - - 


  React.useEffect(function () {
    var fieldObj = getFieldObj(value); // Get the `fieldObj`

    var _errorMessage = getErrorMessage(value); // Get the error message


    setErrorMessage(_errorMessage); // Set the error message state

    refErrorMessage.current = _errorMessage;
    props.onMount && props.onMount(fieldObj); // Run the `onMount` prop with the `fieldObj`
  }, []); // ---------------------------------------------------------------------------------------------
  // Every time the `props.value` changes, we run the code below - - - - - - - - - - - - - - - - -

  React.useEffect(function () {
    if (props.value !== value) {
      setValue(props.value); // Set the value state with the `props.value`

      var fieldObj = getFieldObj(props.value); // Get the `fieldObj`

      var _errorMessage = getErrorMessage(props.value); // Get the error message


      setErrorMessage(_errorMessage); // Set the error message state

      refErrorMessage.current = _errorMessage; // Here we run `onChange` and `onBlur` props.
      // Why we are running both props?
      // As the `props.value` is setting a new value, we need to inform the parent that the value has
      // changed. So, if `props.onChange` and/or `props.onBlur` are set, then we need to run both.

      props.onChange && props.onChange(fieldObj);
      props.onBlur && props.onBlur(fieldObj);
    }
  }, [props.value]); // ---------------------------------------------------------------------------------------------
  // Every time the `value` changes, we run the code below - - - - - - - - - - - - - - - - -

  React.useEffect(function () {
    // This exists only to update our refValue with the current state value.
    // Needed only because the `useEffect` that runs when the component unmounts
    // needs to get the updated value.
    refValue.current = value;
  }, [value]); // --------------------------------------------------------------------------------------------
  // Every time the `props.errorMessage` changes, we run the code below - - - - - - - - - - - - -

  React.useEffect(function () {
    if (props.errorMessage !== errorMessage) {
      setIsErrorVisible(true); // As we are handling an error message, we set enable the error visibility

      var fieldObj = getFieldObj(value); // Get the `fieldObj`

      var _errorMessage = getErrorMessage(value); // Get the error message


      setErrorMessage(_errorMessage); // Set the error message state

      refErrorMessage.current = _errorMessage; // Here we run `onChange` and `onBlur` props.
      // Why we are running both props?
      // As the `props.value` is setting a new value, we need to inform the parent that the value has
      // changed. So, if `props.onChange` and/or `props.onBlur` are set, then we need to run both.

      props.onChange && props.onChange(fieldObj);
      props.onBlur && props.onBlur(fieldObj);
    }
  }, [props.errorMessage]); // --------------------------------------------------------------------------------------------
  // Every time the `props.isErrorVisible` changes, we run the code below - - - - - - - - - - - - -

  React.useEffect(function () {
    props.isErrorVisible !== undefined && setIsErrorVisible(props.isErrorVisible);
  }, [props.isErrorVisible]); // --------------------------------------------------------------------------------------------
  // When the component unmounts, we run the code below - - - - - - - - - - - - - - - - - - - - -

  React.useEffect(function () {
    return function () {
      // Get the errorMessage
      var _errorMessage = getErrorMessage(refValue.current); // Run the onUnmount prop


      props.onUnmount && props.onUnmount({
        name: props.name,
        value: refValue.current,
        isValid: isValid(_errorMessage)
      });
    };
  }, []);
  return props.isReadOnly ? React__default.createElement(ReadOnly, {
    label: props.label,
    value: value
  }) : React__default.createElement(React__default.Fragment, null, props.label && React__default.createElement(Label, null, props.label, " ", props.isRequired && '*', props.label && props.description && React__default.createElement(TooltipHOC, {
    description: props.description
  }, React__default.createElement(QuestionMarkIcon, {
    width: 22,
    height: 22
  }))), React__default.createElement(InputContainer, null, React__default.createElement(Input, {
    type: 'text',
    name: props.name,
    value: value,
    placeholder: props.placeholder,
    disabled: props.isDisabled,
    onChange: function onChange(e) {
      return _onChange(e.target.value);
    },
    onBlur: function onBlur() {
      return _onBlur();
    },
    onPaste: !props.isPasteAllowed ? function (e) {
      e.preventDefault();
    } : null,
    className: errorMessage && isErrorVisible ? 'error' : undefined
  }), isLoading && React__default.createElement("div", {
    className: "loading-icon"
  })), isErrorVisible && errorMessage && React__default.createElement(ErrorMessage, null, React__default.createElement(ErrorMessage$1, {
    fieldName: props.name,
    errorCode: errorMessage,
    lang: props.lang,
    minLength: props.minLength,
    maxLength: props.maxLength,
    locales: props.locales
  })), props.isDebug && React__default.createElement(FieldDebug, {
    state: {
      value: value,
      isErrorVisible: isErrorVisible,
      errorMessage: errorMessage,
      isLoading: isLoading
    },
    parentProps: props
  }));
};

Input$1.defaultProps = {
  lang: 'EN',
  value: '',
  isDisabled: false,
  isRequired: false,
  isReadOnly: false,
  isDebug: false,
  isPasteAllowed: true
};
Input$1.propTypes = {
  // Main props
  name: propTypes.string.isRequired,
  value: propTypes.string,
  label: propTypes.string,
  description: propTypes.string,
  placeholder: propTypes.string,
  errorMessage: propTypes.string,
  isDisabled: propTypes.bool,
  // Advanced props
  lang: propTypes.string,
  locales: propTypes.object,
  maskFunc: propTypes.func,
  isErrorVisible: propTypes.bool,
  isReadOnly: propTypes.bool,
  isPasteAllowed: propTypes.bool,
  isDebug: propTypes.bool,
  // Methods:
  onBlur: propTypes.func,
  onChange: propTypes.func,
  onMount: propTypes.func,
  onUnmount: propTypes.func,
  // Validations:
  minLength: propTypes.number,
  maxLength: propTypes.number,
  isRequired: propTypes.bool,
  customValidationFunc: propTypes.func,
  externalValidationFunc: propTypes.func
};
var Input$2 = React__default.memo(Input$1);

exports.Input = Input$2;
