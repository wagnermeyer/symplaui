// import replace from 'rollup-plugin-replace'
import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import svg from 'rollup-plugin-svg'
import image from 'rollup-plugin-img'
import json from 'rollup-plugin-json'
import peerDepsExternal from 'rollup-plugin-peer-deps-external'
import { uglify } from "rollup-plugin-uglify"

export default {
  input: "./src/exports.js",
  output: {
    file: './lib/dev.js',
    format: "cjs"
  },
  external: [ 'react', 'react-dom', 'styled-components' ],
  plugins: [
    peerDepsExternal(),
    svg(),
    json(),
    resolve({
      extensions: [ '.js', '.jsx', '.json' ]
    }),
    babel({
      exclude: "node_modules/**",
      presets: ['@babel/env', '@babel/preset-react']
    }),
    commonjs(),
    // uglify(),
    image({
      limit: 99999,
      output: `./lib/images`
    }),
  ]
}